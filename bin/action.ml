open Core
open Lib

exception Phase_error of exn
exception Cont_phase_error of exn * bool
exception Program_error of exn

let raise_wrap exn =
  Caml.Printexc.raise_with_backtrace exn (Caml.Printexc.get_raw_backtrace ())

let rec msg_of_error exn =
  match exn with
  | Parse.Error err ->
    Printf.sprintf "Parse error: %s" (Parse.Error.to_string err)
  | Import.Dep.Missing dep ->
    Printf.sprintf "Dependency missing: %s" dep
  | Import.Dep.Broken (dep, exn) ->
    Printf.sprintf
      "Dependency broken: %s\n%s"
      dep
      (msg_of_error exn)
  | Misc.Child_not_found last_elem ->
    Printf.sprintf "Error interpreting: nonexistent path: ...>%s" last_elem
  | Inject.Bad_inj_head x ->
    Printf.sprintf
      "Error interpreting: undefined injection head: #%s"
      x
  | Inject.Bad_inj_params rsn ->
    Printf.sprintf
      "Error interpreting: bad injection parameters: %s"
      rsn
  | Remainder_m.Remainder_in_value_position ->
    "Error interpreting: remainder path in value (e.g. 'foo: >...')"
  | Remainder_m.Value_in_remainder_position ->
    "Error interpreting: value in remainder (e.g. '...: Foo[]')"
  | _ ->
    Printf.sprintf
      "Uncaught exception:\n%s\nBacktrace:\n%s"
      (Exn.to_string exn)
      (Printexc.get_backtrace ())

let evaluate_exc input =
  input |>
  Build.Program.evaluate_file ~format: Print.Format.default |>
  Printf.printf "%s\n"

let evaluate input =
  try evaluate_exc input with
  | exn -> Printf.printf "%s\n" (msg_of_error exn)

let read_refactor_phase in_ch =
  try Parse.Phase_cont.from_channel in_ch |>
      Misc.Phase.map_anns ~f: (fun _ -> ()) with
  | exn -> raise_wrap (Phase_error exn)

let apply_refactor_phase phase input ~format: format =
  try Build.Program.apply_macros_file phase input ~format: format with
  | exn -> raise_wrap (Program_error exn)

let evaluate_source input ~format: format =
  try Build.Program.evaluate_file input ~format: format with
  | exn -> raise_wrap (Program_error exn)

let refactor_exc input phase output =
  apply_refactor_phase phase input ~format: Print.Format.default |>
  fun data -> Out_channel.write_all output ~data: data;
  print_string "Done.\n"

let refactor_exc input phase_in output =
  let phase =
    ( match phase_in with
      | None ->
        print_string "Reading the macro phase from STDIN. Type '---\\n' to finish ...\n\n";
        Out_channel.flush Out_channel.stdout;
        let res = read_refactor_phase In_channel.stdin in
        print_string "\n";
        Out_channel.flush Out_channel.stdout;
        res
      | Some phase_in ->
        let phase_ch = In_channel.create phase_in in
        let res = read_refactor_phase phase_ch in
        In_channel.close phase_ch;
        res
    ) in
  refactor_exc input phase output

let refactor input phase_in output =
  let output = Option.value output ~default: input in
  try refactor_exc input phase_in output with
  | Phase_error exn ->
    Printf.printf "In phase:\n%s\n" (msg_of_error exn)
  | Program_error exn ->
    Printf.printf "In program:\n%s\n" (msg_of_error exn)

let is_source_file x =
  Filename.check_suffix x ".dscr" &&
  not (Filename.check_suffix x ".refactor.dscr")

let is_refactor_file x =
  Filename.check_suffix x ".refactor.dscr"

let source_refactor_file x =
  Filename.chop_suffix x ".dscr" ^ ".refactor.dscr"

let refactor_source_file x =
  Filename.chop_suffix x ".refactor.dscr" ^ ".dscr"

let create_empty_file x =
  let ch = Out_channel.create x in
  Out_channel.close ch

let ensure_file_exists x =
  if Sys.file_exists x ~follow_symlinks: true = `No then
    create_empty_file x

let ensure_file_gone x =
  if Sys.file_exists x ~follow_symlinks: true = `Yes then
    Sys.remove x

let setup_source_file x =
  ensure_file_exists (source_refactor_file x)

let rec setup_env_item x =
  if Sys.is_directory_exn x ~follow_symlinks: true then
    setup_env x
  else if is_source_file x then
    setup_source_file x
and setup_env input =
  Sys.readdir input |>
  Array.map ~f: (Filename.concat input) |>
  Array.iter ~f: setup_env_item

let teardown_source_file x =
  ensure_file_gone (source_refactor_file x)

let rec teardown_env_item x =
  if Sys.is_directory x ~follow_symlinks: true = `Yes then
    teardown_env x
  else if is_source_file x then
    teardown_source_file x
and teardown_env input =
  Sys.readdir input |>
  Array.map ~f: (Filename.concat input) |>
  Array.iter ~f: teardown_env_item

let rec refactor_source src ref =
  ensure_file_exists ref;
  let%lwt (new_src, new_ref) =
    Lwt.wrap
    ( fun () ->
        let ref_ch = In_channel.create ref in
        try
        ( let phase = read_refactor_phase ref_ch in
          let ref_rest = In_channel.input_all ref_ch in
          ( apply_refactor_phase phase src ~format: Print.Format.default,
            ref_rest
          )
        ) with
        | Phase_error (Parse.Error err) ->
          let ref_rest = In_channel.input_all ref_ch in
          let is_ready = not (String.is_empty ref_rest) in
          raise (Cont_phase_error (Parse.Error err, is_ready))
    ) in
  Lwt_io.with_file src ~mode: Lwt_io.Output
  ( fun src_ch ->
      Lwt_io.write src_ch new_src
  );%lwt
  Lwt_io.with_file ref ~mode: Lwt_io.Output
  ( fun ref_ch ->
      Lwt_io.write ref_ch new_ref
  );%lwt
  Lwt_io.printlf "Refactored %s" (Filename.basename src);%lwt
  refactor_source src ref

let rec try_refactor_source src ref =
  try%lwt refactor_source src ref with
  | Cont_phase_error (exn, is_ready) ->
    if is_ready then
      Lwt.fail (Phase_error exn)
    else
      Lwt.return ()

let eval_source src =
  let%lwt out = Lwt.wrap (fun () -> evaluate_source src ~format: Print.Format.default) in
  Lwt_io.printlf "%s:\n%s" src out

let update_env_source_file src =
  try_refactor_source src (source_refactor_file src);%lwt
  eval_source src

let update_env_refactor_file ref =
  try_refactor_source (refactor_source_file ref) ref

let try_update_env_source_file src =
  try%lwt update_env_source_file src with
  | Phase_error exn ->
    Lwt_io.printlf
      "Error in %s:\n%s"
      (Filename.basename (source_refactor_file src))
      (msg_of_error exn)
  | Program_error exn ->
    Lwt_io.printlf
      "Error in %s:\n%s"
      (Filename.basename src)
      (msg_of_error exn)

let try_update_env_refactor_file ref =
  try%lwt update_env_refactor_file ref with
  | Phase_error exn ->
    Lwt_io.printlf
      "Error in %s:\n%s"
      (Filename.basename ref)
      (msg_of_error exn)
  | Program_error exn ->
    Lwt_io.printlf
      "Error in %s (prevented refactor):\n%s"
      (Filename.basename (refactor_source_file ref))
      (msg_of_error exn)

let try_update_env_item x =
  if Sys.file_exists x = `No then
    Lwt.wrap (fun () -> teardown_env_item x)
  else if is_source_file x then
    try_update_env_source_file x
  else if is_refactor_file x then
    try_update_env_refactor_file x
  else
    Lwt.return ()

let start_env input no_teardown =
  let teardown = not no_teardown in
  print_string "Starting development environment...\n";
  setup_env input;
  Out_channel.flush Out_channel.stdout;
  let unwatch_cond = Lwt_condition.create () in
  if teardown then
    Lwt_unix.on_signal (Signal.to_caml_int Signal.int)
    ( fun _ ->
        print_string "\nTearing down development environment...\n";
        Out_channel.flush Out_channel.stdout;
        Lwt_condition.signal unwatch_cond ();
        teardown_env input;
        print_string "Done.\n"
    ) |> ignore;
  Lwt_main.run
  ( let%lwt unwatch = Watcher.hook input try_update_env_item in
    Lwt_io.printl "Started development environment. Ctrl-C to stop.";%lwt
    Lwt_condition.wait unwatch_cond;%lwt
    unwatch ()
  )
