open Core

let next_id_ref = ref 0

let get_next_id () =
  let res = !next_id_ref in
  next_id_ref := res + 1;
  res

let hook_update dir f item =
  f (Filename.concat dir item)

let rec hook dir f =
  Irmin_watcher.hook (get_next_id ()) dir (hook_update dir f)
