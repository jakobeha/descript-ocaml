# Roadmap

- Documentation (of language and code)
- Better pretty printer
- JavaScript backend
- Refactor imports locally
- Range (primitive number) matchers
- VSCode extension (e.g. automatically open `.refactor.dscr` in other pane)
- Efficient interpreter (uses hashmaps to lookup which values to consume)
- Arrays, maps, etc. (via pointer primitives?)
- Compile / use interpreted code
