open OUnit2
open Test_util
open Lib
open Lib.Ast

let assert_print exp x ~format: format =
  assert_equal_str exp (Print.Pure.Program.to_string x ~format: format)

let test_print_simple_basic ctx =
  assert_print
    ~format: Print.Format.Basic
    {|
Foo[foo: <Integer]: Foo[foo: >];
Foo[foo: <]: Bar[];
---
Foo[foo: 4]?
|}
    { phases = [[
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), Integer)
                    };
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), []);
                    };
                  ];
                });
          };
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), Any)
                    };
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "Bar";
                  props = [];
                });
          };
        ]];
      query = Record ((),
          { head = Lit.Value.head_of_symbol () "Foo";
            props = [
              { key = "foo";
                value = Prim ((), Integer 4);
              };
            ];
          });
    }

let test_print_simple_pretty ctx =
  assert_print
    ~format: (Print.Format.Pretty 25)
    {|
Foo[foo: <Integer]: Foo[
  foo: >
]
Foo[foo: <]: Bar[]
---
Foo[foo: 4]?
|}
    { phases = [[
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), Integer)
                    };
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), []);
                    };
                  ];
                });
          };
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), Any)
                    };
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "Bar";
                  props = [];
                });
          };
        ]];
      query = Record ((),
          { head = Lit.Value.head_of_symbol () "Foo";
            props = [
              { key = "foo";
                value = Prim ((), Integer 4);
              };
            ];
          });
    }

let print_suite =
  "print" >:::
  [ "simple_basic" >:: test_print_simple_basic;
    "simple_pretty" >:: test_print_simple_pretty;
  ]
