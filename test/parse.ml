(* Derived from https://v1.realworldocaml.org/v1/en/html/parsing-with-ocamllex-and-menhir.html *)
open Core
open OUnit2
open Test_util
open Lib
open Lib.Ast

let token_to_string =
  function
  | Grammar.Symbol x -> sprintf "Symbol %S" x.value
  | Integer x -> sprintf "Integer %i" x.value
  | Float x -> sprintf "Float %F" x.value
  | String x -> sprintf "String %S" x.value
  | Regexp r -> sprintf "Regexp (parse_regexp %S)" (Regexp.to_string r.value)
  | Hash _ -> "Hash"
  | Dollar _ -> "Dollar"
  | Angle_up _ -> "Angle_up"
  | Angle_bwd _ -> "Angle_bwd"
  | Angle_fwd _ -> "Angle_fwd"
  | Colon -> "Colon"
  | Semicolon -> "Semicolon"
  | Phase_sep -> "Phase_sep"
  | Dash -> "Dash"
  | Ellipsis _ -> "Ellipsis"
  | Question -> "Question"
  | Open_bracket -> "Open_bracket"
  | Close_bracket _ -> "Close_bracket"
  | Open_brace _ -> "Open_brace"
  | Close_brace -> "Close_brace"
  | Eof -> "Eof"

let assert_lex exp inp =
  let lexbuf = Lexing.from_string inp
  and next_opt = ref None
  and act_buf = Buffer.create 16 in
  while not (!next_opt = Some Grammar.Eof) do
    let next = Lexer.read lexbuf in
    next_opt := Some next;
    Buffer.add_string act_buf (sprintf "%s\n" (token_to_string next))
  done;
  let act = String.strip (Buffer.contents act_buf) in
  assert_equal_str exp act

let assert_parse exp inp =
  assert_equal
    exp
    (Parse.Program.from_string inp |> Misc.Program.map_anns ~f: (fun _ -> ()))

let test_lex_query ctx =
  assert_lex {|
Symbol "Foo"
Open_bracket
Symbol "foo"
Colon
Integer 4
Close_bracket
Question
Eof
|} {|
Foo[foo: 4]?
|}

let test_parse_query ctx =
  assert_parse
    { phases = [];
      query = Record ((),
          { head = Lit.Value.head_of_symbol () "Foo";
            props = [
              { key = "foo";
                value = Prim ((), Integer 4);
              };
            ];
          });
    } {|
Foo[foo: 4]?
|}

let test_lex_simple ctx =
  assert_lex {|
Symbol "Foo"
Open_bracket
Symbol "foo"
Colon
Angle_bwd
Symbol "Integer"
Close_bracket
Colon
Symbol "Foo"
Open_bracket
Symbol "foo"
Colon
Angle_fwd
Close_bracket
Semicolon
Symbol "Foo"
Open_bracket
Symbol "foo"
Colon
Angle_bwd
Close_bracket
Colon
Symbol "Bar"
Open_bracket
Close_bracket
Semicolon
Phase_sep
Symbol "Foo"
Open_bracket
Symbol "foo"
Colon
Integer 4
Close_bracket
Question
Eof
|} {|
Foo[foo: <Integer]: Foo[foo: >];
Foo[foo: <]: Bar[];
---
Foo[foo: 4]?
|}

let test_parse_simple ctx =
  assert_parse
    { phases = [[
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), Integer)
                    };
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), []);
                    };
                  ];
                });
          };
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Foo";
                  props = [
                    { key = "foo";
                      value = Pipe ((), Any);
                    };
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "Bar";
                  props = [];
                });
          };
        ]];
      query = Record ((),
          { head = Lit.Value.head_of_symbol () "Foo";
            props = [
              { key = "foo";
                value = Prim ((), Integer 4);
              };
            ];
          });
    } {|
Foo[foo: <Integer]: Foo[foo: >];
Foo[foo: <]: Bar[];
---
Foo[foo: 4]?
|}

let parse_suite =
  "parse" >:::
  [ "lex_query" >:: test_lex_query;
    "query" >:: test_parse_query;
    "lex_simple" >:: test_lex_simple;
    "simple" >:: test_parse_simple;
  ]
