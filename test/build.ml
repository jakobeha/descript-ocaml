open OUnit2
open Test_util
open Lib
open Lib.Ast

exception Bad_example_form of string

type macro_example =
  { macro_phase : string;
    before_phase : string;
    after_phase : string;
    format : Print.format;
    time : test_length;
  }

type eval_example =
  { program : string;
    eval : string;
    format : Print.format;
    test_format : bool;
    time : test_length;
  }

type example =
  | Macro of macro_example
  | Eval of eval_example

let examples_dir =
  "../test-resources"

let examples_rsvr = Import.Dep_resolver.at_dir examples_dir

let simplify_format_general x =
  x |>
  Str.global_replace (Str.regexp "//.*$") "" |>
  Str.global_replace (Str.regexp "/\\*[^*]*\\*/") ""

let simplify_format x ~format: format =
  match format with
  | Print.Format.Basic ->
    x |>
    simplify_format_general |>
    Str.global_replace (Str.regexp "\\[\n[ \t]*") "[" |>
    Str.global_replace (Str.regexp "\n[ \t]*\\]") "]" |>
    Str.global_replace (Str.regexp ":\n[ \t]*") ": " |>
    Str.global_replace (Str.regexp "\n\n+") "\n" |>
    Str.global_replace (Str.regexp "\n[ \t]+") " "
  | Print.Format.Pretty _ ->
    simplify_format_general x

let assert_format x ~format: format =
  try assert_equal_str
        (simplify_format x ~format: format)
        (Build.Program.reformat x ~format: format) with
  | Parse.Error err ->
    assert_failure ("Parse error: " ^ Parse.Error.to_string err)

let assert_evaluate exp x ~format: format =
  try assert_equal_str
        (simplify_format exp ~format: format)
        (Build.Program.evaluate x ~rsvr: examples_rsvr ~format: format) with
  | Parse.Error err ->
    assert_failure ("Parse error: " ^ Parse.Error.to_string err)

let assert_reduce_phase mp exp x ~format: format =
  let mp = Parse.Phase.from_string mp |> Misc.Phase.map_anns ~f: (fun _ -> ()) in
  try assert_equal_str
        (simplify_format exp ~format: format)
        (Build.Phase.apply_macros mp x ~rsvr: examples_rsvr ~format: format) with
  | Parse.Error err ->
    assert_failure ("Parse error: " ^ Parse.Error.to_string err)

let test_format (e : example) ctx =
  match e with
  | Eval e ->
    if e.test_format then
      assert_format e.program ~format: e.format
  | Macro e -> ()

let test_evaluate (e : example) =
  match e with
  | Eval e ->
    (fun ctx -> assert_evaluate e.eval e.program ~format: e.format) |>
    test_case ~length: e.time
  | Macro e ->
    (fun ctx -> assert_reduce_phase e.macro_phase e.after_phase e.before_phase ~format: e.format) |>
    test_case ~length: e.time

let parse_format (str : string) =
  if String.is_substring str "#pretty 80" then
    Print.Format.Pretty 80
  else if String.is_substring str "#pretty" then
    Print.Format.Pretty 100
  else
    Print.Format.Basic

let parse_example (str : string) =
  match Str.split_delim (Str.regexp_string "\n===\n") str with
  | [pr; ev] ->
    Eval
      { program = pr;
        eval = ev;
        format = parse_format pr;
        test_format = not (String.is_substring pr "#no-format");
        time = Custom_length 10.;
      }
  | [ma; be; af] ->
    Macro
      { macro_phase = ma;
        before_phase = be;
        after_phase = af;
        format = parse_format ma;
        time = Custom_length 10.;
      }
  | _ -> raise (Bad_example_form str)

let last_example_id : char =
  'z'

let build_suite =
  let examples =
    Sys.readdir examples_dir |>
    Array.to_list |>
    List.filter ~f: (fun n -> n <> "imported") |>
    List.map ~f: (fun n -> (n, Filename.concat examples_dir n)) |>
    List.map ~f: (fun (n, p) -> (n, String.read_file p)) |>
    List.filter ~f: (fun (n, p) -> String.get n 0 <= last_example_id) |>
    List.map ~f: (fun (n, c) -> (n, parse_example c))
  in
  "build" >:::
  [ "format" >::: List.map ~f: (fun (n, e) -> n >:: test_format e) examples;
    "evaluate" >::: List.map ~f: (fun (n, e) -> n >: test_evaluate e) examples;
  ]
