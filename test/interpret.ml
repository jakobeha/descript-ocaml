open OUnit2
open Lib
open Lib.Ast

let assert_interpret exp x =
  assert_equal exp (Interpret.Slow.Pure.Program.interpret x)

let test_interpret_simple ctx =
  assert_interpret
    (Record ((),
       { head = Lit.Value.head_of_symbol () "Foo";
         props = [
           { key = "foo";
             value = Record ((),
                 { head = Lit.Value.head_of_symbol () "Bar";
                   props = [];
                 });
           };
           { key = "bar";
             value = Prim ((), Integer 5);
           };
           { key = "baz";
             value = Prim ((), String "Hello");
           };
         ];
       }))
    { phases = [[
          { input = Record ((),
                { head = Lit.Value.head_of_symbol () "Baz";
                  props = [
                    { key = "baz";
                      value = Pipe ((), Prim)
                    }
                  ];
                });
            output = Record ((),
                { head = Lit.Value.head_of_symbol () "#Add";
                  props = [
                    { key = "left";
                      value = Prim ((), Integer 1);
                    };
                    { key = "right";
                      value = Pipe ((), [Property "baz"]);
                    }
                  ];
                })
          };
        ]];
      query = Record ((),
          { head = Lit.Value.head_of_symbol () "Foo";
            props = [
              { key = "foo";
                value = Record ((),
                    { head = Lit.Value.head_of_symbol () "Bar";
                      props = [];
                    });
              };
              { key = "bar";
                value = Record ((),
                    { head = Lit.Value.head_of_symbol () "Baz";
                      props = [
                        { key = "baz";
                          value = Prim ((), Integer 4);
                        }
                      ];
                    });
              };
              { key = "baz";
                value = Prim ((), String "Hello");
              };
            ];
          });
    }

let interpret_suite =
  "interpret" >:::
  [ "simple" >:: test_interpret_simple; ]
