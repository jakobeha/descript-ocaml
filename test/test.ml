open OUnit2
open Parse
open Interpret
open Print
open Build

let suite =
  test_list
    [ parse_suite;
      interpret_suite;
      print_suite;
      build_suite
    ]

let _ =
  run_test_tt_main suite
