open OUnit2
include
  ( Util : module type of Util with
    module String := Util.String
  )

module String = struct
  include Util.String

  let read_file (p : string) : string =
    let ic = open_in p in
    really_input_string ic (in_channel_length ic)
end

let assert_equal_str exp act =
  assert_equal ~printer: (fun x -> x) (String.strip exp) act
