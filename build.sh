# Copy extra resources to proper locations
for buildLoc in _build/install _build/default; do
  rm -r "$buildLoc/pylib"
  rm -r "$buildLoc/test-resources"
  cp -r "pylib" "$buildLoc/pylib"
  cp -r "test-resources" "$buildLoc/test-resources"
done

# Actually build
jbuilder $* -f

cp "_build/default/bin/main.exe" "/Users/jakob/.local/bin/descript"
