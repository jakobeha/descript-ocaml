from greenery.lego import lego, parse


def _lego_universal(self):
    return self.everythingbut().empty()


def _lego_issubset(self, other):
    return self.to_fsm().issubset(other.to_fsm())


def _lego_isdisjoint(self, other):
    return self.to_fsm().isdisjoint(other.to_fsm())


lego.universal = _lego_universal
lego.issubset = _lego_issubset
lego.isdisjoint = _lego_isdisjoint
