open Base

exception Child_not_found of string
exception Unexpected_remainder_path_elem
(** Occurs when trying to encode an expression in a value which doesn't
    support it, e.g. a remainder in a regular value. *)
exception Cannot_represent of string

module Symbol = struct
  type t = Ast.symbol

  let valid_regexp =
    Str.regexp "#?[a-zA-Z_][a-zA-Z0-9_]*$"

  let is_valid x =
    Str.string_match valid_regexp x 0

  let from_bool x =
    match x with
    | false -> "False"
    | true -> "True"

  let index_char idx =
    assert (idx >= 0 && idx < 26);
    Char.of_int_exn (Char.to_int 'a' + idx)

  let rec from_index_pre idx =
    if idx >= 26 then from_index (idx / 26) else ""
  and from_index idx =
    from_index_pre idx ^ Char.to_string (index_char (idx mod 26))
end

module Path_elem = struct
  include Ast.Path_elem

  let debug_string x =
    match x with
    | Head -> "^"
    | Property k -> k
    | Remainder subs ->
      ("..." :: subs) |>
      String.concat ~sep: "-"
end

module Property = struct
  include Ast.Property

  let is_remainder x =
    x.key = "..."

  let indexed idx x =
    { key = Symbol.from_index idx;
      value = x;
    }

  let fill_idxs xs =
    List.mapi xs ~f: (fun idx x ->
      if x.key = "{index}" then
        indexed idx x.value
      else
        x)

  let map x ~f: f =
    { key = x.key;
      value = f (x.value);
    }

  let is_equal1 x y ~sub: f =
    x.key = y.key && f x.value y.value

  let align_static xs ys =
      if List.map xs (fun p -> p.key) =
         List.map ys (fun p -> p.key) then
        Some (xs, ys)
      else
        None

  let reify_center_remainder cx cy xys =
    match xys with
    | Either3.Left xs ->
      ( xs,
        List.map xs
        ( fun x ->
          { key = x.key;
            value = cy.value;
          }
        )
      )
    | Right ys ->
      ( List.map ys
        ( fun y ->
          { key = y.key;
            value = cx.value;
          }
        ),
        ys
      )
    | Neither -> ([], [])

  let align_dynamic (lxs, cx, rxs) (lys, cy, rys) =
    let (llxys, clxys) = List.zip_leftover lxs lys
    and (rrxys, crxys) = List.zip_leftover rxs rys in
    if List.for_all llxys (fun (pi, px) -> pi.key = px.key) &&
       List.for_all rrxys (fun (pi, px) -> pi.key = px.key) then
      let (llxs, llys) = List.unzip llxys
      and (rrxs, rrys) = List.unzip rrxys
      and (clxs, clys) = reify_center_remainder cx cy clxys
      and (crxs, crys) = reify_center_remainder cx cy crxys in
      let xs = llxs @ clxs @ [cx] @ crxs @ rrxs
      and ys = llys @ clys @ [cy] @ crys @ rrys in
      Some (xs, ys)
    else
      None

  let align xs ys =
    match ( List.split_on xs is_remainder,
            List.split_on ys is_remainder
          ) with
    | (None, None) -> align_static xs ys
    | (Some _, None)
    | (None, Some _) -> None
    | (Some xs, Some ys) -> align_dynamic xs ys

  module Of_monad (V : Monad.Type) = struct
    let all x =
      let module Let_syntax = V in
      let%map value = x.value in
      { key = x.key;
        value = value;
      }
  end
end

module Record = struct
  include Ast.Record

  module Of_monad (V : Monad.Type) = struct
    module Let_syntax = V
    module V_property = Property.Of_monad(V)
    module V_list = List.Of_monad(V)
    module V_option = Option.Of_monad(V)

    let all x =
      let%map head = x.head
      and props =
        x.props |>
        List.map ~f: V_property.all |>
        V_list.all in
      { head = head;
        props = props;
      }
  end

  module Traverse (M : Ast.Nest_m.Type) = struct
    module Let_syntax = M
    module M_list = List.Of_monad(M)
    module M_option = Option.Of_monad(M)
    module M_property = Property.Of_monad(M)

    let surface x ~f: f =
      let%map head = M.nest Head (f x.head)
      and props =
        x.props |>
        List.map ~f: (fun prop ->
            M.nest
              (Property prop.Property.key)
              (prop |> Property.map ~f: f |> M_property.all)) |>
        M_list.all in
      { head = head;
        props = props;
      }

    let head x ~f: f =
      let%map head = M.nest Head (f x.head) in
      { head = head;
        props = x.props;
      }
  end

  let map x ~f: f =
    { head = f x.head;
      props = List.map x.props (Property.map ~f: f);
    }

  let is_equal1 x y ~sub: f =
    f x.head y.head &&
    List.length x.props = List.length y.props &&
    List.for_all2_exn x.props y.props (Property.is_equal1 ~sub: f)

  let head_location x =
    (Ast.Path_elem.Head, x)

  let prop_imm_location all x =
    if x.Ast.Property.key = "..." then
      let other_keys =
        all |>
        List.map ~f: (fun p -> p.Ast.Property.key) |>
        List.filter ~f: (fun k -> k <> "...") in
      (Ast.Path_elem.Remainder other_keys, x.value)
    else
      (Ast.Path_elem.Property x.Property.key, x.value)

  let imm_locations x =
    head_location x.head ::
    List.map x.props (prop_imm_location x.props)

  let at_elem e x =
    match e with
    | Ast.Path_elem.Head -> Some x.head
    | Property k ->
      x.props |>
      List.find ~f: (fun p -> k = p.Ast.Property.key) |>
      Option.map ~f: (fun p -> p.Ast.Property.value)
    | Remainder _ -> raise Unexpected_remainder_path_elem

  let at_elem_exn e x =
    match at_elem e x with
    | None ->
      raise (Child_not_found (Path_elem.debug_string e))
    | Some r -> r

  let set_at_elem_exn e x ~n: n =
    match e with
    | Ast.Path_elem.Head ->
      { head = n;
        props = x.props;
      }
    | Property k ->
      if not (List.exists x.props ~f: (fun p -> k = p.key)) then
        raise (Child_not_found (Path_elem.debug_string e))
      else
        { head = x.head;
          props =
            List.map x.props ~f:
            ( fun p ->
                if (k <> p.key) then
                  p
                else
                  { key = p.key;
                    value = n;
                  }
            );
        }
    | Remainder subs ->
      { head = x.head;
        props =
          List.map x.props ~f:
          ( fun p ->
              if (List.mem subs p.key ~equal: (=)) then
                p
              else
                { key = p.key;
                  value = n;
                }
          );
      }

  let map_at_elem_exn e x ~f: f =
    match e with
    | Ast.Path_elem.Head ->
      { head = f x.head;
        props = x.props;
      }
    | Property k ->
      if not (List.exists x.props ~f: (fun p -> k = p.key)) then
        raise (Child_not_found (Path_elem.debug_string e))
      else
        { head = x.head;
          props =
            List.map x.props ~f:
            ( fun p ->
                if (k <> p.key) then
                  p
                else
                  { key = p.key;
                    value = f p.value;
                  }
            );
        }
    | Remainder subs ->
      { head = x.head;
        props =
          List.map x.props ~f:
          ( fun p ->
              if (List.mem subs p.key ~equal: (=)) then
                p
              else
                { key = p.key;
                  value = f p.value;
                }
          );
      }
end

module Nest_m = struct
  module Simple (M : Monad.Type) = struct
    include M

    let nest _ x = x
  end

  module Ident = Simple(Monad.Ident)
end

module Value = struct
  include (
    Ast.Value : module type of Ast.Value
    with module Reg := Ast.Value.Reg
    and module In := Ast.Value.In
    and module Out := Ast.Value.Out
  )

  module Gen_traverse (M : Monad.Type) (M_record : sig
    val surface : 'a Ast.record -> f: ('a -> 'b M.t) -> 'b Ast.record M.t

    (** Still applies nesting. *)
    val head : 'a Ast.record -> f: ('a -> 'a M.t) -> 'a Ast.record M.t
  end) = struct
    module Let_syntax = M

    let imm x ~f: f =
      match x with
      | Prim (ann, x) ->
        M.return (Prim (ann, x))
      | Record (ann, x) ->
        let%map r = M_record.surface x f in
        Record (ann, r)
      | Pipe (ann, x) ->
        M.return (Pipe (ann, x))

    let imm_ann x ~f: f =
      match x with
      | Prim (ann, x) ->
        let%map r_ann = f ann in
        Prim (r_ann, x)
      | Record (ann, x) ->
        let%map r_ann = f ann in
        Record (r_ann, x)
      | Pipe (ann, x) ->
        let%map r_ann = f ann in
        Pipe (r_ann, x)

    let rec up x ~f: f =
      x |>
      imm ~f: (up ~f: f) |>
      M.bind ~f: f

    let rec down x ~f: f =
      x |>
      f |>
      M.bind ~f: (imm ~f: (down ~f: f))

    let rec pipes_to_vals x ~f: f =
      match x with
      | Prim (ann, x) ->
        M.return (Prim (ann, x))
      | Record (ann, x) ->
        let%map r = M_record.surface x (pipes_to_vals ~f: f) in
        Record (ann, r)
      | Pipe (ann, x) -> f ann x

    let rec pipes x ~f: f =
      match x with
      | Prim (ann, x) ->
        M.return (Prim (ann, x))
      | Record (ann, x) ->
        let%map r = M_record.surface x (pipes ~f: f) in
        Record (ann, r)
      | Pipe (ann, x) ->
        let%map r = f x in
        Pipe (ann, r)

    let rec pipes_annd x ~f: f =
      match x with
      | Prim (ann, x) ->
        M.return (Prim (ann, x))
      | Record (ann, x) ->
        let%map r = M_record.surface x (pipes_annd ~f: f) in
        Record (ann, r)
      | Pipe (ann, x) ->
        let%map r = f ann x in
        Pipe (ann, r)

    let rec heads x ~f: f =
      match x with
      | Prim (ann, x) ->
        M.return (Prim (ann, x))
      | Record (ann, x) ->
        let%bind r = M_record.surface x (heads ~f: f) in
        let%map r = M_record.head r ~f: f in
        Record (ann, r)
      | Pipe (ann, x) ->
        M.return (Pipe (ann, x))

    let rec anns x ~f: f =
      match x with
      | Prim (ann, x) ->
        let%map r_ann = f ann in
        Prim (r_ann, x)
      | Record (ann, x) ->
        let%map r_ann = f ann
        and r = M_record.surface x (anns ~f: f) in
        Record (r_ann, r)
      | Pipe (ann, x) ->
        let%map r_ann = f ann in
        Pipe (r_ann, x)
  end

  module Traverse (M : Ast.Nest_m.Type) = Gen_traverse(M)(Record.Traverse(M))

  module Fold (R : Monoid.Type) = struct
    module R_writer = Writer.Make(R)
    module R_writer_traverse = Traverse(Nest_m.Simple(R_writer))

    let up x ~f: f =
      let (res, _) = R_writer_traverse.up x (fun x ->
          R_writer.tell_and_return (f x) x) in
      res

    let anns x ~f: f =
      let (res, _) = R_writer_traverse.anns x (fun x ->
          R_writer.tell_and_return (f x) x) in
      res
  end

  module Map = Traverse(Nest_m.Ident)

  let annotation x =
    match x with
    | Prim (ann, _) -> ann
    | Record (ann, _) -> ann
    | Pipe (ann, _) -> ann

  let rec map x ~f: f =
    match x with
    | Prim (ann, x) -> Prim (ann, x)
    | Record (ann, x) -> Record (ann, Record.map x (map ~f: f))
    | Pipe (ann, x) -> Pipe (ann, f x)

  let has_pipe x =
    let module And_fold = Fold(Bool.And) in
    And_fold.up x
    ( function
      | Prim _ -> false
      | Record _ -> false
      | Pipe _ -> true
    )

  let rec is_equiv x y =
    match (x, y) with
    | (Prim (_, x), Prim (_, y)) -> x = y
    | (Record (_, x), Record (_, y)) ->
      Record.is_equal1 x y ~sub: is_equiv
    | (Pipe (_, x), Pipe (_, y)) -> x = y
    | (_, _) -> false

  let rec sub_locations (e, chl) =
    List.map (locations chl) (fun (l, dec) -> (e :: l, dec))
  and child_locations x =
    match x with
    | Record (_, x) ->
      List.concat_map (Record.imm_locations x) sub_locations
    | _ -> []
  and locations x =
    ([], x) :: child_locations x

  let locations_of tr_ch x =
    x |>
    locations |>
    List.filter ~f: (fun (l, v) -> tr_ch = v) |>
    List.map ~f: (fun (l, _) -> l)

  let at_elem e v =
    match v with
    | Record (_, v) -> Record.at_elem e v
    | _ -> None

  let at_elem_exn e v =
    match at_elem e v with
    | None ->
      raise (Child_not_found (Path_elem.debug_string e))
    | Some r -> r

  let set_at_elem_exn e v ~n: n =
    match v with
    | Record (ann, v) -> Record (ann, Record.set_at_elem_exn e v ~n: n)
    | _ ->
      raise (Child_not_found (Path_elem.debug_string e))

  let map_at_elem_exn e v ~f: f =
    match v with
    | Record (ann, v) -> Record (ann, Record.map_at_elem_exn e v ~f: f)
    | _ ->
      raise (Child_not_found (Path_elem.debug_string e))

  let rec at_path_exn l v =
    match l with
    | [] -> v
    | e :: es ->
      at_elem_exn e v |>
      at_path_exn es

  let rec set_at_path_exn l v ~n: n =
    match l with
    | [] -> n
    | e :: es ->
      map_at_elem_exn e v ~f: (set_at_path_exn es ~n: n)

  let rec map_at_path_exn l v ~f: f =
    match l with
    | [] -> f v
    | e :: es ->
      map_at_elem_exn e v ~f: (map_at_path_exn es ~f: f)
end

module Reducer = struct
  include Ast.Reducer

  let map_values x ~f: f =
    { input = f.Ast.Value.Poly_fun.f x.input;
      output = f.f x.output;
    }

  let map_anns x ~f: f =
    { input = Value.Map.anns x.input f;
      output = Value.Map.anns x.output f;
    }
end

module Phase = struct
  include Ast.Phase

  let map_values x ~f: f =
    List.map x (Reducer.map_values ~f: f)

  let map_anns x ~f: f =
    List.map x (Reducer.map_anns ~f: f)

  let app_phases xs ys =
    List.zip_max_right xs ys (@)

  let join_phases xss = List.concat xss
end

module Program = struct
  include Ast.Program

  let map_anns x ~f: f =
    { phases = List.map x.phases (Phase.map_anns ~f: f);
      query = Value.Map.anns x.query f;
    }
end
