module Phase : sig
  val apply_macros :
    Ast.Pure.phase ->
    string ->
    rsvr: Import.dep_resolver ->
    format: Print.format ->
    string
end

module Program : sig
  val reformat : string -> format: Print.format -> string

  val evaluate :
    string ->
    rsvr: Import.dep_resolver ->
    format: Print.format ->
    string

  val evaluate_file : string -> format: Print.format -> string

  val apply_macros :
    Ast.Pure.phase ->
    string ->
    rsvr: Import.dep_resolver ->
    format: Print.format ->
    string

  val apply_macros_file :
    Ast.Pure.phase ->
    string ->
    format: Print.format ->
    string
end
