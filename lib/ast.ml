open Base

(** Identifiers - e.g. record heads, properties, import statemenets. *)
module Symbol = struct
  type t = string
    [@@deriving compare, hash, sexp]
end

type symbol = Symbol.t

(** AST leaf. Built-in data structure. *)
module Prim = struct
  module T = struct
    type t =
      | Integer of int
      | Float of float
      | String of string
      [@@deriving compare, hash, sexp]
  end

  include T
  include Comparable.Make(T)
  include Hashable.Make(T)
  type 'a table = 'a Table.t
end

type prim = Prim.t

(** Key-value entry. *)
module Property = struct
  type 'v t =
    { key : Symbol.t;
      value : 'v;
    }
    [@@deriving compare, hash, sexp]

  module Make (V : sig type t end) = struct
    type _t = V.t t
    type t = _t
  end
end

type 'v property = 'v Property.t

(** AST branch. Has a head and properties. *)
module Record = struct
  type 'v t =
    { head : 'v;
      props : 'v Property.t list;
    }
    [@@deriving compare, hash, sexp]

  module Make (V : sig type t end) = struct
    type _t = V.t t
    type t = _t
  end
end

type 'v record = 'v Record.t

(** Input pipe - allows a reducer to match multiple values. *)
module Matcher = struct
  module T = struct
    type t =
      | Any
      | Prim
      | Record
      | Integer
      | Float
      | String
      | Regexp of Regexp.t
      [@@deriving compare, hash, sexp]
  end

  include T
  include Comparable.Make(T)
  include Hashable.Make(T)
  type 'a table = 'a Table.t
end

type matcher = Matcher.t

(** AST edge. Refers to an immediate child. *)
module Path_elem = struct
  type t =
    | Head
    | Property of Symbol.t
    | Remainder of Symbol.t list
    [@@deriving compare, hash, sexp]
end

type path_elem = Path_elem.t

(** AST path. Refers to a child (e.g. record property). *)
module Path = struct
  type t = Path_elem.t list
    [@@deriving compare, hash, sexp]
end

type path = Path.t

module Nest_m = struct
  module type Type = sig
    include Monad.Type

    val nest : path_elem -> 'a t -> 'a t
  end
end

(** An abstraction. *)
module Pipe = struct
  type t =
    | Matcher of Matcher.t
    | Path of Path.t
    [@@deriving compare, hash, sexp]
end

type pipe = Pipe.t

(** AST node. Encodes data. *)
module Value = struct
  type ('ann, 'abs) t =
    | Prim of 'ann * Prim.t
    | Record of 'ann * ('ann, 'abs) t Record.t
    | Pipe of 'ann * 'abs
    [@@deriving compare, hash, sexp]

  (** A polymorphic function which takes and returns any type of value. *)
  module Poly_fun = struct
    type ('ann, 'abs) _t = ('ann, 'abs) t
    type ('a_ann, 'b_ann) t =
      { f : 'abs. ('a_ann, 'abs) _t -> ('b_ann, 'abs) _t;
      }
  end

  type ('a_ann, 'b_ann) poly_fun = ('a_ann, 'b_ann) Poly_fun.t

  module Make (Abs : sig
    type t

    val compare : t -> t -> int
    val hash_fold_t : Hash.state -> t -> Hash.state
    include Sexpable.S with type t := t
  end) = struct
    type 'ann _t = ('ann, Abs.t) t
      [@@deriving compare, hash, sexp]
    type 'ann t = 'ann _t
      [@@deriving compare, hash, sexp]
  end

  (** Encodes concrete data. *)
  module Reg = Make(Nothing)
  (** A set of values. *)
  module In = Make(Matcher)
  (** A function from a set of values to a single value. *)
  module Out = Make(Path)
  (** Encodes any typpe of value. *)
  module Max = Make(Pipe)
end

type ('ann, 'abs) value = ('ann, 'abs) Value.t
type 'ann reg_value = 'ann Value.Reg.t
type 'ann in_value = 'ann Value.In.t
type 'ann out_value = 'ann Value.Out.t
type 'ann max_value = 'ann Value.Max.t

(** Transforms a value. *)
module Reducer = struct
  type 'ann t =
    { input : 'ann Value.In.t;
      output : 'ann Value.Out.t;
    }
    [@@deriving compare, hash, sexp]
end

type 'ann reducer = 'ann Reducer.t

(** A set of reducers. Transforms a value arbitrarily. *)
module Phase = struct
  type 'ann t = 'ann Reducer.t list
    [@@deriving compare, hash, sexp]
end

type 'ann phase = 'ann Phase.t

(** An entire source file. Calculates a value. *)
module Program = struct
  type 'ann t =
    { phases : 'ann Phase.t list;
      query : 'ann Value.Reg.t;
    }
    [@@deriving compare, hash, sexp]
end

type 'ann program = 'ann Program.t

module Annotate (Ann : sig type t end) = struct
  type ann = Ann.t

  module Value = struct
    type 'abs t = (ann, 'abs) Value.t

    module Make (Abs : sig
      type t

      val compare : t -> t -> int
      val hash_fold_t : Hash.state -> t -> Hash.state
      include Sexpable.S with type t := t
    end) = struct
      type t = ann Value.Make(Abs).t
    end

    module Reg = Make(Nothing)
    module In = Make(Matcher)
    module Out = Make(Path)
    module Max = Make(Pipe)
  end

  type 'abs value = 'abs Value.t
  type reg_value = Value.Reg.t
  type in_value = Value.In.t
  type out_value = Value.Out.t
  type max_value = Value.Max.t

  module Reducer = struct
    type t = ann Reducer.t
  end

  type reducer = Reducer.t

  module Phase = struct
    type t = ann Phase.t
  end

  type phase = Phase.t

  module Program = struct
    type t = ann Program.t
  end

  type program = Program.t
end

module Pure = Annotate(Unit)
module Taint = Annotate(Taint)
