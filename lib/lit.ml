open Core

module Ann = struct
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    val import_wrap : t -> t

    val import_head : t -> t

    val import_location : t -> t

    val bool_head : t -> t

    val property_head : t -> t

    val property_key : t -> t

    (** First input annotation is the full list, second is the first item.
        First output annotation is the cons head, second is the rest list. *)
    val sub_cons : t -> t -> t * t

    val nil_head : t -> t
  end

  module Pure = struct
    module T = Unit

    type t = T.t

    let import_wrap () = ()

    let import_head () = ()

    let import_location () = ()

    let bool_head () = ()

    let property_head () = ()

    let property_key () = ()

    let sub_cons () () = ((), ())

    let nil_head () = ()
  end

  module Taint = struct
    module T = Taint

    type t = T.t

    let import_wrap x = x

    let import_head x = x

    let import_location x = x

    let bool_head x = x

    let property_head x = x

    let property_key x = x

    let sub_cons xs xf =
      ( { Taint.range = xf.Taint.range;
          is_literal = false;
          is_tainted = xf.is_tainted;
        },
        { Taint.range =
            { start = xf.Taint.range.start;
              after = xs.Taint.range.after;
            };
          is_literal = false;
          is_tainted = xf.is_tainted || xs.is_tainted;
        }
      )

    let nil_head x = x
  end
end

module Value = struct
  include Ast.Value

  exception Bad_form of string

  let head_of_symbol ann s =
    Prim (ann, String s)

  let try_symbol_of_head h =
    match h with
    | Prim (_, String s) ->
      if Misc.Symbol.is_valid s then
        Some s
      else
        None
    | _ -> None

  let symbol_of_head h =
    match try_symbol_of_head h with
    | None -> raise (Bad_form "head symbol")
    | Some x -> x

  let to_import_record x =
    match (try_symbol_of_head x.Ast.Record.head, x.props) with
    | ( Some "Import",
        [ { key = "a";
            value = x;
          };
          { key = "from";
            value = Prim (_, String loc);
          };
        ]
      ) -> (loc, x)
    | _ -> raise (Bad_form "import")

  let to_import x =
    match x with
    | Ast.Value.Record (_, x) -> to_import_record x
    | _ -> raise (Bad_form "import")

  let is_import_head x =
    match try_symbol_of_head x with
    | Some "Import" -> true
    | _ -> false

  let is_import x =
    try let _ = to_import x in true with
    | Bad_form "import" -> false

  let to_string x =
    match x with
    | Ast.Value.Prim (_, String x) -> x
    | _ -> raise (Bad_form "string")

  let is_inj_head x =
    match x with
    | Ast.Value.Prim (_, String x) ->
      String.is_prefix x ~prefix: "#"
    | _ -> false

  let to_inj_head x =
    match x |> to_string |> String.chop_prefix ~prefix: "#" with
    | None ->
      raise (Bad_form "inj_head")
    | exception (Bad_form "string") ->
      raise (Bad_form "inj_head")
    | Some res -> res

  let is_import x =
    try let _ = to_import x in true with
    | Bad_form "import" -> false

  let to_property_record x =
    match (try_symbol_of_head x.Ast.Record.head, x.props) with
    | ( Some "Property",
        [ { key = "key";
            value = Prim (_, String key);
          };
          { key = "value";
            value = value;
          };
        ]
      ) ->
      { Ast.Property.key = key;
        value = value;
      }
    | _ -> raise (Bad_form "property")

  let to_property x =
    match x with
    | Ast.Value.Record (_, x) -> to_property_record x
    | _ -> raise (Bad_form "property")

  let is_list_head x =
    match try_symbol_of_head x with
    | Some "Nil"
    | Some "Cons" -> true
    | _ -> false

  let rec to_list_record x =
    match (try_symbol_of_head x.Ast.Record.head, x.props) with
    | (Some "Nil", []) -> []
    | ( Some "Cons",
        [ { key = "first";
            value = x;
          };
          { key = "rest";
            value = xs;
          };
        ]
      ) -> x :: to_list xs
    | _ -> raise (Bad_form "list")
  and to_list x =
    match x with
    | Ast.Value.Record (_, x) -> to_list_record x
    | _ -> raise (Bad_form "list")
end

module Annotate (Ann : Ann.Type) = struct
  type ann = Ann.t

  module Ann_ast = Ast.Annotate(Ann.T)

  module Value = struct
    include Ann_ast.Value

    let import ann loc x =
      Ast.Value.Record
        ( ann,
          { head =
              Value.head_of_symbol
                (Ann.import_head ann)
                "Import";
            props =
              [ { key = "a";
                  value = x;
                };
                { key = "from";
                  value =
                    Ast.Value.Prim
                      ( Ann.import_location ann,
                        String loc
                      );
                };
              ]
          }
        )

    let imported loc x =
      import (Ann.import_wrap (Misc.Value.annotation x)) loc x

    let bool ann x =
      Ast.Value.Record
        ( ann,
          { head =
              Value.head_of_symbol
                (Ann.bool_head ann)
                (Misc.Symbol.from_bool x);
            props = [];
          }
        )

    let property ann x =
      Ast.Value.Record
        ( ann,
          { head =
              Value.head_of_symbol
                (Ann.property_head ann)
                "Property";
            props =
              [ { key = "key";
                  value =
                    Value.head_of_symbol
                      (Ann.property_key ann)
                      x.Ast.Property.key;
                };
                { key = "value";
                  value = x.value;
                };
              ];
          }
        )

    let rec list_init ann xs f_tail =
      match xs with
      | [] -> f_tail ann
      | x :: xs ->
        let x_ann = Misc.Value.annotation x in
        let (r_head_ann, r_next_ann) = Ann.sub_cons ann x_ann in
        Ast.Value.Record
          ( ann,
            { head = Value.head_of_symbol r_head_ann "Cons";
              props = [
                { key = "first";
                  value = x;
                };
                { key = "rest";
                  value = list_init r_next_ann xs f_tail;
                }
              ];
            }
          )

    let nil ann =
      Ast.Value.Record
        ( ann,
          { Ast.Record.head =
              Value.head_of_symbol
                (Ann.nil_head ann)
                "Nil";
            props = [];
          }
        )

    let list ann xs =
      list_init ann xs nil
  end
end

module Pure = Annotate(Ann.Pure)
module Taint = Annotate(Ann.Taint)
