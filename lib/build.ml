open Core
open Ast

module Phase = struct
  let apply_macros mp phs ~rsvr: rsvr ~format: format =
    let mp = Interpret.Slow.Pure.Phase.from_slow mp in
    let module Phs = struct let x = phs end in
    let module Print_phs = Print.Taint(Phs) in
    phs |>
    Parse.Phase.from_string |>
    Import.Phase.map_imports
      ~f: (List.map ~f: (Interpret.Slow.Taint.Phase.reduce mp))
      ~rsvr: rsvr
      ~format: format |>
    Interpret.Slow.Taint.Phase.reduce mp |>
    Print_phs.Phase.to_string ~format: format
end

module Program = struct
  let reformat prg ~format: format =
    prg |>
    Parse.Program.from_string |>
    Misc.Program.map_anns ~f: (fun _ -> ()) |>
    Print.Pure.Program.to_string ~format: format

  let evaluate prg ~rsvr: rsvr ~format: format =
    prg |>
    Parse.Program.from_string |>
    Misc.Program.map_anns ~f: (fun _ -> ()) |>
    Import.Pure.Program.resolve_imports ~rsvr: rsvr |>
    Interpret.Slow.Pure.Program.interpret |>
    Print.Pure.Value.Reg.to_string ~format: format

  let evaluate_file prg ~format: format =
    let rsvr = Import.Dep_resolver.for_file prg in
    prg |>
    Parse.Program.from_file |>
    Misc.Program.map_anns ~f: (fun _ -> ()) |>
    Import.Pure.Program.resolve_imports ~rsvr: rsvr |>
    Interpret.Slow.Pure.Program.interpret |>
    Print.Pure.Value.Reg.to_string ~format: format

  let apply_macros mp prg ~rsvr: rsvr ~format: format =
    let mp = Interpret.Slow.Pure.Phase.from_slow mp in
    let module Prg = struct let x = prg end in
    let module Print_prg = Print.Taint(Prg) in
    prg |>
    Parse.Program.from_string |>
    Import.Program.map_imports
      ~f: (List.map ~f: (Interpret.Slow.Taint.Phase.reduce mp))
      ~rsvr: rsvr
      ~format: format |>
    Interpret.Slow.Taint.Program.macro_reduce mp |>
    Print_prg.Program.to_string ~format: format

  let apply_macros_file mp prg ~format: format =
    let mp = Interpret.Slow.Pure.Phase.from_slow mp
    and rsvr = Import.Dep_resolver.for_file prg in
    let module Prg = struct let x = In_channel.read_all prg end in
    let module Print_prg = Print.Taint(Prg) in
    prg |>
    Parse.Program.from_file |>
    Import.Program.map_imports
      ~f: (List.map ~f: (Interpret.Slow.Taint.Phase.reduce mp))
      ~rsvr: rsvr
      ~format: format |>
    Interpret.Slow.Taint.Program.macro_reduce mp |>
    Print_prg.Program.to_string ~format: format
end
