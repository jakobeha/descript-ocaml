open Base

exception Child_not_found of string
exception Unexpected_remainder_path_elem
(** Occurs when trying to encode an expression in a value which doesn't
    support it, e.g. a remainder in a regular value. *)
exception Cannot_represent of string

module Symbol : sig
  type t = Ast.symbol

  val valid_regexp : Str.regexp

  val is_valid : t -> bool

  val from_bool : bool -> t

  val from_index : int -> t
end

module Path_elem : sig
  type t = Ast.path_elem

  val debug_string : t -> string
end

module Property : sig
  type 'v t = 'v Ast.property

  val is_remainder : 'v t -> bool

  (** Encodes a value at a particular index "instead of" a key. *)
  val indexed : int -> 'v -> 'v t

  (** Replaces each '{index}' key with the key corresponding to its index. *)
  val fill_idxs : 'v t list -> 'v t list

  val map : 'va t -> f: ('va -> 'vb) -> 'vb t

  val is_equal1 : 'v t -> 'v t -> sub: ('v -> 'v -> bool) -> bool

  (** Ensures that both properties have the same keys, reifying some
      from one structure's remainder if necessary. *)
  val align : 'a t list -> 'b t list -> ('a t list * 'b t list) option

  module Of_monad (V : Monad.Type) : sig
    val all : 'a V.t t -> 'a t V.t
  end
end

module Record : sig
  type 'v t = 'v Ast.record

  module Of_monad (V : Monad.Type) : sig
    val all : 'a V.t t -> 'a t V.t
  end

  module Traverse (M : Ast.Nest_m.Type) : sig
    val surface : 'a t -> f: ('a -> 'b M.t) -> 'b t M.t

    (** Still applies nesting. *)
    val head : 'a t -> f: ('a -> 'a M.t) -> 'a t M.t
  end

  val map : 'va t -> f: ('va -> 'vb) -> 'vb t

  val is_equal1 : 'v t -> 'v t -> sub: ('v -> 'v -> bool) -> bool

  (** All possible immediate paths in this record, and the values they correspond to. *)
  val imm_locations : 'v t -> (Ast.path_elem * 'v) list

  val at_elem : Ast.path_elem -> 'v t -> 'v option

  val at_elem_exn : Ast.path_elem -> 'v t -> 'v

  val set_at_elem_exn : Ast.path_elem -> 'v t -> n: 'v -> 'v t

  val map_at_elem_exn : Ast.path_elem -> 'v t -> f: ('v -> 'v) -> 'v t
end

module Nest_m : sig
  module Simple (M : Monad.Type) :
    Ast.Nest_m.Type with type 'a t = 'a M.t

  module Ident : Ast.Nest_m.Type with type 'a t = 'a
end

module Value : sig
  type ('ann, 'abs) t = ('ann, 'abs) Ast.value

  module Gen_traverse (M : Monad.Type) (M_record : sig
    val surface : 'a Ast.record -> f: ('a -> 'b M.t) -> 'b Ast.record M.t

    (** Still applies nesting. *)
    val head : 'a Ast.record -> f: ('a -> 'a M.t) -> 'a Ast.record M.t
  end) : sig
    val imm : ('ann, 'abs) t -> f: (('ann, 'abs) t -> ('ann, 'abs) t M.t) -> ('ann, 'abs) t M.t

    val imm_ann : ('ann, 'abs) t -> f: ('ann -> 'ann M.t) -> ('ann, 'abs) t M.t

    (** Traverses children before parents. *)
    val up : ('ann, 'abs) t -> f: (('ann, 'abs) t -> ('ann, 'abs) t M.t) -> ('ann, 'abs) t M.t

    (** Traverses parents before children. *)
    val down : ('ann, 'abs) t -> f: (('ann, 'abs) t -> ('ann, 'abs) t M.t) -> ('ann, 'abs) t M.t

    (** Traverses pipes up. *)
    val pipes_to_vals : ('ann, 'a_abs) t -> f: ('ann -> 'a_abs -> ('ann, 'b_abs) t M.t) -> ('ann, 'b_abs) t M.t

    (** Traverses pipes. *)
    val pipes : ('ann, 'a_abs) t -> f: ('a_abs -> 'b_abs M.t) -> ('ann, 'b_abs) t M.t

    (** Traverses pipes. *)
    val pipes_annd : ('ann, 'a_abs) t -> f: ('ann -> 'a_abs -> 'b_abs M.t) -> ('ann, 'b_abs) t M.t

    (** Traverses record heads up. *)
    val heads : ('ann, 'abs) t -> f: (('ann, 'abs) t -> ('ann, 'abs) t M.t) -> ('ann, 'abs) t M.t

    (** Traverses annotations. *)
    val anns : ('a_ann, 'abs) t -> f: ('a_ann -> 'b_ann M.t) -> ('b_ann, 'abs) t M.t
  end

  module Traverse (M : Ast.Nest_m.Type) : module type of Gen_traverse(M)(Record.Traverse(M))

  module Fold (R : Monoid.Type) : sig
    val up : ('ann, 'abs) t -> f: (('ann, 'abs) t -> R.t) -> R.t

    (** Folds annotations. *)
    val anns : ('ann, 'abs) t -> f: ('ann -> R.t) -> R.t

  end

  module Map : module type of Traverse(Nest_m.Ident)

  val annotation : ('ann, 'abs) t -> 'ann

  val has_pipe : ('ann, 'abs) t -> bool

  (** Whether the values are semantically equivalent -
      e.g. ignores annotations. *)
  val is_equiv : ('ann, 'abs) t -> ('ann, 'abs) t -> bool

  (** All possible paths in this value, and the values they correspond to. *)
  val locations : ('ann, 'abs) t -> (Ast.path * ('ann, 'abs) t) list

  (** All paths which refer to [c] in [x]. *)
  val locations_of : ('ann, 'abs) t -> ('ann, 'abs) t -> Ast.path list

  val at_elem : Ast.path_elem -> ('ann, 'abs) t -> ('ann, 'abs) t option

  val at_elem_exn : Ast.path_elem -> ('ann, 'abs) t -> ('ann, 'abs) t

  val set_at_elem_exn : Ast.path_elem -> ('ann, 'abs) t -> n: ('ann, 'abs) t -> ('ann, 'abs) t

  val map_at_elem_exn : Ast.path_elem -> ('ann, 'abs) t -> f: (('ann, 'abs) t -> ('ann, 'abs) t) -> ('ann, 'abs) t

  val at_path_exn : Ast.path -> ('ann, 'abs) t -> ('ann, 'abs) t

  val set_at_path_exn : Ast.path -> ('ann, 'abs) t -> n: ('ann, 'abs) t -> ('ann, 'abs) t

  val map_at_path_exn : Ast.path -> ('ann, 'abs) t -> f: (('ann, 'abs) t -> ('ann, 'abs) t) -> ('ann, 'abs) t
end

module Reducer : sig
  type 'ann t = 'ann Ast.reducer

  val map_values : 'a_ann t -> f: ('a_ann, 'b_ann) Ast.Value.poly_fun -> 'b_ann t

  val map_anns : 'a_ann t -> f: ('a_ann -> 'b_ann) -> 'b_ann t
end

module Phase : sig
  type 'ann t = 'ann Ast.phase

  val map_values : 'a_ann t -> f: ('a_ann, 'b_ann) Ast.Value.poly_fun -> 'b_ann t

  val map_anns : 'a_ann t -> f: ('a_ann -> 'b_ann) -> 'b_ann t

  val app_phases : 'ann t list -> 'ann t list -> 'ann t list

  val join_phases : 'ann t list list -> 'ann t list
end

module Program : sig
  type 'ann t = 'ann Ast.program

  val map_anns : 'a_ann t -> f: ('a_ann -> 'b_ann) -> 'b_ann t
end
