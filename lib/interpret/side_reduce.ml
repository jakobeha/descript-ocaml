open Base

module Ann = struct
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    val trans : t -> t
  end

  module Pure = struct
    module T = Unit

    type t = T.t

    let trans () = ()
  end

  module Taint = struct
    module T = Taint

    type t = T.t

    let trans x = Taint.taint x
  end
end

module Make (A : sig type t end) = struct
  type t =
    { src_loc : Ast.path;
      dst : A.t Ast.Pure.value;
    }
end

module In = struct
  include Make(Ast.Matcher)

  let nest _ t = t

  let produce_at p i =
    { src_loc = p;
      dst = i;
    }

  module Annotate (Ann : Ann.Type) = struct
    let apply t i =
      let ann = Misc.Value.annotation i in
      let dst = t.dst |> Misc.Value.Map.anns ~f: (fun () -> Ann.trans ann) in
      Misc.Value.set_at_path_exn t.src_loc i ~n: dst

    let apply_all ts i =
      List.fold ts ~init: i ~f: (fun i t -> apply t i)
  end
end

type input = In.t

module Out = struct
  include Make(Ast.Path)

  let nest_dst k d =
    Misc.Value.Map.pipes d (fun p -> k :: p)

  let nest k t =
    { src_loc = k :: t.src_loc;
      dst = nest_dst k t.dst;
    }

  let rec add_mods mods l =
    match mods with
    | [] -> Remainder_m.return l
    | (adds, subs) :: rmods ->
      match l with
      | [] ->
        (* TODO different error *)
        raise Remainder_m.Value_in_remainder_position
      | le :: les ->
        let module Let_syntax = Remainder_m.Let_syntax in
        match le with
        | Ast.Path_elem.Remainder le_subs ->
          let%bind rles = add_mods rmods les in
          let rem_e =
            { Ast.Property.key = "...";
              value = Ast.Path_elem.Remainder (le_subs @ subs);
            }
          and add_es =
            adds |>
            List.map ~f:
            ( fun add ->
                { Ast.Property.key = add;
                  value = Ast.Path_elem.Property add;
                }
            ) in
          let%map rle = Remainder_m.from_props (rem_e :: add_es) in
          rle :: rles
        | _ ->
          let%map rles = add_mods mods les in
          le :: rles

  let strip_path_elem_prefix1 pre xs =
    match xs with
    | [] -> None
    | x :: xs ->
      match (pre, x) with
      | (Ast.Path_elem.Remainder pre_subs, Ast.Path_elem.Remainder x_subs) ->
        let subs = List.subtract x_subs pre_subs
        and adds = List.subtract pre_subs x_subs in
        Some (Some (adds, subs), xs)
      | _ ->
        if pre = x then
          Some (None, xs)
        else
          None

  let rec strip_path_elem_prefix pres xs =
    match pres with
    | [] -> Some ([], xs)
    | pre :: pres ->
      xs |>
      strip_path_elem_prefix1 pre |>
      Core.Option.bind ~f:
      ( fun (mods, xs) ->
          strip_path_elem_prefix pres xs |>
          Option.map ~f: (fun (r_mods, r) -> (Option.to_list mods @ r_mods, r))
      )

  let dynamic_dst_possibilities io p =
    io |>
    Remainder_m.Pure.Value.locations |>
    List.filter_map ~f: (fun (l, v) ->
        match v with
        | Ast.Value.Pipe ((), vp) ->
          strip_path_elem_prefix vp p |>
          Option.map ~f:
          ( fun (mods, sl) ->
              add_mods mods l |>
              Remainder_m.map ~f: (fun l -> l @ sl)
          )
        | _ -> None) |>
    List.map ~f: (Remainder_m.map ~f: (fun l -> Ast.Value.Pipe ((), l))) |>
    List.map ~f: (Remainder_m.Pure.to_value ())

  let produce_dynamic_dst io p =
    Inject.Pure.Value.inj_amb_union () (dynamic_dst_possibilities io p)

  let rec produce_dst_children io l v =
    { Ast.Record.head = produce_dst io (Ast.Path_elem.Head :: l, v.Ast.Record.head);
      props = List.map v.props (fun vp ->
          { Ast.Property.key = vp.key;
            value = produce_dst io (Property vp.key :: l, vp.value);
          });
    }
  and produce_dst_record io l v =
    Inject.Pure.Value.inj_amb_union ()
      ( Record ((), produce_dst_children io l v) ::
        dynamic_dst_possibilities io l
      )
  and produce_dst io (l, v) =
    match v with
    | Ast.Value.Prim ((), v) -> Ast.Value.Prim ((), v)
    | Record ((), v) -> produce_dst_record io l v
    | Pipe _ -> produce_dynamic_dst io l

  let produce_1 io (l, v) =
    { src_loc = l;
      dst = produce_dst io (l, v);
    }

  (* TODO Need subs? *)
  let produce_all io x =
    x |>
    Remainder_m.Pure.Value.locations |>
    List.map ~f: (produce_1 io)

  let at_src_loc p t =
    let open Option in
    List.zip t.src_loc p >>=
    List.fold_until
      ~init: t.dst
      ~finish: (fun x -> Some x)
      ~f:
    ( fun dst (tse, pe) ->
        match tse with
        | Ast.Path_elem.Head
        | Property _ ->
          if tse <> pe then
            Stop None
          else
            Continue dst
        | Remainder _ ->
          match pe with
          | Head
          | Property _ -> Stop None
          | Remainder _ -> Continue dst
    )

  module Annotate (Ann : Ann.Type) = struct
    let apply_destroy_to_path ts ann p =
      match ts |>
            List.filter ~f: (fun t -> Misc.Value.is_equiv t.dst (Inject.Pure.Value.inj_amb_union () [])) |>
            List.find_map ~f: (at_src_loc p) with
      | None -> Ast.Value.Pipe (ann, p)
      | Some dst ->
        dst |>
        Misc.Value.Map.anns ~f: (fun () -> Ann.trans ann)

    let apply_destroy_all ts o =
      o |>
      Misc.Value.Map.pipes_to_vals ~f: (apply_destroy_to_path ts)

    let apply_to_path ts ann p =
      match List.find_map ts (at_src_loc p) with
      | None -> Ast.Value.Pipe (ann, p)
      | Some dst ->
        dst |>
        Misc.Value.Map.anns ~f: (fun () -> Ann.trans ann) |>
        apply_destroy_all ts

    let apply_all ts o =
      o |>
      Misc.Value.Map.pipes_to_vals ~f: (apply_to_path ts)
  end
end

type out = Out.t

type ('sr, 'a) monad = ('sr list * 'a) list
