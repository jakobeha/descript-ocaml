open Base

module Indiv_consume = struct
  type 'a t =
    | Disjoint
    | Sub of 'a * 'a
    | Super of 'a * 'a
    | Equal of 'a
    | Ambiguous of 'a * 'a

  let flip x =
    match x with
    | Disjoint -> Disjoint
    | Sub (x, y) -> Super (y, x)
    | Super (x, y) -> Sub (y, x)
    | Equal x -> Equal x
    | Ambiguous (x, y) -> Ambiguous (x, y)

  let map x ~f: f =
    match x with
    | Disjoint -> Disjoint
    | Sub (x, y) -> Sub (f x, f y)
    | Super (x, y) -> Super (f x, f y)
    | Equal x -> Equal (f x)
    | Ambiguous (x, y) -> Ambiguous (f x, f y)
end

type 'a indiv_consume = 'a Indiv_consume.t

module Ann = struct
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    module Side_reduce : Side_reduce.Ann.Type with module T = T

    module Remainder_m : Remainder_m.Ann.Type with module T = T

    module Inject : Inject.Ann.Type with module T = T

    val consume : t -> t

    val produce : t -> t
  end

  module Pure = struct
    module T = Unit

    type t = T.t

    module Side_reduce = Side_reduce.Ann.Pure

    module Inject = Inject.Ann.Pure

    module Remainder_m = Remainder_m.Ann.Pure

    let consume () = ()

    let produce () = ()
  end

  module Taint = struct
    module T = Taint

    type t = T.t

    module Side_reduce = Side_reduce.Ann.Taint

    module Inject = Inject.Ann.Taint

    module Remainder_m = Remainder_m.Ann.Taint

    let consume x = x

    let produce x = Taint.taint x
  end
end

module Freeze = struct
  type 'u t =
    { frozens : Ast.Pure.reducer Set.Poly.t;
      underlying : 'u;
    }

  let create u =
    { frozens = Set.Poly.empty;
      underlying = u;
    }

  let map x ~f: f =
    { frozens = x.frozens;
      underlying = f x.underlying;
    }

  let both x y =
    { frozens = Set.union x.frozens y.frozens;
      underlying = (x.underlying, y.underlying);
    }

  let freeze r x =
    { frozens = Set.add x.frozens r;
      underlying = x.underlying;
    }

  module Make (U : sig type t end) = struct
    type _t = U.t t
    type t = _t
  end

  module Ann = struct
    module Make (U : Ann.Type) = struct
      module T = Make(U)

      type t = T.t

      module Inject : Inject.Ann.Type with module T = T = struct
        module T = T

        module Lit = struct
          module T = T

          type t = T.t

          let import_location x = map x ~f: U.Inject.Lit.import_location

          let import_head x = map x ~f: U.Inject.Lit.import_head

          let import_wrap x = map x ~f: U.Inject.Lit.import_wrap

          let bool_head x = map x ~f: U.Inject.Lit.bool_head

          let property_head x = map x ~f: U.Inject.Lit.property_head

          let property_key x = map x ~f: U.Inject.Lit.property_key

          let sub_cons xs xf =
            let rs_rf =
              both xs xf |>
              map ~f: (fun (xs, xf) -> U.Inject.Lit.sub_cons xs xf) in
            ( map rs_rf ~f: (fun (rs, _) -> rs),
              map rs_rf ~f: (fun (_, rf) -> rf)
            )

          let nil_head x = map x ~f: U.Inject.Lit.nil_head
        end

        type t = T.t

        let amb_union_head x = map x ~f: U.Inject.amb_union_head

        let splice_head x = map x ~f: U.Inject.splice_head

        let inj_app x = map x ~f: U.Inject.inj_app
      end

      module Remainder_m = Inject

      let consume x = map x ~f: U.consume

      let produce x = map x ~f: U.produce
    end
  end
end

module Opt = struct
  module type Type = sig
    type phase

    module Value : sig
      module type Type = sig
        type abs

        type t = abs Ast.Pure.value

        val narrow_phase : phase -> t -> Ast.Pure.phase
      end

      module Reg : Type with type abs = Nothing.t
      module In : Type with type abs = Ast.matcher
      module Out : sig
        module Local (CIn : sig val x : Ast.Pure.in_value end) :
          Type with type abs = Ast.path
      end
    end

    module Phase : sig
      val fold_reduce_using :
        'ann Ast.phase list ->
        f: (phase -> 'ann Ast.phase -> 'ann Ast.phase) ->
        phase
    end
  end
end

module Make (Opt : Opt.Type) = struct
  module Reduce = struct
    module type Type = sig
      type t

      type 'a reduce_m

      val reduce : Opt.phase -> t -> t reduce_m
    end
  end

  module Annotate (Ann : Ann.Type) = struct
    module Freeze_ann = Freeze.Ann.Make(Ann)

    type ann = Ann.t
    type freeze_ann = Freeze_ann.t

    module Ann_ast = Ast.Annotate(Ann.T)
    module Ann_side_reduce_in = Side_reduce.In.Annotate(Ann.Side_reduce)
    module Ann_side_reduce_out = Side_reduce.Out.Annotate(Ann.Side_reduce)
    module Freeze_ann_inject = Inject.Annotate(Freeze_ann.Inject)

    module Value = struct
      include (
        Ann_ast.Value : module type of Ann_ast.Value
        with module Make := Ann_ast.Value.Make
        and module Reg := Ann_ast.Value.Reg
        and module In := Ann_ast.Value.In
        and module Out := Ann_ast.Value.Out
      )

      module type Type = sig
        type abs

        type 'ann t = ('ann, abs) Ast.Value.t

        module Consume_m : sig
          include Monad.Type

          val from_indiv : 'a indiv_consume -> 'a t

          val ambiguify : 'a t -> 'a t
        end

        type 'a consume_m = 'a Consume_m.t

        module Produce_m : Monad.Type

        type 'a produce_m = 'a Produce_m.t

        module Reduce_m : sig
          type 'ann _t = 'ann t

          include Ast.Nest_m.Type

          val make : freeze_ann _t Produce_m.t Consume_m.t -> default: freeze_ann _t -> freeze_ann _t t
        end

        type 'a reduce_m = 'a Reduce_m.t

        val consume_pipe : Ast.Pure.in_value -> freeze_ann -> abs -> freeze_ann t consume_m

        val produce_extra : Ast.Pure.out_value -> freeze_ann t -> unit produce_m

        val narrow_phase : Opt.phase -> unit t -> Ast.Pure.phase
      end

      let consume_regexp_regexp i x =
        if Regexp.is_equal i x then
          Indiv_consume.Equal x
        else if Regexp.is_subset x i then
          Indiv_consume.Super (i, x)
        else if Regexp.is_subset i x then
          Indiv_consume.Sub (i, x)
        else if Regexp.is_disjoint i x then
          Indiv_consume.Disjoint
        else
          Indiv_consume.Ambiguous (x, Regexp.intersect i x)

      let consume_matcher_prim i x =
        match i with
        | Ast.Matcher.Any ->
          Indiv_consume.Super (First i, Second x)
        | Prim ->
          Indiv_consume.Super (First i, Second x)
        | Record -> Indiv_consume.Disjoint
        | Integer ->
          ( match x with
            | Ast.Prim.Integer _ ->
              Indiv_consume.Super (First i, Second x)
            | _ -> Indiv_consume.Disjoint
          )
        | Float ->
          ( match x with
            | Ast.Prim.Float _ ->
              Indiv_consume.Super (First i, Second x)
            | _ -> Indiv_consume.Disjoint
          )
        | String ->
          ( match x with
            | Ast.Prim.String _ ->
              Indiv_consume.Super (First i, Second x)
            | _ -> Indiv_consume.Disjoint
          )
        | Regexp i ->
          ( match x with
            | Ast.Prim.String x ->
              if Regexp.matches i x then
                Indiv_consume.Super (First (Regexp i), Second (String x))
              else
                Indiv_consume.Disjoint
            | _ -> Indiv_consume.Disjoint
          )

      let consume_matcher_record i x =
        match i with
        | Ast.Matcher.Any ->
          Indiv_consume.Super (First i, Second x)
        | Prim -> Indiv_consume.Disjoint
        | Record ->
          Indiv_consume.Super (First i, Second x)
        | Integer -> Indiv_consume.Disjoint
        | Float -> Indiv_consume.Disjoint
        | String -> Indiv_consume.Disjoint
        | Regexp _ -> Indiv_consume.Disjoint

      let consume_matcher_matcher i x =
        match (i, x) with
        | (Ast.Matcher.Any, Ast.Matcher.Any) ->
          Indiv_consume.Equal Ast.Matcher.Any
        | (Any, Prim) -> Indiv_consume.Super (i, x)
        | (Any, Record) -> Indiv_consume.Super (i, x)
        | (Any, Integer) -> Indiv_consume.Super (i, x)
        | (Any, Float) -> Indiv_consume.Super (i, x)
        | (Any, String) -> Indiv_consume.Super (i, x)
        | (Any, Regexp _) -> Indiv_consume.Super (i, x)
        | (Prim, Any) -> Indiv_consume.Sub (i, x)
        | (Prim, Prim) -> Indiv_consume.Equal Prim
        | (Prim, Record) -> Indiv_consume.Disjoint
        | (Prim, Integer) -> Indiv_consume.Super (i, x)
        | (Prim, Float) -> Indiv_consume.Super (i, x)
        | (Prim, String) -> Indiv_consume.Super (i, x)
        | (Prim, Regexp _) -> Indiv_consume.Super (i, x)
        | (Record, Any) -> Indiv_consume.Sub (i, x)
        | (Record, Prim) -> Indiv_consume.Disjoint
        | (Record, Record) -> Indiv_consume.Equal Record
        | (Record, Integer) -> Indiv_consume.Disjoint
        | (Record, Float) -> Indiv_consume.Disjoint
        | (Record, String) -> Indiv_consume.Disjoint
        | (Record, Regexp _) -> Indiv_consume.Disjoint
        | (Integer, Any) -> Indiv_consume.Sub (i, x)
        | (Integer, Prim) -> Indiv_consume.Sub (i, x)
        | (Integer, Record) -> Indiv_consume.Disjoint
        | (Integer, Integer) -> Indiv_consume.Equal Integer
        | (Integer, Float) -> Indiv_consume.Disjoint
        | (Integer, String) -> Indiv_consume.Disjoint
        | (Integer, Regexp _) -> Indiv_consume.Disjoint
        | (Float, Any) -> Indiv_consume.Sub (i, x)
        | (Float, Prim) -> Indiv_consume.Sub (i, x)
        | (Float, Record) -> Indiv_consume.Disjoint
        | (Float, Integer) -> Indiv_consume.Disjoint
        | (Float, Float) -> Indiv_consume.Equal Float
        | (Float, String) -> Indiv_consume.Disjoint
        | (Float, Regexp _) -> Indiv_consume.Disjoint
        | (String, Any) -> Indiv_consume.Sub (i, x)
        | (String, Prim) -> Indiv_consume.Sub (i, x)
        | (String, Record) -> Indiv_consume.Disjoint
        | (String, Integer) -> Indiv_consume.Disjoint
        | (String, Float) -> Indiv_consume.Disjoint
        | (String, String) -> Indiv_consume.Equal String
        | (String, Regexp _) -> Indiv_consume.Super (i, x)
        | (Regexp _, Any) -> Indiv_consume.Sub (i, x)
        | (Regexp _, Prim) -> Indiv_consume.Sub (i, x)
        | (Regexp _, Record) -> Indiv_consume.Disjoint
        | (Regexp _, Integer) -> Indiv_consume.Disjoint
        | (Regexp _, Float) -> Indiv_consume.Disjoint
        | (Regexp _, String) -> Indiv_consume.Sub (i, x)
        | (Regexp i, Regexp x) ->
          Indiv_consume.map
            (consume_regexp_regexp i x)
            (fun res -> Ast.Matcher.Regexp res)

      let consume_prim_prim i x =
        if i = x then
          Indiv_consume.Equal x
        else
          Indiv_consume.Disjoint

      let consume_prim_matcher i x =
        consume_matcher_prim x i |>
        Indiv_consume.flip

      let consume_record_matcher i x =
        consume_matcher_record x i |>
        Indiv_consume.flip

      module Make (A : Type) : sig
        include
          ( Reduce.Type
            with type t = ann A.t
            and type 'a reduce_m = 'a A.reduce_m
          )

        val consume : Ast.Pure.in_value -> freeze_ann A.t -> freeze_ann A.t A.consume_m
      end = struct
        open A

        exception Unexpected_matcher

        module Reduce_m = A.Reduce_m
        module Consume_property = Misc.Property.Of_monad(Consume_m)
        module Consume_list = List.Of_monad(Consume_m)
        module Traverse_reduce_m = Misc.Value.Traverse(A.Reduce_m)

        type t = ann A.t
        type 'a reduce_m = 'a Reduce_m.t

        let reify_concrete_consume_res x =
          match x with
          | First () -> raise Unexpected_matcher
          | Second x -> x

        let consume_matcher i x =
          match x with
          | Ast.Value.Prim (ann, x) ->
            let ann = Freeze_ann.consume ann in
            consume_matcher_prim i x |>
            Indiv_consume.map ~f:
              ( function
                | First _ -> First ()
                | Second res -> Second (Ast.Value.Prim (ann, res))
              ) |>
            Consume_m.from_indiv |>
            Consume_m.map ~f: reify_concrete_consume_res
          | Record (ann, x) ->
            let ann = Freeze_ann.consume ann in
            consume_matcher_record i x |>
            Indiv_consume.map ~f:
              ( function
                | First _ -> First ()
                | Second res -> Second (Ast.Value.Record (ann, res))
              ) |>
            Consume_m.from_indiv |>
            Consume_m.map ~f: reify_concrete_consume_res
          | Pipe (ann, x) ->
            let ann = Freeze_ann.consume ann in
            A.consume_pipe (Pipe ((), i)) ann x

        let consume_prim i x =
          match x with
          | Ast.Value.Prim (ann, x) ->
            let ann = Freeze_ann.consume ann in
            consume_prim_prim i x |>
            Indiv_consume.map ~f: (fun res ->
                Ast.Value.Prim (ann, res)) |>
            Consume_m.from_indiv
          | Record _ ->
            Indiv_consume.Disjoint |>
            Consume_m.from_indiv
          | Pipe (ann, x) ->
            let ann = Freeze_ann.consume ann in
            A.consume_pipe (Prim ((), i)) ann x
        let rec consume_prop_prop (i : Ast.Pure.in_value Ast.property) x =
          if i.Ast.Property.key = x.Ast.Property.key then
            let module Let_syntax = Consume_m in
            let%map value = consume i.value x.value in
            { Ast.Property.key = x.key;
              value = value;
            }
          else
            Consume_m.from_indiv Indiv_consume.Disjoint
        and consume_sprops_remainder is x =
          is |>
          List.map ~f:
          ( fun i ->
              let module Let_syntax = Consume_m in
              let%map value =
                consume i.Ast.Property.value x.Ast.Property.value in
              { Ast.Property.key = i.key;
                value = value;
              }
          ) |>
          Consume_list.all
        and consume_remainder_sprops i xs =
          xs |>
          List.map ~f:
          ( fun x ->
              let module Let_syntax = Consume_m in
              let%map value =
                consume i.Ast.Property.value x.Ast.Property.value in
              { Ast.Property.key = x.key;
                value = value;
              }
          ) |>
          Consume_list.all
        and consume_remainder_remainder i x =
          let module Let_syntax = Consume_m in
          let%map value =
            consume i.Ast.Property.value x.Ast.Property.value in
          { Ast.Property.key = "...";
            value = value;
          }
        and consume_sprops_sprops_zip ixs =
          ixs |>
          List.map ~f: (fun (i, x) -> consume_prop_prop i x) |>
          Consume_list.all
        and consume_sprops_remainder_zip ci cx cixs =
          match cixs with
          | Either3.Left cis -> consume_sprops_remainder cis cx
          | Right cxs -> consume_remainder_sprops ci cxs
          | Neither -> Consume_m.from_indiv (Indiv_consume.Equal [])
        and consume_sprops_sprops is xs =
          match List.zip is xs with
          | None -> Consume_m.from_indiv Indiv_consume.Disjoint
          | Some ixs -> consume_sprops_sprops_zip ixs
        and consume_dprops_sprops (lis, ci, ris) xs =
          if (List.length lis + List.length ris) > List.length xs then
            Consume_m.from_indiv Indiv_consume.Disjoint
          else
            let module Let_syntax = Consume_m in
            let (lxs, crxs) = List.split_n xs (List.length lis) in
            let (cxs, rxs) = List.split_n crxs (List.length crxs - List.length ris) in
            let%map lrs = consume_sprops_sprops lis lxs
            and crs = consume_remainder_sprops ci cxs
            and rrs = consume_sprops_sprops ris rxs in
            lrs @ crs @ rrs
        and consume_sprops_dprops is (lxs, cx, rxs) =
          if List.length is < (List.length lxs + List.length rxs) then
            Consume_m.from_indiv Indiv_consume.Disjoint
          else
            let module Let_syntax = Consume_m in
            let (lis, cris) = List.split_n is (List.length lxs) in
            let (cis, ris) = List.split_n cris (List.length cris - List.length rxs) in
            let%map lrs = consume_sprops_sprops lis lxs
            and crs = consume_sprops_remainder cis cx
            and rrs = consume_sprops_sprops ris rxs in
            lrs @ crs @ rrs
        and consume_dprops_dprops (lis, ci, ris) (lxs, cx, rxs) =
          let module Let_syntax = Consume_m in
          let (llixs, clixs) = List.zip_leftover lis lxs
          and (rrixs, crixs) = List.zip_leftover ris rxs in
          let%map llrs = consume_sprops_sprops_zip llixs
          and rrrs = consume_sprops_sprops_zip rrixs
          and clrs = consume_sprops_remainder_zip ci cx clixs
          and crrs = consume_sprops_remainder_zip ci cx crixs
          and cr = consume_remainder_remainder ci cx in
          llrs @ clrs @ [cr] @ crrs @ rrrs
        and consume_props_props is xs =
          match ( List.split_on is Misc.Property.is_remainder,
                  List.split_on xs Misc.Property.is_remainder
                ) with
          | (None, None) -> consume_sprops_sprops is xs
          | (Some is, None) -> consume_dprops_sprops is xs
          | (None, Some xs) -> consume_sprops_dprops is xs
          | (Some is, Some xs) -> consume_dprops_dprops is xs
        and consume_record_record i x =
          let module Let_syntax = Consume_m in
          let%map head = consume i.Ast.Record.head x.Ast.Record.head
          and props = consume_props_props i.props x.props in
          { Ast.Record.head = head;
            props = props;
          }
        and consume_record i x =
          match x with
          | Ast.Value.Prim _ ->
            Consume_m.from_indiv Indiv_consume.Disjoint
          | Record (ann, x) ->
            let ann = Freeze_ann.consume ann in
            consume_record_record i x |>
            Consume_m.map ~f: (fun r -> Ast.Value.Record (ann, r))
          | Pipe (ann, x) ->
            let ann = Freeze_ann.consume ann in
            A.consume_pipe (Record ((), i)) ann x
        (** Returns every reification of the target value which is consumable
            by the input value. For regular and output targets, either returns
            the target (if it matches) or an empty list (if it doesn't). For
            input targets, if the target contains a matcher, and the target
            matcher can be reified to be matched by the input value, or reified
            to not be matched, will return the version of the target where the
            matcher was reified, and then (after) the version where it wasn't
            (which would just be the original target - since it comes after
            the reified target, it won't match anything the reified target
            would). *)
        and consume i x =
          match i with
          | Prim ((), i) -> consume_prim i x
          | Record ((), i) -> consume_record i x
          | Pipe ((), i) -> consume_matcher i x

        let produce_pure o v =
          let ann = Misc.Value.annotation v in
          o |>
          Misc.Value.Map.anns ~f: (fun () -> Freeze_ann.produce ann) |>
          Remainder_m.Value.Traverse.pipes_to_vals ~f:
          ( fun _ o ->
              Remainder_m.Value.at_path_exn o v |>
              Remainder_m.map ~f: (Misc.Value.Map.anns ~f: Freeze_ann.produce)
          ) |>
          Remainder_m.to_single

        let inject_freeze_props r xs =
          match xs with
          | [ { Ast.Property.key = "a"; value = x };
            ] -> Misc.Value.Map.imm_ann x (Freeze.freeze r)
          | _ -> raise (Inject.Bad_inj_params "expected '[_]' (or '[a: _]')")

        let try_inject_freeze_record r ann v =
          if Lit.Value.try_symbol_of_head (v.Ast.Record.head) = Some "#Freeze" then
            inject_freeze_props r v.props
          else
            Ast.Value.Record (ann, v)

        let try_inject_freeze_indiv r v =
          match v with
          | Ast.Value.Prim (ann, v) -> Ast.Value.Prim (ann, v)
          | Record (ann, v) -> try_inject_freeze_record r ann v
          | Pipe (ann, x) -> Pipe (ann, x)

        let try_inject_freeze r v =
          Misc.Value.Map.down v (try_inject_freeze_indiv r)

        let produce r o v =
          let module Let_syntax = Produce_m in
          let%map () = A.produce_extra o v in
          produce_pure o v |>
          try_inject_freeze r

        let reduce_indiv_force r x =
          x |>
          consume r.Ast.Reducer.input |>
          Consume_m.map ~f: (produce r r.output) |>
          Reduce_m.make ~default: x

        let is_frozen_for r x =
          Set.mem (Misc.Value.annotation x).Freeze.frozens r

        let reduce_indiv r x =
          if is_frozen_for r x then
            Reduce_m.return x
          else
            reduce_indiv_force r x

        let reduce_surface_pure p x =
          List.fold
            p
            ~init: (Reduce_m.return x)
            ~f: (fun x r -> Reduce_m.bind x (reduce_indiv r))

        let reduce_surface_limited p x =
          reduce_surface_pure p x |>
          Reduce_m.map ~f: Freeze_ann_inject.Value.try_inject_apply

        let reduce_surface p x =
          reduce_surface_limited
            (A.narrow_phase p (Misc.Value.Map.anns x (fun _ -> ())))
            x

        let rec reduce_once p x =
          x |>
          Traverse_reduce_m.imm ~f: (reduce_annd p) |>
          Reduce_m.bind ~f: (reduce_surface p)
        and reduce_rest p pr nx =
          if Misc.Value.is_equiv pr nx then
            Reduce_m.return nx
          else
            reduce_annd p nx
        and reduce_annd p x =
          x |>
          reduce_once p |>
          Reduce_m.bind ~f: (reduce_rest p x)

        let reduce p x =
          x |>
          Misc.Value.Map.anns ~f: Freeze.create |>
          reduce_annd p |>
          Reduce_m.map ~f: (Misc.Value.Map.anns ~f: (fun ann -> ann.Freeze.underlying))
      end

      module Reg :
        Reduce.Type
        with type t = ann Ast.Value.Reg.t
        and type 'a reduce_m = 'a = struct
        module Impl = struct
          type abs = Nothing.t

          include Ast.Value.Reg

          module Consume_m = struct
            include Option

            let from_indiv x =
              match x with
              | Indiv_consume.Disjoint -> None
              | Sub (_, _) ->
                raise (Misc.Cannot_represent "ambiguous (subset) consume")
              | Super (_, x) -> Some x
              | Equal x -> Some x
              | Ambiguous (_, _) ->
                raise (Misc.Cannot_represent "ambiguous consume")

            let ambiguify x =
              raise (Misc.Cannot_represent "ambiguous consume")
          end

          type 'a consume_m = 'a Consume_m.t

          module Produce_m = Monad.Ident

          type 'a produce_m = 'a Produce_m.t

          module Reduce_m = struct
            type 'ann _t = 'ann t

            include Monad.Ident

            let make res ~default: def =
              match res with
              | None -> def
              | Some res -> res

            let nest _ x = x
          end

          type 'a reduce_m = 'a Reduce_m.t

          let consume_pipe _ _ x = Nothing.unreachable_code x

          let produce_extra _ _ = ()

          let narrow_phase p x = Opt.Value.Reg.narrow_phase p x
        end

        include Make(Impl)
      end

      module Amb_consume_m = struct
        module Success = struct
          type 'a t =
            { consumed : 'a;
              is_ambiguous : bool;
            }
        end

        type 'a success = 'a Success.t

        type 'a t =
          | Fail
          | Success of 'a success

        let from_indiv x =
          match x with
          | Indiv_consume.Disjoint -> Fail
          | Sub (c, _) ->
            Success
              { consumed = c;
                is_ambiguous = true;
              }
          | Super (_, c) ->
            Success
              { consumed = c;
                is_ambiguous = false;
              }
          | Equal c ->
            Success
              { consumed = c;
                is_ambiguous = false;
              }
          | Ambiguous (_, c) ->
            Success
              { consumed = c;
                is_ambiguous = true;
              }

        let ambiguify x =
          match x with
          | Fail -> Fail
          | Success x ->
            Success
              { consumed = x.consumed;
                is_ambiguous = true;
              }

        let return x =
          Success
            { consumed = x;
              is_ambiguous = false;
            }

        let map x ~f: f =
          match x with
          | Fail -> Fail
          | Success x ->
            Success
              { consumed = f x.consumed;
                is_ambiguous = x.is_ambiguous;
              }

        let both x y =
          match (x, y) with
          | (Fail, Fail) -> Fail
          | (Success _, Fail) -> Fail
          | (Fail, Success _) -> Fail
          | (Success x, Success y) ->
            Success
              { consumed = (x.consumed, y.consumed);
                is_ambiguous = x.is_ambiguous || y.is_ambiguous;
              }

        (* `all` is like `List.Of_monad(...).all`,
            but short-circuits on `Fail`. *)
        let all xs =
          List.fold_right xs ~init: (return []) ~f: (fun x ps ->
              match (x, ps) with
              | (Fail, Fail) -> Fail
              | (Fail, Success _) -> Fail
              | (Success _, Fail) -> Fail
              | (Success x, Success ps) ->
                Success
                  { consumed = x.consumed :: ps.consumed;
                    is_ambiguous = x.is_ambiguous || ps.is_ambiguous;
                  })

        (* `trav ~f: f` is like `map ~f: f |> List.Of_monad(...).all`,
            but short-circuits on `Fail`. *)
        let trav xs ~f: f =
          List.fold_right xs ~init: (return []) ~f: (fun x ps ->
              match ps with
              | Fail -> Fail
              | Success ps ->
                match f x with
                | Fail -> Fail
                | Success x ->
                  Success
                    { consumed = x.consumed :: ps.consumed;
                      is_ambiguous = x.is_ambiguous || ps.is_ambiguous;
                    })

        let bind x ~f: f =
          match x with
          | Fail -> Fail
          | Success x ->
            match f x.consumed with
            | Fail -> Fail
            | Success r ->
              Success
                { consumed = r.consumed;
                  is_ambiguous = x.is_ambiguous || r.is_ambiguous;
                }

        let join x =
          match x with
          | Fail -> Fail
          | Success x ->
            match x.consumed with
            | Fail -> Fail
            | Success r ->
              Success
                { consumed = r.consumed;
                  is_ambiguous = x.is_ambiguous || r.is_ambiguous;
                }
      end

      type 'a amb_consume_m = 'a Amb_consume_m.t

      module In : sig
        include
          ( Reduce.Type
            with type t = ann Ast.Value.In.t
            and type 'a reduce_m = (Side_reduce.out, 'a) Side_reduce.monad
          )

        val is_redundant_ambiguous : 'ann Ast.in_value -> bool

        val consume : Ast.Pure.in_value -> freeze_ann Ast.in_value -> freeze_ann Ast.in_value amb_consume_m
      end = struct
        let is_redundant_ambiguous def =
          match def with
          | Ast.Value.Pipe (_, Ast.Matcher.Any)
          | Ast.Value.Pipe (_, Record)
          | Ast.Value.Record
            ( _,
              { head = Pipe (_, Any);
                props = [{ key = "..."; value = Pipe (_, Any) }];
              }
            ) -> true
          | _ -> false

        module Impl = struct
          type abs = Ast.matcher

          include Ast.Value.In

          module Consume_m = Amb_consume_m

          type 'a consume_m = 'a Consume_m.t

          module Produce_m = Writer.Make(List.Make(Side_reduce.Out))

          type 'a produce_m = 'a Produce_m.t

          module Reduce_m = struct
            type 'ann _t = 'ann t

            include Produce_m.Trans(List.Cartesian)

            let make res ~default: def =
              match res with
              | Consume_m.Fail -> return def
              | Success ires ->
                if ires.is_ambiguous && is_redundant_ambiguous def then
                  return def
                else if ires.is_ambiguous then
                  [ires.consumed; Produce_m.return def]
                else if Freeze_ann_inject.Value.is_inj_discard (Tuple2.get2 ires.consumed) &&
                        not (Freeze_ann_inject.Value.is_inj_discard def) then
                  []
                else
                  [ires.consumed]

            let nest e x = map_written x (List.map ~f: (Side_reduce.Out.nest e))
          end

          type 'a reduce_m = 'a Reduce_m.t

          let consume_pipe_indiv i ann x =
            match i with
            | Ast.Value.Prim ((), i) ->
              consume_prim_matcher i x |>
              Indiv_consume.map ~f:
                ( function
                  | First res -> Ast.Value.Pipe (ann, res)
                  | Second res -> Prim (ann, res)
                )
            | Record ((), i) ->
              consume_record_matcher i x |>
              Indiv_consume.map ~f:
                ( function
                  | First res -> Ast.Value.Pipe (ann, res)
                  | Second res ->
                    ( Ast.Value.Record ((), res) |>
                      Misc.Value.Map.anns ~f: (fun () -> ann)
                    )
                )
            | Pipe ((), i) ->
              consume_matcher_matcher i x |>
              Indiv_consume.map ~f: (fun res ->
                  Ast.Value.Pipe (ann, res))

          let consume_pipe i ann x =
            consume_pipe_indiv i ann x |>
            Consume_m.from_indiv

          let produce_extra_pure o x =
            Produce_m.tell (Side_reduce.Out.produce_all o x)

          let produce_extra o x =
            produce_extra_pure o (Misc.Value.Map.anns x (fun _ -> ()))

          let narrow_phase p x = Opt.Value.In.narrow_phase p x
        end

        include Make(Impl)
      end

      module Out = struct
        module Local (CIn : sig val x : Ast.Annotate(Ann.T).in_value end) : Reduce.Type
          with type t = ann Ast.Value.Out.t
          and type 'a reduce_m = (Side_reduce.input, 'a) Side_reduce.monad = struct

          let is_redundant_ambiguous def =
            match def with
            | Ast.Value.Pipe (_, def_path) ->
              Misc.Value.at_path_exn def_path CIn.x |>
              In.is_redundant_ambiguous
            | _ -> false

          module Impl = struct
            type abs = Ast.path

            include Ast.Value.Out

            module Opt_out_local = Opt.Value.Out.Local(struct
              let x = Misc.Value.Map.anns CIn.x (fun _ -> ())
            end)

            module Sub_produce_m = Writer.Make(List.Make(Side_reduce.In))

            module Consume_m = struct
              include Sub_produce_m.Trans(Amb_consume_m)

              let from_indiv x =
                x |>
                Indiv_consume.map ~f: Sub_produce_m.return |>
                Amb_consume_m.from_indiv

              let ambiguify x =
                Amb_consume_m.ambiguify x
            end

            type 'a consume_m = 'a Consume_m.t

            module Produce_m = Monad.Ident

            type 'a produce_m = 'a Produce_m.t

            module Reduce_m = struct
              type 'ann _t = 'ann t

              include Sub_produce_m.Trans(List.Cartesian)

              let make res ~default: def =
                match res with
                | Amb_consume_m.Fail -> return def
                | Success ires ->
                  if ires.is_ambiguous && is_redundant_ambiguous def then
                    return def
                  else if ires.is_ambiguous then
                    [ires.consumed; Sub_produce_m.return def]
                  else if Freeze_ann_inject.Value.is_inj_discard (Tuple2.get2 ires.consumed) &&
                          not (Freeze_ann_inject.Value.is_inj_discard def) then
                    []
                  else
                    [ires.consumed]

              let nest e x = map_written x (List.map ~f: (Side_reduce.In.nest e))
            end

            type 'a reduce_m = 'a Reduce_m.t

            let trans_matcher ann p m =
              Sub_produce_m.tell_and_return
                [ m |>
                  Misc.Value.Map.anns ~f: (fun _ -> ()) |>
                  Side_reduce.In.produce_at p;
                ]
                ( Misc.Value.Map.pipes_to_vals m
                    (fun _ _ -> Ast.Value.Pipe (ann, p))
                )

            let consume_pipe i ann p =
              let module Amb_consume_m_list = List.Of_monad(Amb_consume_m) in
              let module Freeze_ann_remainder_m = Remainder_m.Annotate(Freeze_ann.Remainder_m) in
              Remainder_m.Value.at_path_exn p CIn.x |>
              Remainder_m.map ~f: (Misc.Value.Map.anns ~f: Freeze.create) |>
              Freeze_ann_remainder_m.to_value ann |>
              In.consume i |>
              (* TODO is this right? e.g. Annotations are removed then
                re-added in the side-reduce - should they be preserved?
                Should annotations, especially freezes, from In.consume
                be in the result consume? *)
              Amb_consume_m.map ~f: (trans_matcher ann p)

            let produce_extra _ _ = ()

            let narrow_phase p x = Opt_out_local.narrow_phase p x
          end

          include Make(Impl)
        end
      end
    end

    module Reducer = struct
      include Ann_ast.Reducer

      type 'a reduce_m = 'a list

      let reduce p x =
        let module Let_syntax = List in
        let%bind (out_transes, semi_new_in) = Value.In.reduce p x.Ast.Reducer.input in
        let module Semi_new_in = struct let x = semi_new_in end in
        let module Local_out_value = Value.Out.Local(Semi_new_in) in
        (* The order of trans_paths and reduce_out_value probably doesn't matter. *)
        let%map (in_transes, new_out) =
          x.output |>
          Ann_side_reduce_out.apply_all out_transes |>
          Local_out_value.reduce p in
        let new_in = Ann_side_reduce_in.apply_all in_transes semi_new_in in
        { Ast.Reducer.input = new_in;
          output = new_out;
        }
    end

    module Phase = struct
      include Ann_ast.Phase

      type 'a reduce_m = 'a

      let reduce p x =
        List.concat_map x (Reducer.reduce p)

      let fold_reduce xs =
        Opt.Phase.fold_reduce_using xs ~f: reduce

      let from_slow x = fold_reduce [x]
    end

    module Program = struct
      include Ann_ast.Program

      let interpret x =
        Value.Reg.reduce (Phase.fold_reduce x.Ast.Program.phases) x.query

      let macro_reduce p x =
        { Ast.Program.phases =
            List.map x.Ast.Program.phases (Phase.reduce p);
          query = Value.Reg.reduce p x.query;
        }
    end
  end

  module Pure = Annotate(Ann.Pure)
  module Taint = Annotate(Ann.Taint)
end
