open Base

module Opt : Interpret_gen.Opt.Type with type phase = Ast.Pure.phase = struct
  type phase = Ast.Pure.phase

  module Value = struct
    module type Type = sig
      type abs

      type t = abs Ast.Pure.value

      val narrow_phase : phase -> t -> Ast.Pure.phase
    end

    module Reg : Type with type abs = Nothing.t = struct
      type abs = Nothing.t

      type t = abs Ast.Pure.value

      let narrow_phase p _ = p
    end

    module In : Type with type abs = Ast.matcher = struct
      type abs = Ast.matcher

      type t = abs Ast.Pure.value

      let narrow_phase p _ = p
    end

    module Out = struct
      module Local (CIn : sig val x : Ast.Pure.in_value end) :
        Type with type abs = Ast.path = struct
        type abs = Ast.path

        type t = abs Ast.Pure.value

        let narrow_phase p _ = p
      end
    end
  end

  module Phase = struct
    let reduce_app_using p x ~f: f_reduce =
      f_reduce p x |>
      Misc.Phase.map_anns ~f: (fun _ -> ()) |>
      fun r -> p @ r

    let fold_reduce_using xs ~f: f_reduce =
      List.fold xs ~init: [] ~f: (reduce_app_using ~f: f_reduce)
  end
end
