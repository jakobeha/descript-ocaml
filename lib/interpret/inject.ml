open Base

exception Bad_inj_head of string
exception Bad_inj_params of string

module Ann = struct
  module type Type = sig
    module T : sig
      type t
    end

    module Lit : Lit.Ann.Type with module T = T

    type t = T.t

    val amb_union_head : t -> t
    val splice_head : t -> t
    val inj_app : t -> t
  end

  module Pure = struct
    module T = Unit

    module Lit = Lit.Ann.Pure

    type t = T.t

    let amb_union_head () = ()
    let splice_head () = ()
    let inj_app () = ()
  end

  module Taint = struct
    module T = Taint

    module Lit = Lit.Ann.Taint

    type t = T.t

    let amb_union_head x = x

    let splice_head x = x

    let inj_app x = Taint.taint x
  end
end

module Annotate (Ann : Ann.Type) = struct
  type ann = Ann.t

  module Ann_ast = Ast.Annotate(Ann.T)
  module Ann_lit = Lit.Annotate(Ann.Lit)

  module Value = struct
    include Ann_ast.Value

    let pure_inj_discard =
      Ast.Value.Record
        ( (),
          { head = Lit.Value.head_of_symbol () "#Discard";
            props = [];
          }
        )

    let is_inj_discard x =
      Misc.Value.is_equiv
        (Misc.Value.Map.anns x (fun _ -> ()))
        pure_inj_discard

    let refine_params_to_unary xs =
      match xs with
      | [{ Ast.Property.key = "a"; value = x }] -> x
      | _ -> raise (Bad_inj_params "expected '[_]' (or '[a: _]')")

    let refine_params_to_binary xs ~left: l_key ~right: r_key =
      match xs with
      | [ { Ast.Property.key = l_key; value = l };
          { key = r_key; value = r };
        ] -> (l, r)
      | _ ->
        raise (Bad_inj_params (sprintf "expected '[%s: _; %s: _]'" l_key r_key))

    let refine_params_to_ternary xs ~left: l_key ~middle: m_key ~right: r_key =
      match xs with
      | [ { Ast.Property.key = l_key; value = l };
          { key = m_key; value = m };
          { key = r_key; value = r };
        ] -> (l, m, r)
      | _ ->
        raise (Bad_inj_params (sprintf "expected '[%s: _; %s; _; %s: _]'" l_key m_key r_key))

    let refine_params_to_binary_op xs =
      refine_params_to_binary xs ~left: "left" ~right: "right"

    let refine_params_to_list xs =
      List.mapi xs (fun idx x ->
          if x.Ast.Property.key = Misc.Symbol.from_index idx then
            x.value
          else
            raise (Bad_inj_params "expected '[_; _; ...]' (or '[a: _; b: _; ...]')"))

    let refine_params_to_binary_op_or_list xs =
      try First (refine_params_to_binary_op xs) with
      | Bad_inj_params x ->
        try Second (refine_params_to_list xs) with
        | Bad_inj_params y ->
          raise (Bad_inj_params (x ^ " or " ^ y))

    let refine_params_to_binary_op_free xs ~f: f =
      match refine_params_to_binary_op_or_list xs with
      | First (x, y) -> f (x, y)
      | Second xs ->
        match xs with
        | [] -> raise (Bad_inj_params "expected at least one parameter")
        | [x] ->
          ( try Lit.Value.to_list x with
            | Lit.Value.Bad_form "list" -> raise (Bad_inj_params "expected a list or more than one parameter")
          ) |>
          List.reduce_exn ~f: (fun x y -> f (x, y))
        | _ -> List.reduce_exn xs ~f: (fun x y -> f (x, y))

    let amb_union_head ann =
      Lit.Value.head_of_symbol (Ann.amb_union_head ann) "#AmbUnion"

    let splice_head ann =
      Lit.Value.head_of_symbol (Ann.splice_head ann) "#Splice"

    let bubble_amb_union1 x =
      let module Traverse = Misc.Value.Traverse(Misc.Nest_m.Simple(List.Cartesian)) in
      Traverse.up x
        ( function
          | Record (ann, x) ->
            if Misc.Value.is_equiv x.head (amb_union_head ann) then
              refine_params_to_list x.props
            else
              [Record (ann, x)]
          | x -> [x]
        )

    (* Splices nested '#AmbUnion's into the list. *)
    let bubble_amb_union xs =
      List.concat_map xs bubble_amb_union1

    let inj_amb_union ann xs =
      let xs = bubble_amb_union xs in
      match List.stable_dedup xs with
      | [x] -> x
      | xs ->
        Record
          ( ann,
            { head = amb_union_head ann;
              props = List.mapi xs Misc.Property.indexed;
            }
          )

    let inj_splice ann xs =
      Ast.Value.Record
        ( ann,
          { head = splice_head ann;
            props = xs
          }
        )

    let apply_add ann xs =
      refine_params_to_binary_op_free xs ~f:
      ( function
        | (Ast.Value.Prim (_, Integer x), Ast.Value.Prim (_, Integer y)) ->
          Ast.Value.Prim (ann, Integer (x + y))
        | (Prim (_, Float x), Prim (_, Float y)) ->
          Prim (ann, Float (x +. y))
        | _ ->
          raise (Bad_inj_params "expected integers or floats")
      )

    let apply_subtract ann xs =
      match refine_params_to_binary_op xs with
      | (Ast.Value.Prim (_, Integer x), Prim (_, Integer y)) ->
        Ast.Value.Prim (ann, Integer (x - y))
      | (Prim (_, Float x), Prim (_, Float y)) ->
        Prim (ann, Float (x -. y))
      | _ ->
        raise (Bad_inj_params "expected 2 integers or floats")

    let apply_multiply ann xs =
      refine_params_to_binary_op_free xs ~f:
      ( function
        | (Ast.Value.Prim (_, Integer x), Ast.Value.Prim (_, Integer y)) ->
          Ast.Value.Prim (ann, Integer (x * y))
        | (Prim (_, Float x), Prim (_, Float y)) ->
          Prim (ann, Float (x *. y))
        | _ ->
          raise (Bad_inj_params "expected integers or floats")
      )

    let apply_divide ann xs =
      match refine_params_to_binary_op xs with
      | (Ast.Value.Prim (_, Integer x), Prim (_, Integer y)) ->
        Ast.Value.Prim (ann, Integer (x / y))
      | (Prim (_, Float x), Prim (_, Float y)) ->
        Prim (ann, Float (x /. y))
      | _ ->
        raise (Bad_inj_params "expected 2 integers or floats")

    let apply_app ann xs =
      refine_params_to_binary_op_free xs ~f:
      ( function
        | (Ast.Value.Prim (_, String x), Ast.Value.Prim (_, String y)) ->
          Ast.Value.Prim (ann, String (x ^ y))
        | _ ->
          raise (Bad_inj_params "expected strings")
      )

    let regexp_match r t =
      try
        let re = Re.compile (Re.Posix.re r) in
        Re.Group.get (Re.exec re t) 1
      with
      | Re.Posix.Parse_error
      | Re.Posix.Not_supported ->
        raise (Bad_inj_params "regular expression is invalid")
      | Caml.Not_found
      | (Not_found_s _) ->
        raise (Bad_inj_params "regular expression didn't find a match")

    let apply_regex_match ann xs =
      match refine_params_to_binary xs ~left: "pattern" ~right: "input" with
      | (Ast.Value.Prim (_, String x), Prim (_, String y)) ->
        Ast.Value.Prim (ann, String (regexp_match x y))
      | _ ->
        raise (Bad_inj_params "expected 2 strings ('pattern' and 'input')")

    let apply_compare ann xs op =
      let (x, y) = refine_params_to_binary_op xs in
      Ann_lit.Value.bool ann (op x y)

    let apply_is_equal ann xs =
      apply_compare ann xs Misc.Value.is_equiv

    let apply_prim_compare ann xs op =
      match refine_params_to_binary_op xs with
      | (Ast.Value.Prim (_, Integer x), Prim (_, Integer y)) ->
        Ann_lit.Value.bool ann
          (op (Ast.Prim.Integer x) (Ast.Prim.Integer y))
      | (Prim (_, Float x), Prim (_, Float y)) ->
        Ann_lit.Value.bool ann (op (Float x) (Float y))
      | (Prim (_, String x), Prim (_, String y)) ->
        Ann_lit.Value.bool ann (op (String x) (String y))
      | _ ->
        raise (Bad_inj_params "expected 2 primitives of the same type")

    let apply_is_gt ann xs =
      apply_prim_compare ann xs (>)

    let apply_is_lt ann xs =
      apply_prim_compare ann xs (<)

    let apply_is_gteq ann xs =
      apply_prim_compare ann xs (>=)

    let apply_is_lteq ann xs =
      apply_prim_compare ann xs (<=)

    let apply_escape ann xs =
      match refine_params_to_unary xs with
      | Ast.Value.Prim (_, String x) ->
        Ast.Value.Prim (ann, String (String.escaped x))
      | _ ->
        raise (Bad_inj_params "expected a string")

    let apply_to_string ann xs =
      match refine_params_to_unary xs with
      | Ast.Value.Prim (_, Integer x) ->
        Ast.Value.Prim (ann, String (string_of_int x))
      | Prim (_, Float x) ->
        Prim (ann, String (string_of_float x))
      | _ ->
        raise (Bad_inj_params "expected an integer or float")

    let apply_get ann xs =
      match refine_params_to_binary xs ~left: "field" ~right: "in" with
      | (Ast.Value.Prim (_, String field), Record (_, from)) ->
        ( match List.find from.props (fun p -> p.key = field) with
          | None ->
            raise (Bad_inj_params (sprintf "field %s not in record" field))
          | Some prop -> prop.value
        )
      | _ ->
        raise (Bad_inj_params "expected a string (property key) and record")

    let apply_set ann xs =
      match refine_params_to_ternary xs ~left: "field" ~middle: "to" ~right: "in" with
      | (Ast.Value.Prim (_, String field), to_, Record (from_ann, from)) ->
        ( match List.findi from.props (fun _ p -> p.key = field) with
          | None ->
            raise (Bad_inj_params (sprintf "field %s not in record" field))
          | Some (idx, _) ->
            let to_prop =
              { Ast.Property.key = field;
                value = to_;
              } in
            Ast.Value.Record
              ( from_ann,
                { head = from.head;
                  props = List.set_at_exn from.props ~n: to_prop ~idx: idx;
                }
              )
        )
      | _ ->
        raise (Bad_inj_params "expected a string (property key) and record")

    let apply_to_list ann xs =
      xs |>
      List.map ~f: (fun x -> Ann_lit.Value.property (Misc.Value.annotation x.Ast.Property.value) x) |>
      Ann_lit.Value.list ann

    let apply_from_list ann xs =
      try refine_params_to_unary xs |>
          Lit.Value.to_list |>
          List.map ~f: Lit.Value.to_property |>
          inj_splice ann with
      | Lit.Value.Bad_form "list" ->
        raise (Bad_inj_params "expected a list")
      | Lit.Value.Bad_form "property" ->
        raise (Bad_inj_params "expected a list of properties (not all items are properties)")

    let apply_amb_union ann xs =
      inj_amb_union ann (refine_params_to_list xs)

    let inject_apply ann head prms =
      let ann = Ann.inj_app ann in
      match head with
      | "Add" -> apply_add ann prms
      | "Subtract" -> apply_subtract ann prms
      | "Multiply" -> apply_multiply ann prms
      | "Divide" -> apply_divide ann prms
      | "App" -> apply_app ann prms
      | "RegexMatch" -> apply_regex_match ann prms
      | "IsEqual" -> apply_is_equal ann prms
      | "IsGT" -> apply_is_gt ann prms
      | "IsLT" -> apply_is_lt ann prms
      | "IsGTEQ" -> apply_is_gteq ann prms
      | "IsLTEQ" -> apply_is_lteq ann prms
      | "Escape" -> apply_escape ann prms
      | "ToString" -> apply_to_string ann prms
      | "Get" -> apply_get ann prms
      | "Set" -> apply_set ann prms
      | "ToList" -> apply_to_list ann prms
      | "FromList" -> apply_from_list ann prms
      | "AmbUnion" -> apply_amb_union ann prms
      | _ ->
        raise (Invalid_argument "unexpected inj app - not caught in inj_head_reduces")

    let inj_head_reduces head =
      match head with
      | "Freeze"
      | "Discard"
      | "Imports"
      | "Code"
      | "Splice" -> false
      | "Add"
      | "Subtract"
      | "Multiply"
      | "Divide"
      | "App"
      | "RegexMatch"
      | "IsEqual"
      | "IsGT"
      | "IsLT"
      | "IsGTEQ"
      | "IsLTEQ"
      | "Escape"
      | "ToString"
      | "Get"
      | "ToList"
      | "FromList"
      | "AmbUnion" -> true
      | _ -> raise (Bad_inj_head head)

    let try_splice_prop x =
      match x.Ast.Property.value with
      | Ast.Value.Prim (_, _)
      | Pipe (_, _) -> None
      | Record (_, v) ->
        if Lit.Value.try_symbol_of_head v.head <> Some "#Splice" then
          None
        else
          Some (x.key, v.props)

    let try_inject_apply_record_splice ann x =
      match List.find_mapi x.Ast.Record.props
            ( fun idx x ->
                try_splice_prop x |>
                Option.map ~f: (fun r -> (idx, r))
            ) with
      | None -> x
      | Some (idx, (c_key, txs)) ->
        let (lxs, _, rxs) = List.split_on_n_exn x.props ~n: idx in
        if c_key = "..." then
          (** B[a: 5; ...: #Splice[foo: 7; ...: >bar>...-baz>z]] =>
              B[a: 5; foo: 7; ...: B>bar>...-baz>z] *)
          { head = x.head;
            props = lxs @ txs @ rxs;
          }
        else
          (** B[a: 5; b: #Splice[foo: 7; ...: >bar>...-baz>z]] =>
              #Splice[foo: B[a: 5; b: 7]; ...: B[a: 5; b: >bar>...-baz>z]] *)
          { head = Lit.Value.head_of_symbol (Ann.splice_head ann) "#Splice";
            props =
              List.map txs
              ( fun tx ->
                  let cx =
                    { Ast.Property.key = c_key;
                      value = tx.value;
                    } in
                  { Ast.Property.key = tx.key;
                    value =
                      Ast.Value.Record
                      ( ann,
                        { head = x.head;
                          props = lxs @ [cx] @ rxs
                        }
                      )
                  }
              );
          }

    let try_inject_apply_record_single ann x =
      if List.exists x.Ast.Record.props (fun p -> Misc.Value.has_pipe p.value) then
        Ast.Value.Record (ann, x) (** Wait for properties to be resolved *)
      else
        let open Option.Let_syntax in
        match
          x.head |>
          Lit.Value.try_symbol_of_head >>=
          String.chop_prefix ~prefix: "#" |>
          Option.filter ~f: inj_head_reduces with
        | None -> Record (ann, x)
        | Some head -> inject_apply ann head x.props

    let try_inject_apply_record ann x =
      x |>
      try_inject_apply_record_splice ann |>
      try_inject_apply_record_single ann

    let try_inject_apply x =
      match x with
      | Ast.Value.Prim (ann, x) -> Ast.Value.Prim (ann, x)
      | Record (ann, x) -> try_inject_apply_record ann x
      | Pipe (ann, x) -> Pipe (ann, x)
  end
end

module Pure = Annotate(Ann.Pure)
