open Base

module Dep = struct
  type t = Ast.Pure.phase list
  type mutable_t = Ast.Taint.phase list

  exception Missing of string
  exception Broken of string * exn

  let empty = [[]]

  let app x y =
    Misc.Phase.app_phases x y
end

type dep = Dep.t
type mutable_dep = Dep.mutable_t

module Dep_resolver = struct
  type t = string

  let at_dir loc = loc

  let for_file loc = at_dir (Filename.dirname loc)

  let get_base_loc () =
    Unix.get_home_dir () |>
    Option.map ~f: (fun home -> Filename.concat home ".descript")

  let force_resolve_at path loc =
    Parse.Program.from_file path |>
    Misc.Program.map_anns ~f: (fun _ -> ()) |>
    (fun prg -> prg.phases)

  let force_resolve_map_at path loc ~f: f ~format: format =
    let text = In_channel.read_all path in
    let module Text = struct let x = text end in
    let module Print_text = Print.Taint(Text) in
    Parse.Program.from_string text |>
    (fun prg -> { Ast.Program.phases = f prg.phases; query = prg.query }) |>
    Print_text.Program.to_string ~format: format |>
    fun out -> Out_channel.write_all path ~data: out

  let guard_resolve_file_at f_res dir loc =
    let path = Filename.concat dir loc ^ ".dscr" in
    if Sys.file_exists path ~follow_symlinks: true <> `Yes then
      raise (Dep.Missing loc)
    else
      try f_res path loc with
      | Parse.Error err ->
        raise (Dep.Broken (loc, Parse.Error err))

  let rec guard_resolve_dir_at f_res (f_comb : 'a list -> f: ('a -> 'b) -> 'c) dir loc =
    let path = Filename.concat dir loc in
    if Sys.file_exists path ~follow_symlinks: true <> `Yes then
      raise (Dep.Missing (loc ^ "/*"))
    else
      Sys.ls_dir path |>
      List.filter_map ~f: (String.chop_suffix ~suffix: ".dscr") |>
      List.filter ~f:
        (fun sub_loc -> not (String.is_suffix sub_loc ~suffix: ".refactor")) |>
      List.map ~f: (Filename.concat loc) |>
      f_comb ~f: (guard_resolve_at f_res f_comb dir)
  and guard_resolve_at f_res f_comb dir loc =
    match String.chop_suffix loc ~suffix: "/*" with
    | None -> guard_resolve_file_at f_res dir loc
    | Some loc_dir -> guard_resolve_dir_at f_res f_comb dir loc_dir

  let resolve_at dir loc =
    guard_resolve_at
      force_resolve_at
      ( fun x ~f: f ->
          x |>
          List.map ~f: f |>
          List.reduce ~f: Dep.app |>
          Option.value ~default: Dep.empty
      )
      dir
      loc

  let resolve_map_at dir loc ~f: f ~format: format =
    guard_resolve_at
      (force_resolve_map_at ~f: f ~format: format)
      List.iter
      dir
      loc

  let resolve rsvr_loc loc =
    try resolve_at rsvr_loc loc with
    | Dep.Missing dep ->
      match get_base_loc () with
      | None -> raise (Dep.Missing dep)
      | Some base_loc -> resolve_at base_loc loc

  let resolve_map rsvr_loc loc ~f: f =
    try resolve_map_at rsvr_loc loc ~f: f with
    | Dep.Missing dep ->
    match get_base_loc () with
    | None -> raise (Dep.Missing dep)
    | Some base_loc -> resolve_map_at base_loc loc ~f: f
end

type dep_resolver = Dep_resolver.t

module Value = struct
  include Ast.Value

  let is_inj_imports_record x =
    Lit.Value.try_symbol_of_head (x.Ast.Record.head) = Some "#Imports"

  let is_inj_imports x =
    match x with
    | Record (_, x) -> is_inj_imports_record x
    | _ -> false
end

module Reducer = struct
  include Ast.Reducer

  let is_inj_imports x =
    Value.is_inj_imports x.input
end

module Phase = struct
  include Ast.Phase

  exception Malformed_deps of string

  let find_imports_exn x ~rsvr: rsvr =
    match List.find x Reducer.is_inj_imports with
    | None -> None
    | Some red ->
      Some
        ( Misc.Value.annotation red.input,
          red.output |>
          Misc.Value.Map.anns ~f: (fun _ -> ()) |>
          Lit.Value.to_list |>
          List.map ~f: Lit.Value.to_string
        )

  let find_imports x ~rsvr: rsvr =
    try find_imports_exn x ~rsvr: rsvr with
    | Lit.Value.Bad_form rsn -> raise (Malformed_deps rsn)

  let rec map_import dep ~f: f ~rsvr: rsvr ~format: format =
    try Dep_resolver.resolve_map rsvr dep ~format: format ~f:
          ( fun dep ->
              dep |>
              List.map ~f: (map_imports ~f: f ~rsvr: rsvr ~format: format) |>
              f
          ) with
    | Malformed_deps rsn ->
      raise (Dep.Broken (dep, Malformed_deps rsn))
  and map_imports :
    'ann.
    'ann t ->
    f: (mutable_dep -> mutable_dep) ->
    rsvr: dep_resolver ->
    format: Print.format ->
    'ann t
    = fun x ~f: f ~rsvr: rsvr ~format: format ->
      match find_imports x ~rsvr: rsvr with
      | None -> x
      | Some (_, deps) ->
        List.iter deps (map_import ~f: f ~rsvr: rsvr ~format: format);
        x
end

module Program = struct
  include Ast.Program

  let map_imports x ~f: f ~rsvr: rsvr ~format: format =
    { Ast.Program.phases =
        x.Ast.Program.phases |>
        List.map ~f: (Phase.map_imports ~f: f ~rsvr: rsvr ~format: format);
      query = x.query;
    }
end

module Ann = struct
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    (** Argument is the annotation of the #Imports[] record. *)
    val dep : t -> t
  end

  module Pure : Type with module T = Unit = struct
    module T = Unit

    type t = T.t

    let dep () = ()
  end

  module Taint : Type with module T = Taint = struct
    module T = Taint

    type t = T.t

    let dep x = x
  end
end

module Annotate (Ann : Ann.Type) = struct
  module Ann_ast = Ast.Annotate(Ann.T)

  module Phase = struct
    include Ann_ast.Phase

    let annotate_dep ann dep =
      let ann = Ann.dep ann in
      dep |> List.map ~f: (Misc.Phase.map_anns ~f: (fun () -> ann))

    let rec resolve_import ann dep ~rsvr: rsvr =
      try Dep_resolver.resolve rsvr dep |>
          annotate_dep ann |>
          List.map ~f: (resolve_imports ~rsvr: rsvr) |>
          Misc.Phase.join_phases with
      | Phase.Malformed_deps rsn ->
        raise (Dep.Broken (dep, Phase.Malformed_deps rsn))
    and resolve_imports x ~rsvr: rsvr =
      match Phase.find_imports x ~rsvr: rsvr with
      | None -> [x]
      | Some (ann, deps) ->
        let raw_x = List.filter x (fun red -> not (Reducer.is_inj_imports red)) in
        deps |>
        List.map ~f: (resolve_import ann ~rsvr: rsvr) |>
        List.fold ~init: [raw_x] ~f: Dep.app
  end

  module Program = struct
    include Ann_ast.Program

    let resolve_imports x ~rsvr: rsvr =
      { Ast.Program.phases =
          x.Ast.Program.phases |>
          List.map ~f: (Phase.resolve_imports ~rsvr: rsvr) |>
          Misc.Phase.join_phases;
        query = x.query;
      }
  end
end

module Pure = Annotate(Ann.Pure)
module Taint = Annotate(Ann.Taint)
