(* Derived from https://v1.realworldocaml.org/v1/en/html/parsing-with-ocamllex-and-menhir.html *)
{
open Util
open Lexing
open Grammar

exception SyntaxError of string

let loc_of_position p =
  { Loc.Loc.pos = p.pos_cnum;
    line = p.pos_lnum;
    column = p.pos_cnum - p.pos_bol;
  }

let pre_loc lexbuf =
  loc_of_position lexbuf.lex_start_p

let post_loc lexbuf =
  loc_of_position lexbuf.lex_curr_p

let range lexbuf =
  { Loc.Range.start = pre_loc lexbuf;
    after = post_loc lexbuf;
  }

let ranged x lexbuf =
  { Loc.Ranged.range = range lexbuf;
    value = x;
  }

let ranged_end x lexbuf =
  (x, post_loc lexbuf)

let ranged_start f lexbuf =
  let start = pre_loc lexbuf in
  let (value, after) = f lexbuf in
  { Loc.Ranged.range = { start = start; after = after };
    value = value;
  }
}

let integer = '-'? ['0'-'9'] ['0'-'9']*
let digit = ['0'-'9']
let dec = '.' digit*
let exp = ['e' 'E'] ['-' '+']? digit+
let float = digit+ dec? exp?
let white = [' ' '\t']+
let newline = '\r' | '\n' | "\r\n"
let not_newline = [^ '\r' '\n']
let symbol = ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '0'-'9' '_']*

rule read_block_comment = parse
  | "*/"               { () }
  | newline            { Lexing.new_line lexbuf; read_block_comment lexbuf }
  | [^ '*' '\r' '\n']+ { read_block_comment lexbuf }
  | '*'                { read_block_comment lexbuf }
  | _                  { raise (SyntaxError ("Illegal block comment character: " ^ Lexing.lexeme lexbuf)) }
  | eof                { raise (SyntaxError ("Block comment is not terminated")) }
and read_string buf = parse
  | '"'                     { ranged_end (Buffer.contents buf) lexbuf }
  | newline                 { Lexing.new_line lexbuf; read_string buf lexbuf }
  | '\\' '"'                { Buffer.add_char buf '"'; read_string buf lexbuf }
  | '\\' '\\'               { Buffer.add_char buf '\\'; read_string buf lexbuf }
  | '\\' 'b'                { Buffer.add_char buf '\b'; read_string buf lexbuf }
  | '\\' 'f'                { Buffer.add_char buf '\012'; read_string buf lexbuf }
  | '\\' 'n'                { Buffer.add_char buf '\n'; read_string buf lexbuf }
  | '\\' 'r'                { Buffer.add_char buf '\r'; read_string buf lexbuf }
  | '\\' 't'                { Buffer.add_char buf '\t'; read_string buf lexbuf }
  | [^ '"' '\\' '\r' '\n']+ { Buffer.add_string buf (Lexing.lexeme lexbuf); read_string buf lexbuf }
  | _                       { raise (SyntaxError ("Illegal string character: " ^ Lexing.lexeme lexbuf)) }
  | eof                     { raise (SyntaxError ("String is not terminated")) }
and read_regexp buf = parse
  | '/'                     { ranged_end (Regexp.parse (Buffer.contents buf)) lexbuf }
  | '\\' '/'                { Buffer.add_char buf '/'; read_regexp buf lexbuf }
  | '\\'                    { Buffer.add_char buf '\\'; read_regexp buf lexbuf }
  | [^ '/' '\\' '\r' '\n']+ { Buffer.add_string buf (Lexing.lexeme lexbuf); read_regexp buf lexbuf }
  | _                       { raise (SyntaxError ("Illegal regexp character: " ^ Lexing.lexeme lexbuf)) }
  | eof                     { raise (SyntaxError ("Regexp is not terminated")) }
and read = parse
  | white             { read lexbuf }
  | newline           { Lexing.new_line lexbuf; read lexbuf }
  | "//" not_newline* { read lexbuf }
  | "/*"              { read_block_comment lexbuf; read lexbuf }
  | integer           { Integer (ranged (int_of_string (Lexing.lexeme lexbuf)) lexbuf) }
  | float             { Float (ranged (float_of_string (Lexing.lexeme lexbuf)) lexbuf) }
  | '"'               { String (ranged_start (read_string (Buffer.create 17)) lexbuf) }
  | '/'               { Regexp (ranged_start (read_regexp (Buffer.create 17)) lexbuf) }
  | '$'               { Dollar (pre_loc lexbuf) }
  | '#'               { Hash (pre_loc lexbuf) }
  | '<'               { Angle_bwd (range lexbuf) }
  | '>'               { Angle_fwd (pre_loc lexbuf) }
  | '^'               { Angle_up (post_loc lexbuf) }
  | ':'               { Colon }
  | ';'               { Semicolon }
  | '-'               { Dash }
  | "---"             { Phase_sep }
  | "..."             { Ellipsis (post_loc lexbuf) }
  | '?'               { Question }
  | '['               { Open_bracket }
  | ']'               { Close_bracket (post_loc lexbuf) }
  | '{'               { Open_brace (pre_loc lexbuf) }
  | '}'               { Close_brace }
  | symbol            { Symbol (ranged (Lexing.lexeme lexbuf) lexbuf) }
  | eof               { Eof }
  | _                 { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
