open Base

module Ann : sig
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    module Side_reduce : Side_reduce.Ann.Type with module T = T

    module Remainder_m : Remainder_m.Ann.Type with module T = T

    module Inject : Inject.Ann.Type with module T = T

    val consume : t -> t

    val produce : t -> t
  end

  module Pure : Type with module T = Unit
  module Taint : Type with module T = Taint
end

module Opt : sig
  module type Type = sig
    type phase

    module Value : sig
      module type Type = sig
        type abs

        type t = abs Ast.Pure.value

        val narrow_phase : phase -> t -> Ast.Pure.phase
      end

      module Reg : Type with type abs = Nothing.t
      module In : Type with type abs = Ast.matcher
      module Out : sig
        module Local (CIn : sig val x : Ast.Pure.in_value end) :
          Type with type abs = Ast.path
      end
    end

    module Phase : sig
      val fold_reduce_using :
        'ann Ast.phase list ->
        f: (phase -> 'ann Ast.phase -> 'ann Ast.phase) ->
        phase
    end
  end
end

module Make (Opt : Opt.Type) : sig
  module Reduce : sig
    module type Type = sig
      type t

      type 'a reduce_m

      val reduce : Opt.phase -> t -> t reduce_m
    end
  end

  module Annotate (Ann : Ann.Type) : sig
    module Value : sig
      module Reg : Reduce.Type
        with type t = Ast.Annotate(Ann.T).Value.Reg.t
         and type 'a reduce_m = 'a

      module In : Reduce.Type
        with type t = Ast.Annotate(Ann.T).Value.In.t
         and type 'a reduce_m = (Side_reduce.out, 'a) Side_reduce.monad

      module Out : sig
        module Local (CIn : sig val x : Ast.Annotate(Ann.T).in_value end) : Reduce.Type
          with type t = Ast.Annotate(Ann.T).Value.Out.t
           and type 'a reduce_m = (Side_reduce.input, 'a) Side_reduce.monad
      end
    end

    module Reducer :
      Reduce.Type with
      type t = Ast.Annotate(Ann.T).Reducer.t and
    type 'a reduce_m = 'a list

    module Phase : sig
      include (
        Reduce.Type
        with type t = Ast.Annotate(Ann.T).Phase.t
         and type 'a reduce_m = 'a
      )

      (** Uses the first phase to reduce the second, then the first and
          second phase to reduce the third, etc. (previous phases to reduce
          next phases). Returns all these reduced phases concatetated. *)
      val fold_reduce : t list -> Opt.phase

      val from_slow : t -> Opt.phase
    end

    module Program : sig
      type t = Ast.Annotate(Ann.T).Program.t

      (** Fold-reduce all phases, then use the result phase to reduce the
          query. *)
      val interpret : t -> Ast.Annotate(Ann.T).reg_value

      (** Reduce all of the values within the program - reduce all phases
          and the query. *)
      val macro_reduce : Opt.phase -> t -> t
    end
  end

  module Pure : module type of Annotate(Ann.Pure)
  module Taint : module type of Annotate(Ann.Taint)
end
