open Base

exception Remainder_in_value_position
exception Value_in_remainder_position
exception Zip_different_remainders

type 'a t

module Let_syntax : Monad.Type with type 'a t = 'a t

include Monad.Type with type 'a t := 'a t

val from_props : 'a Ast.property list -> 'a t

val to_single : 'a t -> 'a

val splice : 'a t -> 'a Ast.property list t

val flatten : 'a t -> 'a list

module Record : sig
  type 'a _t = 'a t
  type 'v t = 'v Ast.record

  val at_elem : Ast.path_elem -> 'v t -> 'v _t option

  val at_elem_exn : Ast.path_elem -> 'v t -> 'v _t

  module Traverse : sig
    val surface : 'a t -> f: ('a -> 'b _t) -> 'b t _t

    (** Still applies nesting. *)
    val head : 'a t -> f: ('a -> 'a _t) -> 'a t _t
  end
end

module Value : sig
  type 'a _t = 'a t
  type ('ann, 'abs) t = ('ann, 'abs) Ast.value

  val at_elem : Ast.path_elem -> ('ann, 'abs) t -> ('ann, 'abs) t _t option

  val at_elem_exn : Ast.path_elem -> ('ann, 'abs) t -> ('ann, 'abs) t _t

  val at_path_exn : Ast.path -> ('ann, 'abs) t -> ('ann, 'abs) t _t

  module Traverse : module type of Misc.Value.Gen_traverse(Let_syntax)(Record.Traverse)
end

module Ann : module type of Inject.Ann

module Annotate (Ann : Ann.Type) : sig
  type ann = Ann.t

  val to_value : ann -> 'abs Ast.Annotate(Ann.T).value t -> 'abs Ast.Annotate(Ann.T).value

  module Value : sig
    type 'ann t = 'ann Ast.Annotate(Ann.T).value

    (** All possible paths in this value, and the values they correspond to. *)
    val locations : 'abs t -> (Ast.path * 'abs t) list

    (** All paths which refer to [c] in [x]. *)
    val locations_of : 'abs t -> 'abs t -> Ast.path list
  end
end

module Pure : module type of Annotate(Ann.Pure)
module Taint : module type of Annotate(Ann.Taint)
