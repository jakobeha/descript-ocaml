open Base

exception Remainder_in_value_position
exception Value_in_remainder_position
exception Zip_different_remainders

module Let_syntax = struct
  type 'a t =
    | Single of 'a
    | Remainder of 'a t Ast.property list

  let return x =
    Single x

  let rec map x ~f: f =
    match x with
    | Single x ->
      Single (f x)
    | Remainder ps ->
      Remainder (ps |> List.map ~f: (Misc.Property.map ~f: (map ~f: f)))

  let rec both x y =
    match (x, y) with
    | (Single x, Single y) ->
      Single (x, y)
    | (Single x, Remainder yps) ->
      Remainder (yps |> List.map ~f: (Misc.Property.map ~f: (both (Single x))))
    | (Remainder xps, Single y) ->
      Remainder (xps |> List.map ~f: (Misc.Property.map ~f: (fun x -> both x (Single y))))
    | (Remainder xps, Remainder yps) ->
      match Misc.Property.align xps yps with
      | None -> raise Zip_different_remainders
      | Some (xps, yps) ->
        Remainder
        ( List.map2_exn xps yps
          ( fun x y ->
              { Ast.Property.key = x.Ast.Property.key; (* = y.key *)
                value = both x.value y.Ast.Property.value;
              }
          )
        )

  let rec bind x ~f: f =
    match x with
    | Single x -> f x
    | Remainder xps ->
      Remainder (xps |> List.map ~f: (Misc.Property.map ~f: (bind ~f: f)))

  let rec join x =
    match x with
    | Single x -> x
    | Remainder xps ->
      Remainder (xps |> List.map ~f: (Misc.Property.map ~f: join))
end

include Let_syntax

module M_list = List.Of_monad(Let_syntax)
module M_option = Option.Of_monad(Let_syntax)
module M_property = Misc.Property.Of_monad(Let_syntax)

let from_props xps =
  Remainder (xps |> List.map ~f: (Misc.Property.map ~f: (fun x -> Single x)))

let to_single x =
  match x with
  | Single x -> x
  | Remainder _ -> raise Remainder_in_value_position

let splice x =
  match x with
  | Single _ -> raise Value_in_remainder_position
  | Remainder xps ->
    xps |>
    List.map ~f: M_property.all |>
    M_list.all

let rec flatten x =
  match x with
  | Single x -> [x]
  | Remainder xps ->
    xps |>
    List.map ~f: (fun xp -> xp.Ast.Property.value) |>
    List.concat_map ~f: flatten

module Property = struct
  include Ast.Property

  let splice x =
    if x.key = "..." then
      splice x.value
    else
      M_property.all x |>
      map ~f: (fun r -> [r])
end

module Record = struct
  type 'a _t = 'a t

  include Ast.Record

  let at_elem e x =
    match e with
    | Ast.Path_elem.Head -> Some (Single x.head)
    | Property k ->
      x.props |>
      List.find ~f: (fun p -> k = p.Ast.Property.key) |>
      Option.map ~f: (fun p -> Single (p.Ast.Property.value))
    | Remainder subs ->
      x.props |>
      List.filter ~f: (fun p -> not (List.mem subs p.Ast.Property.key ~equal: (=))) |>
      fun props -> Some (from_props props)

  let at_elem_exn e x =
    match at_elem e x with
    | None ->
      raise (Misc.Child_not_found (Misc.Path_elem.debug_string e))
    | Some r -> r

  module Traverse = struct
    let surface x ~f: f =
      let%map head = f x.head
      and props =
        x.props |>
        List.map ~f:
        ( fun prop ->
            prop |>
            Misc.Property.map ~f: f |>
            Property.splice
        ) |>
        M_list.all |>
        map ~f: List.concat in
      { head = head;
        props = props;
      }

    let head x ~f: f =
      let%map head = f x.head in
      { head = head;
        props = x.props;
      }
  end

end

module Value = struct
  type 'a _t = 'a t

  include Ast.Value

  let at_elem e v =
    match v with
    | Record (_, v) -> Record.at_elem e v
    | _ -> None

  let at_elem_exn e v =
    match at_elem e v with
    | None ->
      raise (Misc.Child_not_found (Misc.Path_elem.debug_string e))
    | Some r -> r

  let rec at_path_exn l v =
    match l with
    | [] -> Single v
    | e :: es ->
      at_elem_exn e v |>
      bind ~f: (at_path_exn es)

  module Traverse = Misc.Value.Gen_traverse(Let_syntax)(Record.Traverse)
end

module Ann = Inject.Ann

module Annotate (Ann : Ann.Type) = struct
  type ann = Ann.t

  module Ann_ast = Ast.Annotate(Ann.T)
  module Ann_inject = Inject.Annotate(Ann)

  let rec to_value ann x =
    match x with
    | Single x -> x
    | Remainder xs ->
      xs |>
      List.map ~f: (Misc.Property.map ~f: (to_value ann)) |>
      Ann_inject.Value.inj_splice ann

  module Value = struct
    include Ann_ast.Value

    let record_head_location x =
      (Ast.Path_elem.Head, x)

    let record_prop_imm_location x =
      if x.Ast.Property.key = "..." then
        None
      else
        Some (Ast.Path_elem.Property x.Property.key, x.value)

    let record_remainder_imm_location ann ps =
      match List.findi ps (fun _ p -> p.Ast.Property.key = "...") with
      | None ->
        let keys = List.map ps (fun op -> op.Ast.Property.key) in
        (Ast.Path_elem.Remainder keys, Ann_inject.Value.inj_splice ann [])
      | Some (idx, p) ->
        let other_keys =
          ps |>
          List.remove_at_exn ~idx: idx |>
          List.map ~f: (fun op -> op.Ast.Property.key) in
        (Remainder other_keys, p.value)

    let record_imm_locations ann x =
      record_head_location x.Ast.Record.head ::
      record_remainder_imm_location ann x.props ::
      List.filter_map x.props record_prop_imm_location

    let rec sub_locations (e, chl) =
      List.map (locations chl) (fun (l, dec) -> (e :: l, dec))
    and child_locations x =
      match x with
      | Ast.Value.Record (ann, x)
          when Lit.Value.try_symbol_of_head x.Ast.Record.head <> Some "#Splice" ->
        List.concat_map (record_imm_locations ann x) sub_locations
      | _ -> []
    and locations x =
      ([], x) :: child_locations x

    let locations_of tr_ch x =
      x |>
      locations |>
      List.filter ~f: (fun (l, v) -> tr_ch = v) |>
      List.map ~f: (fun (l, _) -> l)
  end
end

module Pure = Annotate(Ann.Pure)
module Taint = Annotate(Ann.Taint)
