open Core

module Dep : sig
  type t = Ast.Pure.phase list
  type mutable_t = Ast.Taint.phase list

  exception Missing of string
  exception Broken of string * exn

  val empty : t

  val app : t -> t -> t
end

type dep = Dep.t
type mutable_dep = Dep.mutable_t

module Dep_resolver : sig
  type t

  val at_dir : string -> t

  val for_file : string -> t

  val resolve : t -> string -> dep

  val resolve_map :
    t ->
    string ->
    f: (mutable_dep -> mutable_dep) ->
    format: Print.format ->
    unit
end

type dep_resolver = Dep_resolver.t

module Phase : sig
  type 'ann t = 'ann Ast.phase

  exception Malformed_deps of string

  val map_imports :
    'ann t ->
    f: (mutable_dep -> mutable_dep) ->
    rsvr: dep_resolver ->
    format: Print.format ->
    'ann t
end

module Program : sig
  type 'ann t = 'ann Ast.program

  val map_imports :
    'ann t ->
    f: (mutable_dep -> mutable_dep) ->
    rsvr: dep_resolver ->
    format: Print.format ->
    'ann t
end

module Ann : sig
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    (** Argument is the annotation of the #Imports[] record. *)
    val dep : t -> t
  end

  module Pure : Type with module T = Unit
  module Taint : Type with module T = Taint
end

module Annotate (Ann : Ann.Type) : sig
  module Phase : sig
    type t = Ast.Annotate(Ann.T).phase

    val resolve_imports : t -> rsvr: dep_resolver -> t list
  end

  module Program : sig
    type t = Ast.Annotate(Ann.T).program

    val resolve_imports : t -> rsvr: dep_resolver -> t
  end
end

module Pure : module type of Annotate(Ann.Pure)
module Taint : module type of Annotate(Ann.Taint)
