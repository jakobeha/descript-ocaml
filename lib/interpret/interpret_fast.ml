open Base

module Record = struct
  include
    ( Ast_fast.Record : module type of Ast_fast.Record
      with module Shape := Ast_fast.Record.Shape
    )

  module Shape = struct
    include Ast_fast.Record.Shape

    let is_valid xs =
      let open Core in
      List.count xs ~f: (fun x -> x = "...") <= 1

    let concrete_of_record x =
      List.map x.Ast.Record.props (fun p -> p.key)

    let concrete_strip_from x =
      x.Ast.Record.head :: List.map x.props (fun p -> p.value)

    let all_of_record x =
      let concrete = concrete_of_record x in
      let sub_ranges =
        List.range 0 (List.length concrete) |>
        List.concat_map ~f:
        ( fun first ->
            List.range first (List.length concrete) |>
            List.map ~f: (fun last -> (first, last + 1))
        ) in
      let remainders =
        sub_ranges |>
        List.map ~f:
          (fun sub_range -> List.splice_exn concrete sub_range ["..."]) |>
        List.filter ~f: is_valid in
      concrete :: remainders
  end
end

module Matcher = struct
  include Ast.Matcher

  let to_discrete i =
    match i with
    | Any
    | Prim
    | Record
    | Integer
    | Float
    | String -> i
    | Regexp _ -> String

  let supers_for_prim i =
    match i with
    | Ast.Prim.Integer _ ->
      [ Any;
        Prim;
        Integer;
      ]
    | Float _ ->
      [ Any;
        Prim;
        Float;
      ]
    | String _ ->
      [ Any;
        Prim;
        String;
      ]

  let supers_for_record =
    [ Any;
      Record;
    ]

  let supers_for_matcher i =
    match i with
    | Any -> []
    | Prim
    | Record ->
      [ Any
      ]
    | Integer
    | Float
    | String ->
      [ Prim;
        Any;
      ]
    | Regexp _ ->
      [ String;
        Prim;
        Any;
      ]
end

module Value = struct
    include Ast.Value

  (* Result might be more specific than actual intersection. *)
  let super_intersect x _ = x

  let detach_shape x =
    let concrete_shape = Record.Shape.concrete_of_record x
    and concrete_strip = Record.Shape.concrete_strip_from x in
    let sub_ranges =
      List.range 0 (List.length concrete_shape) |>
      List.concat_map ~f:
      ( fun first ->
          List.range first (List.length concrete_shape) |>
          List.map ~f: (fun last -> (first, last + 1))
      ) in
    let concrete = (concrete_shape, concrete_strip)
    and remainders =
      List.filter_map sub_ranges
      ( fun (first, after) ->
          let shape = List.splice_exn concrete_shape (first, after) ["..."] in
          Option.some_if (Record.Shape.is_valid shape)
          ( shape,
            List.split_on_range_exn concrete_strip (first + 1, after + 1) |>
            fun (ls, cs, rs) ->
              ls @ [List.reduce_exn cs ~f: super_intersect] @ rs
          )
      ) in
    concrete :: remainders
end

module Narrow = struct
  include Ast_fast.Narrow

  exception Mismatch_shape

  let to_done x =
    match x with
    | Done x -> x
    | Continue _ -> raise Mismatch_shape

  let to_continue x =
    match x with
    | Done _ -> raise Mismatch_shape
    | Continue x -> force !x

  let create () =
    { leaves = Ast_fast.Value.In.Leaf.Table.create ();
      super_leaves = Ast.Matcher.Table.create ();
      records = Ast_fast.Record.Shape.Table.create ();
    }

  let rec add_to_table :
    'a 'k. ('k, 'a) Hashtbl.t ->
    'k ->
    fs: ((unit -> 'a) * ('a -> unit)) ->
    unit =
    fun p key ~fs: (f_new, f_add) ->
      let d = Hashtbl.find_or_add p key ~default: f_new in
      f_add d
  and add_to_narrow_record :
    'a. Ast.Pure.in_value list ->
    fs: ((unit -> 'a) * ('a -> unit)) ->
    (unit -> 'a record) * ('a record -> unit) =
    fun xs ~fs: ((f_new, f_add) as fs) ->
      match xs with
      | [] ->
        ( (fun () -> Done (f_new ())),
          ( function
            | Done d -> f_add d
            | Continue _ -> raise Mismatch_shape
          )
        )
      | x :: xs ->
        ( (fun () -> Continue (ref (lazy (create ())))),
          ( function
            | Done _ -> raise Mismatch_shape
            | Continue ld ->
              let old_ld = !ld in
              ld := lazy
              ( let d = force old_ld in
                add d x ~fs: (add_to_narrow_record xs ~fs: fs);
                d
              )
          )
        )
  and add :
    'a. 'a Ast_fast.narrow ->
    Ast.Pure.in_value ->
    fs: ((unit -> 'a) * ('a -> unit)) ->
    unit =
    fun p i ~fs: fs ->
      match i with
      | Ast.Value.Prim ((), i) ->
        add_to_table p.leaves (Prim i) ~fs: fs;
        List.iter
          (Matcher.supers_for_prim i)
          (fun i -> add_to_table p.super_leaves i ~fs: fs)
      | Record ((), i) ->
        List.iter (Value.detach_shape i)
        ( fun (shape, strip) ->
            add_to_table
              p.records
              shape
              ~fs: (add_to_narrow_record strip ~fs: fs);
        );
        List.iter
          Matcher.supers_for_record
          (fun i -> add_to_table p.super_leaves i ~fs: fs)
      | Pipe ((), i) ->
        let i = Matcher.to_discrete i in
        add_to_table p.leaves (Matcher i) ~fs: fs;
        add_to_table p.super_leaves i ~fs: fs;
        List.iter
          (Matcher.supers_for_matcher i)
          (fun i -> add_to_table p.super_leaves i ~fs: fs)
end

module Phase = struct
  include
    ( Ast_fast.Phase : module type of Ast_fast.Phase
      with module Fast := Ast_fast.Phase.Fast
    )

  module Fast = struct
    include Ast_fast.Phase.Fast

    let create () =
      { narrow = Narrow.create ();
        last_idx = 0;
      }

    let add_reducer p idx r =
      Narrow.add p.narrow r.Ast.Reducer.input
      ( (fun () -> ref []),
        (fun rs -> rs := (r, p.last_idx + idx) :: !rs)
      )

    let add p pslow =
      List.iteri pslow (add_reducer p);
      p.last_idx <- p.last_idx + List.length pslow
  end
end

module Opt : Interpret_gen.Opt.Type with type phase = Ast_fast.Phase.fast = struct
  type phase = Ast_fast.Phase.fast

  module Value = struct
    module Impl = struct
      module type Type = sig
        type abs

        type t = abs Ast.Pure.value

        val narrow_gen_pipe : 'a Ast_fast.narrow -> abs -> 'a list
      end
    end

    module type Type = sig
      type abs

      type t = abs Ast.Pure.value

      val narrow_phase : phase -> t -> Ast.Pure.phase
    end

    let narrow_prim p x =
      Hashtbl.find p.Ast_fast.Narrow.leaves (Prim x) |>
      Option.to_list

    let narrow_matcher p x =
      Hashtbl.find p.Ast_fast.Narrow.super_leaves x |>
      Option.to_list

    let narrow_super_matcher p x =
      Hashtbl.find p.Ast_fast.Narrow.leaves (Matcher x) |>
      Option.to_list

    module Make (A : Impl.Type) : sig
      include (Type with type abs = A.abs)

      val narrow_gen :
        'a Ast_fast.narrow ->
        abs Ast.Pure.value ->
        'a list
    end = struct
      type abs = A.abs

      type t = abs Ast.Pure.value

      let rec narrow_record :
        'a. 'a Ast_fast.Narrow.record ->
        abs Ast.Pure.value list ->
        'a list =
        fun p xs ->
          List.fold_until xs ~init: [p] ~finish: ident ~f:
          ( fun prevs x ->
            match prevs |>
                  List.map ~f: Narrow.to_continue |>
                  List.concat_map ~f: (fun prev -> narrow_gen prev x) with
            | [] -> Stop []
            | rs -> Continue rs
          ) |>
          List.map ~f: Narrow.to_done
      and narrow_gen :
        'a. 'a Ast_fast.narrow ->
        abs Ast.Pure.value ->
        'a list =
        fun p x ->
          match x with
          | Ast.Value.Prim ((), x) ->
            narrow_prim p x @
            ( Matcher.supers_for_prim x |>
              List.concat_map ~f: (narrow_super_matcher p)
            )
          | Ast.Value.Record ((), x) ->
            ( Value.detach_shape x |>
              List.concat_map ~f:
              ( fun (shape, strip) ->
                  match Hashtbl.find p.records shape with
                  | None -> []
                  | Some pl -> narrow_record pl strip
              )
            ) @
            ( Matcher.supers_for_record |>
              List.concat_map ~f: (narrow_super_matcher p)
            )
          | Ast.Value.Pipe ((), x) ->
            A.narrow_gen_pipe p x

      let narrow_phase p x =
        narrow_gen p.Ast_fast.Phase.Fast.narrow x |>
        List.concat_map ~f: (fun rp -> !rp) |>
        List.dedup_and_sort ~compare: (fun (_, x_idx) (_, y_idx) -> compare_int x_idx y_idx) |>
        List.map ~f: (fun (r, _) -> r)
    end

    module Reg = Make(struct
      type abs = Nothing.t

      type t = abs Ast.Pure.value

      let narrow_gen_pipe _ x = Nothing.unreachable_code x
    end)

    module In = Make(struct
      type abs = Ast.matcher

      type t = abs Ast.Pure.value

      let narrow_gen_pipe p x =
        let x = Matcher.to_discrete x in
        narrow_matcher p x @
        ( Matcher.supers_for_matcher x |>
          List.concat_map ~f: (narrow_super_matcher p)
        )
    end)

    module Out = struct
      module Local (CIn : sig val x : Ast.Pure.in_value end) = Make(struct
        type abs = Ast.path

        type t = abs Ast.Pure.value

        let narrow_gen_pipe p x =
          Misc.Value.at_path_exn x CIn.x |>
          In.narrow_gen p
      end)
    end
  end

  module Phase = struct
    let reduce_app_using p x ~f: f_reduce =
      f_reduce p x |>
      Misc.Phase.map_anns ~f: (fun _ -> ()) |>
      Phase.Fast.add p

    let fold_reduce_using xs ~f: f_reduce =
      let res = Phase.Fast.create () in
      List.iter xs (reduce_app_using res ~f: f_reduce);
      res
  end
end
