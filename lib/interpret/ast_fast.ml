open Base

module Record = struct
  module Shape = struct
    module T = struct
      type t = string list
        [@@deriving compare, hash, sexp]
    end

    include T
    include Comparable.Make(T)
    include Hashable.Make(T)
    type 'a table = 'a Table.t
  end

  type shape = Shape.t
end

module Value = struct
  module In = struct
    module Leaf = struct
      module T = struct
        type t =
          | Prim of Ast.Prim.t
          | Matcher of Ast.Matcher.t
          [@@deriving compare, hash, sexp]
      end

      include T
      include Comparable.Make(T)
      include Hashable.Make(T)
      type 'a table = 'a Table.t
    end

    type leaf = Leaf.t
  end
end

module Narrow = struct
  type 'a record =
    | Done of 'a
    | Continue of 'a record t lazy_t ref
  and 'a t =
    { leaves : 'a Value.In.Leaf.table;
      super_leaves : 'a Ast.Matcher.table;
      records : 'a record Record.Shape.table;
    }
end

type 'a narrow = 'a Narrow.t

module Phase = struct
  module Fast = struct
    type t =
      { narrow : (Ast.Pure.reducer * int) list ref narrow;
        mutable last_idx : int;
      }
  end

  type fast = Fast.t
end
