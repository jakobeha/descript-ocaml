open Core

module Ann : sig
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    val trans : t -> t
  end

  module Pure : Type with module T = Unit

  module Taint : Type with module T = Taint
end

module In : sig
  type t

  val nest : Ast.path_elem -> t -> t

  val produce_at : Ast.path -> Ast.Pure.in_value -> t

  module Annotate (Ann : Ann.Type) : sig
    val apply_all : t list -> Ast.Annotate(Ann.T).in_value -> Ast.Annotate(Ann.T).in_value
  end
end

type input = In.t

module Out : sig
  type t

  val nest : Ast.path_elem -> t -> t

  val produce_all : Ast.Pure.out_value -> Ast.Pure.in_value -> t list

  module Annotate (Ann : Ann.Type) : sig
    val apply_all : t list -> Ast.Annotate(Ann.T).out_value -> Ast.Annotate(Ann.T).out_value
  end
end

type out = Out.t

type ('sr, 'a) monad = ('sr list * 'a) list
