open Core

exception Bad_inj_head of string
exception Bad_inj_params of string

module Ann : sig
  module type Type = sig
    module T : sig
      type t
    end

    module Lit : Lit.Ann.Type with module T = T

    type t = T.t

    val amb_union_head : t -> t
    val splice_head : t -> t
    val inj_app : t -> t
  end

  module Pure : Type with module T = Unit

  module Taint : Type with module T = Taint
end

module Annotate (Ann : Ann.Type) : sig
  type ann = Ann.t

  module Value : sig
    type 'abs t = 'abs Ast.Annotate(Ann.T).value

    val is_inj_discard : 'abs t -> bool

    val inj_amb_union : ann -> 'abs t list -> 'abs t

    val inj_splice : ann -> 'abs t Ast.property list -> 'abs t

    val inject_apply : ann -> string -> 'abs t Ast.property list -> 'abs t

    (** If the value is an injected application, applies it. *)
    val try_inject_apply : 'abs t -> 'abs t
  end
end

module Pure : module type of Annotate(Ann.Pure)
