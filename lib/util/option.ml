include Core.Option

module Of_monad (A : Monad.Type) = struct
  let all x =
    match x with
    | None -> A.return None
    | Some x -> A.map x (fun x -> Some x)
end

let map2_exn x y ~f: f =
  match (x, y) with
  | (None, None) -> None
  | (Some _, None) -> raise (Invalid_argument "left optional contains a value, right doesn't")
  | (None, Some _) -> raise (Invalid_argument "left optional doesn't contain a value, right does")
  | (Some x, Some y) -> Some (f x y)
