include Core.Unix
open Core

let get_home_dir () =
  Array.find_map
    (Unix.environment ())
    (String.chop_prefix ~prefix: "HOME=")
