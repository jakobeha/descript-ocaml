include Core.String
open Str

let replace i o x =
  Str.global_replace (Str.regexp_string i) o x

let indent x =
  replace "\n" "\n\t" x
