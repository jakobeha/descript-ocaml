open Core
open Lymp
open Python_ffi

let ffi () =
  get_module (pycommunication ()) "regexp"

type t = pycallable

let parse (s : string) : t =
  let m = Mutex.create () in
  Mutex.lock m;
  let res = get_ref (ffi ()) "parse" [Pystr s] in
  Mutex.unlock m;
  res

let to_string (r : t) : string =
  get_string (builtin_ffi ()) "str" [Pyref r]

let matches (r : t) (s : string) : bool =
  get_bool r "matches" [Pystr s]

let is_equal (rx : t) (ry : t) : bool =
  get_bool rx "__eq__" [Pyref ry]

let compare (rx : t) (ry : t) : int =
  if is_equal rx ry then
    0
  else
    compare (to_string rx) (to_string ry)

let is_subset (rx : t) (ry : t) : bool =
  get_bool rx "issubset" [Pyref ry]

let is_disjoint (rx : t) (ry : t) : bool =
  get_bool rx "isdisjoint" [Pyref ry]

let intersect (rx : t) (ry : t) : t =
  get_ref rx "intersection" [Pyref ry]

let is_any (rx : t) : bool =
  get_bool rx "universal" []

let get_any () : t =
  parse ".*"

let hash_fold_t s rx =
  hash_fold_string s (to_string rx)

let t_of_sexp x = parse (string_of_sexp x)

let sexp_of_t x = sexp_of_string (to_string x)
