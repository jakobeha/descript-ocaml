module Const = struct
  module Make (A : sig type t end) = struct
    type 'a t = A.t
  end
end

module Compose = struct
  module Make (A : sig type 'a t end) (B : sig type 'a t end) = struct
    type 'a t = 'a A.t B.t
  end
end
