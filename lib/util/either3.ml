type ('a, 'b) t =
  | Neither
  | Left of 'a
  | Right of 'b
