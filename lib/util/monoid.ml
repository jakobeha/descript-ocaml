module type Type = sig
  type t

  val empty : t

  val app : t -> t -> t
end
