open Core

type 'a hashtbl = 'a Pid.Table.t

let local (gen : unit -> 'a) : unit -> 'a =
  let ptbl = Pid.Table.create () in
  fun () ->
    let pid = Unix.getpid () in
    Pid.Table.find_or_add ptbl pid ~default: gen
