include Core.List

module Make (A : sig type t end) = struct
  type t = A.t list

  let empty = []

  let app x y = x @ y
end

module Of_monad (A : Monad.Applicative.Type) = struct
  module Let_syntax = A

  let rec all xs =
    match xs with
    | [] -> A.return []
    | x :: xs ->
      let%map x = x
      and xs = all xs in
      x :: xs
end

(** Applicative instance uses the cartesian product. *)
module Cartesian = struct
  module Base = struct
    include Core.List

    module Of_monad (A : Monad.Type) = Of_monad(A)

    let both xs ys = cartesian_product xs ys
  end

  module Trans (A : Monad.Type) = Monad.Trans(A)(Base)

  include Base
end

let rec set_at_exn (xs : 'a list) ~n: (n : 'a) ~idx: (idx : int) : 'a list =
  match xs with
  | [] -> raise (Invalid_argument "expected nonempty list")
  | x :: xs ->
    if idx = 0 then
      n :: xs
    else
      x :: set_at_exn xs ~n: n ~idx: (idx - 1)

let rec remove_at_exn (xs : 'a list) ~idx: (idx : int) : 'a list =
  match xs with
  | [] -> raise (Invalid_argument "expected nonempty list")
  | x :: xs ->
    if idx = 0 then
      xs
    else
      x :: remove_at_exn xs ~idx: (idx - 1)

let subtract (ls : 'a list) (rs : 'a list) : 'a list =
  filter ls (fun l -> not (mem rs l ~equal: (=)))

(** Removes the given amount of elements from the end of the list. *)
let drop_end (xs : 'a list) ~n: (n : int) : 'a list =
  xs |>
  rev |>
  (fun xs -> drop xs n) |>
  rev

let rec split_on_n_exn (xs : 'a list) ~n: (n : int) : ('a list * 'a * 'a list) =
  match xs with
  | [] -> raise (Invalid_argument "index out of bounds")
  | x :: xs ->
    if n = 0 then
      ([], x, xs)
    else
      let (ls, c, rs) = split_on_n_exn xs ~n: (n - 1) in
      (x :: ls, c, rs)

(** Splits the list on the item which satisfies [f], or returns [None]
    if no items satisfy. *)
let rec split_on (xs : 'a list) ~f: (f_pred : 'a -> bool) : ('a list * 'a * 'a list) option =
  match xs with
  | [] -> None
  | x :: xs ->
    if f_pred x then
      Some ([], x, xs)
    else
      split_on xs ~f: f_pred |>
      Option.map ~f: (fun (ls, c, rs) -> (x :: ls, c, rs))

let contains_idx_range (xs : 'a list) ((first, after) : int * int) : bool =
  first >= 0 &&
  first < length xs &&
  after >= 0 &&
  after < length xs &&
  first <= after

(** In the first list, replaces the range ([(start, end)]) with the second list *)
let splice_exn (xs : 'a list) ((first, after) : int * int) (rep : 'a list) : 'a list =
  if not (contains_idx_range xs (first, after)) then
    raise (Invalid_argument "invalid range")
  else
    take xs first @ rep @ drop_end xs ~n: (length xs - after)

let split_on_range_exn (xs : 'a list) ((first, after) : int * int) : ('a list * 'a list * 'a list) =
  if not (contains_idx_range xs (first, after)) then
    raise (Invalid_argument "range is invalid")
  else
    let (ls, crs) = split_n xs first in
    let (cs, rs) = split_n crs (after - first) in
    (ls, cs, rs)

let strip_prefix1_by (pre : 'a) (xs : 'b list) ~equal: (f_eq : 'a -> 'b -> bool) : 'b list option =
  match xs with
  | [] -> None
  | x :: xs ->
    if f_eq pre x then
      Some xs
    else
      None

let rec strip_prefix_by (pres : 'a list) (xs : 'b list) ~equal: (f_eq : 'a -> 'b -> bool) : 'b list option =
  match pres with
  | [] -> Some xs
  | pre :: pres ->
    xs |>
    strip_prefix1_by pre ~equal: f_eq |>
    Core.Option.bind ~f: (strip_prefix_by pres ~equal: f_eq)

let strip_prefix (pres : 'a list) (xs : 'a list) : 'a list option =
  strip_prefix_by pres xs ~equal: (=)

(** Like [intersperse], but if the list is nonempty, also puts
    separators before the first item and after the last. *)
let outersperse xs ~sep: sep =
  match xs with
  | [] -> []
  | _ -> sep :: concat_map xs (fun x -> [x; sep])

(** Extras from either list are put directly in the result. *)
let rec zip_max xs ys ~f: f =
  match (xs, ys) with
  | ([], []) -> []
  | ([], ys) -> ys
  | (xs, []) -> xs
  | (x :: xs, y :: ys) ->
    f x y :: zip_max xs ys ~f: f

(** Extras on the left of either list are put directly in the result. *)
let zip_max_right xs ys ~f: f =
  rev (zip_max (rev xs) (rev ys) f)

(** If there are extras from the left list they'll be in Left,
    if there are extras from the right list they'll be in Right. *)
let zip_leftover xs ys =
  if (length xs > length ys) then
    let zip_xs, rest_xs = split_n xs (length ys) in
    (zip_exn zip_xs ys, Either3.Left rest_xs)
  else if (length xs < length ys) then
    let zip_ys, rest_ys = split_n ys (length xs) in
    (zip_exn xs zip_ys, Right rest_ys)
  else (* length xs = length ys *)
    (zip_exn xs ys, Neither)
