include Core.Bool

module And : Monoid.Type with type t = bool = struct
  type t = bool

  let empty = true

  let app x y = x && y
end

module Or : Monoid.Type with type t = bool = struct
  type t = bool

  let empty = false

  let app x y = x || y
end
