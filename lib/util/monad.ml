include
  ( Core.Monad : module type of Core.Monad with
    module Ident := Core.Monad.Ident
  )

module Applicative = struct
  module type Type = sig
    type 'a t

    val return : 'a -> 'a t

    val map : 'a t -> f: ('a -> 'b) -> 'b t

    val both : 'a t -> 'b t -> ('a * 'b) t
  end
end

module type Type = sig
  include Applicative.Type

  val bind : 'a t -> f: ('a -> 'b t) -> 'b t

  val join : 'a t t -> 'a t
end

module Traversable = struct
  module type Type = sig
    type 'a t

    module Of_monad (A : Type) : sig
      val all : 'a A.t t -> 'a t A.t
    end
  end
end

module Ident = struct
  include Core.Monad.Ident

  let both x y = (x, y)
end

module Trans (Out : Type) (In : sig
  include Type
  include Traversable.Type with type 'a t := 'a t
end) : Type with type 'a t = 'a In.t Out.t = struct
  module In_of_out = In.Of_monad(Out)

  type 'a t = 'a In.t Out.t

  let return x = Out.return (In.return x)

  let map x ~f: f =
    Out.map x (In.map ~f: f)

  let both x y =
    Out.both x y |>
    Out.map ~f: (fun (x, y) -> In.both x y)

  let bind x ~f: f =
    Out.bind x (fun x ->
        x |>
        In.map ~f: f |>
        In_of_out.all |>
        Out.map ~f: In.join)

  let join x =
    Out.bind x (fun x ->
        x |>
        In_of_out.all |>
        Out.map ~f: In.join)
end
