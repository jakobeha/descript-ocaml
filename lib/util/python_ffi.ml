open Lymp
open Process

let pycommunication : unit -> pycommunication =
  Process.local
    (fun () -> init ~exec: "python3" "/Users/jakob/Desktop/Projects/Descript-ocaml/_build/default/pylib")

let builtin_ffi () : pycallable =
  builtins (pycommunication ())
