module Make (A : Monoid.Type) = struct
  module Base = struct
    module Of_monad (U : Monad.Type) = struct
      let all (r, v) =
        U.map v (fun v -> (r, v))
    end

    type 'a t = (A.t * 'a)

    let return x = (A.empty, x)

    let map (r, v) ~f: f =
      (r, f v)

    let both (xr, xv) (yr, yv) =
      (A.app xr yr, (xv, yv))

    let bind (xr, xv) ~f: f =
      let (yr, yv) = f xv in
      (A.app xr yr, yv)

    let join (xr, (yr, v)) =
      (A.app xr yr, v)

    let tell r =
      (r, ())

    let tell_and_return r v =
      (r, v)

    let map_written (r, v) ~f: f =
      (f r, v)
  end

  module Trans (U : Monad.Type) = struct
    include Monad.Trans(U)(Base)

    let tell r =
      U.return (Base.tell r)

    let tell_and_return r v =
      U.return (Base.tell_and_return r v)

    let map_written x ~f: f =
      U.map x (Base.map_written ~f: f)
  end

  include Base
end
