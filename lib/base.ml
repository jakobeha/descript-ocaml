include (
  Core : module type of Core
  with module Monad := Core.Monad
   and module Bool := Core.Bool
   and module Option := Core.Option
   and module List := Core.List
   and module String := Core.String
   and module Unix := Core.Unix
)
include Util
