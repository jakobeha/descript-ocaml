open Base

module Format : sig
  type t =
    | Basic
    | Pretty of int

  val default : t
end

type format = Format.t

module type Type = sig
  type t

  val to_string : t -> format: format -> string
end

module Prim : Type with type t = Ast.Prim.t

module Symbol : Type with type t = Ast.Symbol.t

module Matcher : Type with type t = Ast.Matcher.t

module Path_elem : Type with type t = Ast.Path_elem.t

module Path : Type with type t = Ast.Path.t

module Pipe : Type with type t = Ast.Pipe.t

module Ann : sig
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    val alter : 'abs Ast.Annotate(T).value -> string option
  end

  module Unit : Type with module T = Unit
  module Taint (Text : sig val x : string end) :
    Type with module T = Taint
end

module Annotate (Ann : Ann.Type) : sig
  module Value : sig
    module Reg : Type with type t = Ast.Annotate(Ann.T).Value.Reg.t

    module In : Type with type t = Ast.Annotate(Ann.T).Value.In.t

    module Out : Type with type t = Ast.Annotate(Ann.T).Value.Out.t
  end

  module Reducer : Type with type t = Ast.Annotate(Ann.T).Reducer.t

  module Phase : Type with type t = Ast.Annotate(Ann.T).Phase.t

  module Program : Type with type t = Ast.Annotate(Ann.T).Program.t
end

module Pure : module type of Annotate(Ann.Unit)
module Taint (Text : sig val x : string end) :
  module type of Annotate(Ann.Taint(Text))
