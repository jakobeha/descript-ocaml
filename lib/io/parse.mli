open Core
open Lexing

module Error : sig
  type t =
    { reason : string;
      range : Loc.range;
    }

  val to_string : t -> string
end

exception Error of Error.t

module type Type = sig
  type t

  val parse : lexbuf -> t

  val from_string : string -> t

  val from_channel : In_channel.t -> t

  val from_file : string -> t
end

module Phase : Type with type t = Ast.Taint.phase

(** Instead of waiting for an EOF, waits for '---' - useful for
    continuous channels like STDIN. *)
module Phase_cont : Type with type t = Ast.Taint.phase

module Program : Type with type t = Ast.Taint.program
