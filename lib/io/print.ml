open Base

module Format = struct
  type t =
    | Basic
    | Pretty of int

  let default = Pretty 100
end

type format = Format.t

module PString = struct
  type dynamic =
    { collapsed : t list;
      expanded : t list lazy_t option;
    }
  and t =
    | Static of string
    | Dynamic of dynamic

  let string_fits x ~line: line ~first: f_line =
    match String.split x ~on: '\n' with
    | [] -> true
    | x :: xs ->
      String.length x < (line - f_line) &&
      List.for_all xs (fun x -> String.length x < line)

  let next_first_line x ~first: f_line =
    match String.split x ~on: '\n' with
    | [] -> f_line
    | [x] -> f_line + String.length x
    | xs -> String.length (List.last_exn xs)

  let to_string_static x =
    String.replace "\t" "  " x

  let rec list_to_string x ~line: line ~first: f_line =
    List.fold x ~init: "" ~f:
      ( fun cur next ->
          let next_f_line = next_first_line cur ~first: f_line in
          cur ^ to_string_part next ~line: line ~first: next_f_line
      )
  and dynamic_to_string x ~line: line ~first: f_line =
    let collapsed_str = (* Could optimize via short-circuit *)
      x.collapsed |>
      list_to_string ~line: line ~first: f_line in
    match x.expanded with
    | Some expanded when
        not (string_fits collapsed_str ~line: line ~first: f_line) ->
      expanded |>
      Lazy.force |>
      list_to_string ~line: line ~first: f_line
    | _ -> collapsed_str
  and to_string_part x ~line: line ~first: f_line =
    match x with
    | Static x -> to_string_static x
    | Dynamic x -> dynamic_to_string x ~line: line ~first: f_line

  let to_string x ~line: line =
    to_string_part x ~line: line ~first: 0

  let to_collapsed_string x =
    to_string x ~line: Int.max_value_30_bits

  let rec dynamic_indent x =
    { collapsed = List.map x.collapsed indent;
      expanded =
        x.expanded |>
        Option.map ~f: (Lazy.map ~f: (List.map ~f: indent));
    }
  and indent x =
    match x with
    | Static x -> Static (String.indent x)
    | Dynamic x -> Dynamic (dynamic_indent x)
end

type pstring = PString.t

module type Type = sig
  type t

  val to_string : t -> format: format -> string
end

module Simple = struct
  module type _Type = Type

  module Impl = struct
    module type Type = sig
      type t

      val to_bstring : t -> string
    end
  end

  module type Type = sig
    include _Type

    include Impl.Type with type t := t
  end

  module Make (A : Impl.Type) : Type with type t = A.t = struct
    include A

    let to_string x ~format: _ =
      to_bstring x
  end
end

module Complex = struct
  module type _Type = Type

  module Impl = struct
    module type Type = sig
      type t

      val to_string_basic : t -> string

      val to_string_pretty : t -> pstring
    end
  end

  module type Type = sig
    include _Type

    include Impl.Type with type t := t
  end

  module Value = struct
    module type Type = sig
      include Type

      val try_simple_head : t -> string option
    end
  end

  module Make (A : Impl.Type) : Type with type t = A.t = struct
    include A

    let to_string x ~format: format =
      match format with
      | Format.Basic -> to_string_basic x
      | Pretty line ->
        to_string_pretty x |>
        PString.to_string ~line: line
  end
end


module Nothing = Simple.Make(struct
  include Nothing

  let to_bstring x =
    Nothing.unreachable_code x
end)

module Prim = Simple.Make(struct
  include Ast.Prim

  let to_bstring x =
    match x with
    | Integer x -> string_of_int x
    | Float x -> string_of_float x
    | String x -> sprintf "%S" x
end)

module Symbol = Simple.Make(struct
  include Ast.Symbol

  let to_bstring x = x
end)

module Matcher = Simple.Make(struct
  include Ast.Matcher

  let prim_regexp_to_string r =
    "/" ^ String.replace "/" "\\/" (Regexp.to_string r) ^ "/"

  let type_to_string x =
    match x with
    | Ast.Matcher.Any -> ""
    | Prim -> "Prim"
    | Record -> "Record"
    | Integer -> "Integer"
    | Float -> "Float"
    | String -> "String"
    | Regexp r -> prim_regexp_to_string r

  let to_bstring x =
    "<" ^ type_to_string x
end)

module Path_elem = Simple.Make(struct
  include Ast.Path_elem

  let to_bstring e =
    match e with
    | Head -> "^"
    | Property k -> k
    | Remainder subs ->
      ("..." :: List.map subs Symbol.to_bstring) |>
      String.concat ~sep: "-"
end)

module Path = Simple.Make(struct
  include Ast.Path

  let to_bstring es =
    ">" ^ String.concat ~sep: ">" (List.map es Path_elem.to_bstring)
end)

module Pipe = Simple.Make(struct
  include Ast.Pipe

  let to_bstring x =
    match x with
    | Matcher x -> Matcher.to_bstring x
    | Path x -> Path.to_bstring x
end)

module Property = struct
  include
    ( Ast.Property : module type of Ast.Property with
      module Make := Ast.Property.Make
    )

  module Make (V : Complex.Type) = struct
    include Complex.Make(struct
      include Ast.Property.Make(V)

      let to_string_basic x =
        x.Ast.Property.key ^ ": " ^ V.to_string_basic x.value

      let to_string_pretty x =
        let key_pstr = PString.Static (Symbol.to_bstring x.Ast.Property.key)
        and val_pstr = V.to_string_pretty x.value in
        PString.Dynamic
          { collapsed = [key_pstr; Static ": "; val_pstr];
            expanded = Some (lazy [key_pstr; Static ":\n\t"; val_pstr])
          }
    end)

    let to_string_pretty_idxd idx x =
      if x.Ast.Property.key = Misc.Symbol.from_index idx then
        V.to_string_pretty x.value
      else
        to_string_pretty x
  end
end

module Record = struct
  include (
    Ast.Record : module type of Ast.Record
    with module Make := Ast.Record.Make
  )

  module Make (V : Complex.Value.Type) = struct
    include Ast.Record.Make(V)

    module V_property = Property.Make(V)

    let string_of_head_basic x =
      match V.try_simple_head x with
      | None -> "{" ^ V.to_string_basic x ^ "}"
      | Some x -> x

    let string_of_head_pretty x =
      match V.try_simple_head x with
      | None ->
        let x_pstr = V.to_string_pretty x in
        PString.Dynamic
          { collapsed = [Static "{"; x_pstr; Static "}"];
            expanded = Some (lazy [Static "{\n"; PString.indent x_pstr; Static "\n}"]);
          }
      | Some x -> Static x

    let to_string_basic x =
      string_of_head_basic x.Ast.Record.head ^
      "[" ^
      String.concat
        (List.map x.props V_property.to_string_basic)
        ~sep: "; " ^
      "]"

    let to_string_pretty x =
      let head_pstr = string_of_head_pretty x.Ast.Record.head
      and prop_pstrs = List.mapi x.props V_property.to_string_pretty_idxd in
      let prop_cstrs = List.map prop_pstrs PString.to_collapsed_string in
      PString.Dynamic
        { collapsed =
            if List.length prop_pstrs = 1 then
              ( List.concat
                  [ [head_pstr; Static "["];
                    List.intersperse prop_pstrs ~sep: (Static "; ");
                    [Static "]"];
                  ]
              )
            else
              [ head_pstr;
                Static ("[" ^ String.concat prop_cstrs ~sep: "; " ^ "]");
              ];
          expanded =
            if List.is_empty prop_pstrs then
              None
            else
              Some
                ( lazy
                  ( List.concat
                      [ [head_pstr; Static "[\n\t"];
                        prop_pstrs |>
                        List.map ~f: PString.indent |>
                        List.intersperse ~sep: (PString.Static "\n\t");
                        [Static "\n]"];
                      ]
                  )
                );
        }
  end
end

module Ann = struct
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    val alter : 'abs Ast.Annotate(T).value -> string option
  end

  module Unit = struct
    module T = Unit

    type t = T.t

    let alter _ = None
  end

  module Taint (Text : sig val x : string end) = struct
    module T = Taint
    module Value_fold_or = Misc.Value.Fold(Bool.Or)

    type t = T.t

    let reprint x =
      let taint = Misc.Value.annotation x in
      Value_fold_or.anns x (fun taint -> taint.Taint.is_tainted) ||
      not (taint.is_literal)

    let alter x =
      if reprint x then
        None
      else
        let taint = Misc.Value.annotation x in
        Some (Loc.Range.in_string taint.range Text.x)
  end
end

module Annotate (Ann : Ann.Type) = struct
  module Ann_ast = Ast.Annotate(Ann.T)

  module Value = struct
    include (
      Ann_ast.Value : module type of Ann_ast.Value
      with module Make := Ann_ast.Value.Make
       and module Reg := Ann_ast.Value.Reg
       and module In := Ann_ast.Value.In
       and module Out := Ann_ast.Value.Out
    )

    module Make (Abs : Simple.Type) = struct
      module rec Self : Complex.Value.Type
        with type t = (Ann.t, Abs.t) Ast.Value.t = struct
        type t = (Ann.t, Abs.t) Ast.Value.t

        module Self_record = Record.Make(Self)

        module Complex_impl = Complex.Make(struct
          type _t = t
          type t = _t

          let pure_to_string_basic x =
            match x with
            | Ast.Value.Prim (_, x) -> Prim.to_bstring x
            | Record (_, x) -> Self_record.to_string_basic x
            | Pipe (_, x) -> Abs.to_bstring x

          let plain_to_string_pretty x =
            match x with
            | Ast.Value.Prim (_, x) -> PString.Static (Prim.to_bstring x)
            | Record (_, x) -> Self_record.to_string_pretty x
            | Pipe (_, x) -> PString.Static (Abs.to_bstring x)

          let shortcut_list_to_string xs =
            let pstrs = List.map xs Self.to_string_pretty in
            let cstrs = List.map pstrs PString.to_collapsed_string in
            PString.Dynamic
              { collapsed =
                  if List.length pstrs = 1 then
                    ( List.concat
                        [ [PString.Static "$["];
                          List.intersperse pstrs ~sep: (Static "; ");
                          [Static "]"];
                        ]
                    )
                  else
                    [Static ("$[" ^ String.concat cstrs ~sep: "; " ^ "]")];
                expanded =
                  if List.is_empty pstrs then
                    None
                  else
                    Some
                      ( lazy
                        ( List.concat
                            [ [PString.Static "$[\n\t"];
                              pstrs |>
                              List.map ~f: PString.indent |>
                              List.intersperse ~sep: (PString.Static "\n\t");
                              [Static "\n]"];
                            ]
                        )
                      );
              }

          let pure_to_string_pretty x =
            try x |> Lit.Value.to_list |> shortcut_list_to_string with
            | Lit.Value.Bad_form "list" ->
              plain_to_string_pretty x

          let to_string_basic x =
            match Ann.alter x with
            | None -> pure_to_string_basic x
            | Some res -> res

          let to_string_pretty x =
            match Ann.alter x with
            | None -> pure_to_string_pretty x
            | Some res -> Static res
        end)

        include
          ( Complex_impl : module type of Complex_impl
            with type t := t
          )

        let local_simple_head x =
          x |>
          Lit.Value.symbol_of_head |>
          Symbol.to_bstring

        let qualified_simple_head x =
          let (loc, local) = Lit.Value.to_import x in
          if Misc.Symbol.is_valid loc then
            loc ^ ">" ^ local_simple_head local
          else
            raise (Lit.Value.Bad_form "symbol head qualifier")

        let try_simple_head x =
          try Some (qualified_simple_head x) with
          | Lit.Value.Bad_form _ ->
            try Some (local_simple_head x) with
            | Lit.Value.Bad_form _ -> None
      end

      include Self
    end

    module Reg = Make(Nothing)
    module In = Make(Matcher)
    module Out = Make(Path)
  end

  module Reducer = Complex.Make(struct
    include Ann_ast.Reducer

    let to_string_basic x =
      Value.In.to_string_basic x.Ast.Reducer.input ^
      ": " ^
      Value.Out.to_string_basic x.output

    let to_string_pretty x =
      let in_pstr = Value.In.to_string_pretty x.Ast.Reducer.input
      and out_pstr = Value.Out.to_string_pretty x.output in
      PString.Dynamic
        { collapsed = [in_pstr; Static ": "; out_pstr];
          expanded = Some (lazy [in_pstr; Static ":\n\t"; out_pstr]);
        }
  end)

  module Phase = Complex.Make(struct
    include Ann_ast.Phase

    let to_string_basic xs =
      xs |>
      List.map ~f: (fun x -> Reducer.to_string_basic x ^ ";") |>
      String.concat ~sep: "\n"


    let to_string_pretty xs =
      PString.Dynamic
        { collapsed =
            xs |>
            List.map ~f: Reducer.to_string_pretty |>
            List.intersperse ~sep: (PString.Static "\n");
          expanded = None;
        }
  end)

  module Program = Complex.Make(struct
    include Ann_ast.Program

    let basic_string_of_query x =
      Value.Reg.to_string_basic x ^ "?"

    let pretty_string_of_query x =
      PString.Dynamic
        { collapsed = [Value.Reg.to_string_pretty x; Static "?"];
          expanded = None;
        }

    let to_string_basic x =
      String.concat ~sep: "\n---\n"
        ( List.map x.Ast.Program.phases Phase.to_string_basic @
          [basic_string_of_query x.query]
        ) |>
      String.strip

    let to_string_pretty x =
      PString.Dynamic
        { collapsed =
            ( List.map x.Ast.Program.phases Phase.to_string_pretty @
              [pretty_string_of_query x.query];
            ) |>
            List.intersperse ~sep: (PString.Static "\n---\n");
          expanded = None;
        }
  end)
end

module Pure = Annotate(Ann.Unit)
module Taint (Text : sig val x : string end) = Annotate(Ann.Taint(Text))
