type t =
  { range : Loc.range;
    is_literal : bool;
    is_tainted : bool;
  }

val parsed : Loc.range -> t

val taint : t -> t
