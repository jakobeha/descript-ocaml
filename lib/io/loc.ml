open Core

module Loc = struct
  type t =
    { pos : int;
      line : int;
      column : int;
    }

  let zero =
    { pos = 0;
      line = 0;
      column = 0;
    }

  let to_string x =
    sprintf "%i:%i" x.line x.column
end

type loc = Loc.t

module Range = struct
  type t =
    { start : loc;
      after : loc;
    }

  let pos_length x =
    x.after.pos - x.start.pos

  let in_string x str =
    String.sub str x.start.pos (pos_length x)

  let to_string x =
    sprintf "%s-%s" (Loc.to_string x.start) (Loc.to_string x.after)
end

type range = Range.t

module Locd = struct
  type 'a t =
    { loc : loc;
      value : 'a;
    }
end

type 'a locd = 'a Locd.t

module Ranged = struct
  type 'a t =
    { range : range;
      value : 'a;
    }
end

type 'a ranged = 'a Ranged.t
