module Loc : sig
  type t =
    { pos : int;
      line : int;
      column : int;
    }

  val zero : t

  val to_string : t -> string
end

type loc = Loc.t

module Range : sig
  type t =
    { start : loc;
      after : loc;
    }

  val in_string : t -> string -> string

  val to_string : t -> string
end

type range = Range.t

module Locd : sig
  type 'a t =
    { loc : loc;
      value : 'a;
    }
end

type 'a locd = 'a Locd.t

module Ranged : sig
  type 'a t =
    { range : range;
      value : 'a;
    }
end

type 'a ranged = 'a Ranged.t
