type t =
  { range : Loc.range;
    is_literal : bool;
    is_tainted : bool;
  }

let parsed x =
  { range = x;
    is_literal = true;
    is_tainted = false;
  }

let taint x =
  { range = x.range;
    is_literal = true;
    is_tainted = true;
  }
