exception Error of Loc.range * string

let raise_error rng msg =
  raise (Error (rng, msg))
