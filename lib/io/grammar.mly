(* Derived from https://v1.realworldocaml.org/v1/en/html/parsing-with-ocamllex-and-menhir.html *)
%{
open Core
open Refine

module OLocd = struct
  type 'a t =
    { oloc : Loc.loc option;
      value : 'a;
    }
end

type 'a olocd = 'a OLocd.t

let refine_to_matcher_type rng x =
  match x with
  | "Prim" -> Ast.Matcher.Prim
  | "Record" -> Record
  | "Integer" -> Integer
  | "Float" -> Float
  | "String" -> String
  | _ -> Refine.raise_error rng "undefined matcher type"

let rec refine_to_reg_value x =
  Misc.Value.Map.pipes_annd x
  ( fun ann p ->
      match p with
      | Ast.Pipe.Matcher _ ->
        Refine.raise_error ann.Taint.range "matcher in regular value"
      | Path _ ->
        Refine.raise_error ann.Taint.range "path in regular value"
  )

let rec refine_to_in_value x =
  Misc.Value.Map.pipes_annd x
  ( fun ann p ->
      match p with
      | Ast.Pipe.Matcher p -> p
      | Path _ ->
        Refine.raise_error ann.Taint.range "path in input value"
  )

let rec refine_to_out_value x =
  Misc.Value.Map.pipes_annd x
  ( fun ann p ->
      match p with
      | Ast.Pipe.Matcher _ ->
        Refine.raise_error ann.Taint.range "matcher in output value"
      | Path x -> x
  )
%}
%token <string Loc.ranged> Symbol
%token <int Loc.ranged> Integer
%token <float Loc.ranged> Float
%token <string Loc.ranged> String
%token <Util.Regexp.t Loc.ranged> Regexp
%token <Loc.loc> Dollar
%token <Loc.loc> Hash
%token <Loc.range> Angle_bwd
%token <Loc.loc> Angle_fwd
%token <Loc.loc> Angle_up
%token Dash
%token Phase_sep
%token <Loc.loc> Ellipsis
%token Semicolon
%token Colon
%token Question
%token Open_bracket
%token <Loc.loc> Close_bracket
%token <Loc.loc> Open_brace
%token Close_brace
%token Eof
%type <Ast.Taint.max_value Loc.locd> head
%type <Ast.Taint.max_value Ast.property> property
%type <Ast.Taint.max_value Ast.record Loc.ranged> record
%type <Ast.matcher Loc.ranged> matcher
%type <Ast.matcher olocd> matcher_type
%type <Ast.path_elem Loc.locd> path_elem
%type <Ast.path Loc.ranged> path
%type <Ast.pipe Loc.ranged> pipe
%type <Ast.Taint.reg_value> reg_value
%type <Ast.Taint.in_value> in_value
%type <Ast.Taint.out_value> out_value
%type <Ast.Taint.max_value> value
%type <Ast.Taint.reducer> reducer
%start <Ast.Taint.phase> phase_sepd
%start <Ast.Taint.phase> phase_eof
%start <Ast.Taint.program> program_eof
%%
program_eof:
  | x = program; Eof
  { x }
  ;
phase_eof:
  | x = phase; Eof
  { x }
  ;
program:
  | q = query
  { { Ast.Program.phases = []; query = q } }
  | x = phase_sepd; p = program
  { { phases = x :: p.phases; query = p.query } }
  ;
phase_sepd:
  | x = phase; Phase_sep
  { x }
  ;
(* TODO Find out how to not inline 'phases'. *)
%inline phase:
  | xs = list(x = reducer; Semicolon { x })
  { xs }
  ;
query:
  | x = reg_value; Question
  { x }
  ;
reducer:
  | i = in_value; Colon; o = out_value
  { { input = i; output = o; } }
  ;
%inline reg_value:
  | x = value
  { refine_to_reg_value x }
  ;
%inline in_value:
  | x = value
  { refine_to_in_value x }
  ;
%inline out_value:
  | x = value
  { refine_to_out_value x }
  ;
value:
  | x = prim
  { Prim (Taint.parsed x.Loc.Ranged.range, x.value) }
  | x = record
  { Record (Taint.parsed x.Loc.Ranged.range, x.value) }
  | x = pipe
  { Pipe (Taint.parsed x.Loc.Ranged.range, x.value) }
  | x = shortcut
  { x }
  ;
pipe:
  | x = matcher
  { { range = x.range; value = Matcher x.value } }
  | x = path
  { { range = x.range; value = Path x.value } }
  ;
prim:
  | x = Integer
  { { Loc.Ranged.range = x.range; value = Ast.Prim.Integer x.value } }
  | x = Float
  { { range = x.range; value = Ast.Prim.Float x.value } }
  | x = String
  { { range = x.range; value = Ast.Prim.String x.value } }
  ;
record:
  | h = head; ps = bracket_list(property)
  { { range = { start = h.loc; after = ps.loc };
      value =
        { head = h.value;
          props = Misc.Property.fill_idxs ps.value;
        };
    }
  }
  ;
head:
  | x = Symbol
  { { loc = x.range.start;
      value = Lit.Value.head_of_symbol (Taint.parsed x.range) x.value;
    }
  }
  | loc = Symbol; Angle_fwd; x = Symbol
  { { loc = loc.range.start;
      value =
        Lit.Taint.Value.import
          (Taint.parsed { start = loc.range.start; after = x.range.after })
          loc.value
          (Lit.Value.head_of_symbol (Taint.parsed x.range) x.value)
    }
  }
  | r = Hash; x = Symbol
  { { loc = r;
      value =
        Prim
          ( Taint.parsed { start = r; after = x.range.after },
            String ("#" ^ x.value)
          );
    }
  }
  | r = Open_brace; x = value; Close_brace
  { { loc = r; value = x } }
  ;
property:
  | k = Symbol; Colon; v = value
  { { key = k.value; value = v; } }
  | Ellipsis; Colon; v = value
  { { key = "..."; value = v; } }
  | v = value
  { { key = "{index}"; value = v; } }
  ;
matcher:
  | r = Angle_bwd; x = matcher_type
  { { range =
        { start = r.start;
          after = Option.value x.OLocd.oloc ~default: r.after;
        };
      value = x.value;
    }
  }
  ;
%inline matcher_type:
  | (* empty *)
  { { OLocd.oloc = None; value = Ast.Matcher.Any } }
  | x = Regexp
  { { OLocd.oloc = Some x.range.after;
      value = Ast.Matcher.Regexp x.value;
    }
  }
  | x = Symbol
  { { OLocd.oloc = Some x.range.after;
      value = refine_to_matcher_type x.range x.value;
    }
  }
  ;
path:
  | r = Angle_fwd; x = separated_list(Angle_fwd, path_elem)
  { { range =
        { Loc.Range.start = r;
          after =
            match List.last x with
            | None -> r
            | Some e -> e.Loc.Locd.loc;
        };
      value = List.map x (fun e -> e.value);
    }
  }
  ;
path_elem:
  | r = Angle_up
  { { loc = r; value = Head } }
  | x = Symbol
  { { loc = x.range.after; value = Property x.value } }
  | r = Ellipsis; subs = list(path_elem_remainder_sub)
  { { loc =
        ( match List.last subs with
          | None -> r
          | Some sub -> sub.range.after
        );
      value = Remainder (List.map subs (fun sub -> sub.value));
    }
  }
  ;
%inline path_elem_remainder_sub:
  | Dash; x = Symbol
  { x }
  ;
shortcut:
  | r = Dollar; xs = bracket_list(value)
  { Lit.Taint.Value.list
      (Taint.parsed { start = r; after = xs.loc })
      xs.value
  }
  ;
%inline bracket_list(X):
  | Open_bracket; xs = separated_list(Semicolon, X); r = Close_bracket
  { { Loc.Locd.loc = r; value = xs } }
  ;
