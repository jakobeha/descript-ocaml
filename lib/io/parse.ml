(* Derived from https://v1.realworldocaml.org/v1/en/html/parsing-with-ocamllex-and-menhir.html *)
open Base
open Lexing
open Lexer
open Grammar

module Error = struct
  type t =
    { reason : string;
      range : Loc.range;
    }

  let to_string x =
    sprintf "%s - %s" (Loc.Range.to_string x.range) x.reason
end

exception Error of Error.t

module type Type = sig
  type t

  val parse : lexbuf -> t

  val from_string : string -> t

  val from_channel : In_channel.t -> t

  val from_file : string -> t
end

module Lexbuf = struct
  let position lexbuf =
    let pos = lexbuf.lex_curr_p in
    sprintf "%s:%d:%d" pos.pos_fname
      pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)
end

module type Impl_type = sig
  type t

  val parse_checkpoint : position -> t MenhirInterpreter.checkpoint
end

module Make (A : Impl_type) : Type with type t = A.t = struct
  type t = A.t

  type parse_failure =
    | Grammar of t MenhirInterpreter.checkpoint
    | Refine of Loc.range * string

  type lookahead_supplier =
    { supplier : MenhirInterpreter.supplier;
      mutable prev_token : (MenhirInterpreter.token * position * position) option;
      mutable prev_prev_token : (MenhirInterpreter.token * position * position) option;
    }

  module Recover_m = struct
    type _t = t
    type 'a t =
      | Fail
      | Intermediate of 'a
      | Done of _t

    let map x ~f: f =
      match x with
      | Fail -> Fail
      | Intermediate x -> Intermediate (f x)
      | Done x -> Done x

    let bind x ~f: f =
      match x with
      | Fail -> Fail
      | Intermediate x -> f x
      | Done x -> Done x

    let bind_if pred x ~f: f =
      if pred then
        bind x ~f: f
      else
        x

    let (>>|) x f = map x ~f: f

    let (>>=) x f = bind x ~f: f
  end

  let rec force_consume checkpoint =
    match checkpoint with
    | MenhirInterpreter.Shifting _
    | AboutToReduce _ ->
      force_consume (MenhirInterpreter.resume checkpoint)
    | _ -> checkpoint

  let try_consume checkpoint =
    match force_consume checkpoint with
    | HandlingError _ -> Recover_m.Fail
    | InputNeeded env -> Intermediate env
    | Accepted x -> Done x
    | _ -> invalid_arg "unexpected checkpoint"

  (* Taken directly from menhirLib *)
  let rec loop_handle_undo succeed fail read (prev_inputneeded, inputneeded, checkpoint) =
    match checkpoint with
    | MenhirInterpreter.InputNeeded _ ->
      (* Update the last recorded [InputNeeded] checkpoint. *)
      let prev_inputneeded = inputneeded
      and inputneeded = checkpoint
      and triple = read () in
      let checkpoint = MenhirInterpreter.offer checkpoint triple in
      loop_handle_undo succeed fail read (prev_inputneeded, inputneeded, checkpoint)
    | Shifting _
    | AboutToReduce _ ->
      ( match MenhirInterpreter.resume checkpoint with
        | checkpoint ->
          loop_handle_undo succeed fail read (prev_inputneeded, inputneeded, checkpoint)
        | exception Refine.Error (rng, msg) ->
          fail prev_inputneeded (Refine (rng, msg))
      )
    | HandlingError _
    | Rejected ->
      fail inputneeded (Grammar checkpoint)
    | Accepted v ->
      succeed v

  let loop_handle_undo succeed fail read checkpoint =
    assert
      ( match checkpoint with
        | MenhirInterpreter.InputNeeded _ -> true
        | _ -> false
      );
    loop_handle_undo succeed fail read (checkpoint, checkpoint, checkpoint)

  let raise_error x = raise (Error x)

  let parse_success x = x

  let parse_fail_env env =
    match MenhirInterpreter.top env with
    | None ->
      raise_error
        { reason = "empty source - expected something!";
          range =
            { start = Loc.Loc.zero;
              after = Loc.Loc.zero;
            };
        }
    | Some MenhirInterpreter.Element (state, _, start_pos, end_pos) ->
      raise_error
        { reason = Grammar_messages.message (MenhirInterpreter.number state);
          range =
            { start = Lexer.loc_of_position start_pos;
              after = Lexer.loc_of_position end_pos;
            };
        }

  let parse_fail err =
    match err with
    | Grammar state ->
    ( match state with
      | MenhirInterpreter.HandlingError env ->
        parse_fail_env env
      | _ -> invalid_arg "expected handling error state"
    )
    | Refine (rng, msg) ->
      raise_error
        { reason = msg;
          range = rng;
        }

  let supplier_of_lexbuf lexbuf =
    let unchecked_supplier =
      MenhirInterpreter.lexer_lexbuf_to_supplier Lexer.read lexbuf in
    let rec res =
      { supplier = (fun () ->
            try ( let next = unchecked_supplier () in
                  res.prev_prev_token <- res.prev_token;
                  res.prev_token <- Some next;
                  next ) with
            | SyntaxError msg ->
              raise_error
                { reason = msg;
                  range = Lexer.range lexbuf;
                });
        prev_token = None;
        prev_prev_token = None;
      } in
    res

  let try_recover supplier prev has_2_prevs =
    let prev_token =
      if has_2_prevs then
        supplier.prev_prev_token
      else
        supplier.prev_token in
    match prev_token with
    | None -> Recover_m.Fail
    | Some ((_, prev_start_pos, _) as prev_token) ->
      match prev with
      | MenhirInterpreter.InputNeeded env ->
        let (_, end_pos) = MenhirInterpreter.positions env in
        if prev_start_pos.pos_lnum <= end_pos.pos_lnum then
          Fail
        else
          let open Recover_m in
          MenhirInterpreter.offer prev (Semicolon, end_pos, end_pos) |>
          try_consume >>|
          MenhirInterpreter.input_needed >>|
          (fun x -> MenhirInterpreter.offer x prev_token) >>=
          try_consume >>|
          MenhirInterpreter.input_needed |>
          Recover_m.bind_if has_2_prevs ~f:
          ( fun x ->
            let next_prev_token = Option.value_exn supplier.prev_token in
            MenhirInterpreter.offer x next_prev_token |>
            try_consume >>|
            MenhirInterpreter.input_needed
          )
      | _ -> invalid_arg "expected input needed state"

  let err_has_2_prevs err =
    match err with
    | Grammar _ -> false
    | Refine _ -> true

  let rec parse_reroute supplier prev err =
    match try_recover supplier prev (err_has_2_prevs err) with
    | Recover_m.Fail -> parse_fail err
    | Intermediate cp -> parse_from supplier cp
    | Done x -> x
  and parse_from supplier cp =
    loop_handle_undo
      parse_success
      (parse_reroute supplier)
      supplier.supplier
      cp

  let parse lexbuf =
    parse_from
      (supplier_of_lexbuf lexbuf)
      (A.parse_checkpoint lexbuf.lex_curr_p)

  let from_string x =
    parse (Lexing.from_string x)

  let from_channel ch =
    parse (Lexing.from_channel ch)

  let from_file path =
    from_channel (In_channel.create path)
end

module Phase = Make(struct
  include Ast.Taint.Phase

  let parse_checkpoint x =
    Grammar.Incremental.phase_eof x
end)

module Phase_cont = Make(struct
  include Ast.Taint.Phase

  let parse_checkpoint x =
    Grammar.Incremental.phase_sepd x
end)

module Program = Make(struct
  include Ast.Taint.Program

  let parse_checkpoint x =
    Grammar.Incremental.program_eof x
end)
