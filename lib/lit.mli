open Core

module Ann : sig
  module type Type = sig
    module T : sig
      type t
    end

    type t = T.t

    val import_wrap : t -> t

    val import_head : t -> t

    val import_location : t -> t

    val bool_head : t -> t

    val property_head : t -> t

    val property_key : t -> t

    (** First input annotation is the full list, second is the first item.
        First output annotation is the cons head, second is the rest list. *)
    val sub_cons : t -> t -> t * t

    val nil_head : t -> t
  end

  module Pure : Type with module T = Unit
  module Taint : Type with module T = Taint
end

module Value : sig
  type ('ann, 'abs) t = ('ann, 'abs) Ast.Value.t

  exception Bad_form of string

  val head_of_symbol : 'ann -> Ast.symbol -> ('ann, 'abs) t

  val try_symbol_of_head : ('ann, 'abs) t -> Ast.symbol option

  val symbol_of_head : ('ann, 'abs) t -> Ast.symbol

  val to_import : ('ann, 'abs) t -> string * ('ann, 'abs) t

  val is_import_head : ('ann, 'abs) t -> bool

  val is_import : ('ann, 'abs) t -> bool

  val to_string : ('ann, 'abs) t -> string

  val is_inj_head : ('ann, 'abs) t -> bool

  val to_inj_head : ('ann, 'abs) t -> string

  val to_property : ('ann, 'abs) t -> ('ann, 'abs) t Ast.property

  val is_list_head : ('ann, 'abs) t -> bool

  val to_list : ('ann, 'abs) t -> ('ann, 'abs) t list
end

module Annotate (Ann : Ann.Type) : sig
  type ann = Ann.t

  module Value : sig
    type 'abs t = 'abs Ast.Annotate(Ann.T).Value.t

    (** Given annotation is for the wrapper. *)
    val import : ann -> string -> 'abs t -> 'abs t

    (** Uses [Ann.Type.import_wrap] to get wrapper annotation. *)
    val imported : string -> 'abs t -> 'abs t

    val bool : ann -> bool -> 'abs t

    val property : ann -> 'abs t Ast.property -> 'abs t

    val list_init : ann -> 'abs t list -> (ann -> 'abs t) -> 'abs t

    val nil : ann -> 'abs t

    val list : ann -> 'abs t list -> 'abs t
  end
end

module Pure : module type of Annotate(Ann.Pure)
module Taint : module type of Annotate(Ann.Taint)
