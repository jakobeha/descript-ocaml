# Descript-lang

OCaml interpreter for descript, a programming language.

- Some incomplete documentation: <https://bitbucket.org/jakobeha/descript-ocaml/src/master/docs/Summary.md>
- The old Haskell implementation: <https://bitbucket.org/jakobeha/descript-lang/src/master/>
